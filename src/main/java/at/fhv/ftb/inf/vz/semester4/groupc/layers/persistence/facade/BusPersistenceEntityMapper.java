package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class BusPersistenceEntityMapper<BusEntity> extends PersistenceEntityMapper<BusEntity> {
    @Override
    public BusEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (BusEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.BusEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("BusEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<BusEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from BusEntity ");
            return (List<BusEntity>) query.getResultList();
        }
    }

    @Override
    public List<BusEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from BusEntity " + whereQueryFragment);
            return (List<BusEntity>) query.getResultList();
        }
    }
}
