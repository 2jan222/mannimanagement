package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class AbsencePersistenceEntityMapper<AbsenceEntity> extends PersistenceEntityMapper<AbsenceEntity>{
    @Override
    public AbsenceEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (AbsenceEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.AbsenceEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("AbsenceEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<AbsenceEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from AbsenceEntity ");
            return (List<AbsenceEntity>) query.getResultList();
        }
    }

    @Override
    public List<AbsenceEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from AbsenceEntity " + whereQueryFragment);
            return (List<AbsenceEntity>) query.getResultList();
        }
    }
}
