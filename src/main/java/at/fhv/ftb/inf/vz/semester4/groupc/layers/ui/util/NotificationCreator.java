package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class NotificationCreator {


    public static void createNotificationInternal(String title, String text) {
        createNotificationInternal(title, text, null).show();
    }

    public static void createWarningNotification(String title, String text) {
        createNotificationInternal(title, text, null).showWarning();
    }

    public static void createErrorNotification(String title, String text) {
        createNotificationInternal(title, text, null).showError();
    }

    public static void createNotification(String title, String text, Node graphic) {
        createNotificationInternal(title, text, graphic).show();
    }

    public static void createWarningNotification(String title, String text, Node graphic) {
        createNotificationInternal(title, text, graphic).showWarning();
    }

    public static void createErrorNotification(String title, String text, Node graphic) {
        createNotificationInternal(title, text, graphic).showError();
    }

    private static Notifications createNotificationInternal(String title, String text, Node graphic) {
        Notifications notifications = Notifications.create();
        return notifications
                .title(title)
                .text(text)
                .position(Pos.TOP_RIGHT)
                .hideAfter(Duration.seconds(5))
                .onAction(e -> notifications.hideAfter(Duration.millis(0)))
                .darkStyle()
                .graphic(graphic);
    }
}
