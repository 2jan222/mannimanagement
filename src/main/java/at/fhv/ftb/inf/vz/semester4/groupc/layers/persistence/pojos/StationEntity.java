package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "station", schema = "public", catalog = "kmobil")
public class StationEntity implements DatabaseEntityMarker {
    private int stationId;
    private String stationName;
    private String shortName;
    private Set<StationConnectorEntity> startStationConnectorsByStationId;
    private Set<StationConnectorEntity> endStationConnectorsByStationId;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "station_id")
    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    @Basic
    @Column(name = "station_name")
    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Basic
    @Column(name = "short_name")
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StationEntity that = (StationEntity) o;
        return stationId == that.stationId &&
                Objects.equals(stationName, that.stationName) &&
                Objects.equals(shortName, that.shortName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stationId, stationName, shortName);
    }

    @OneToMany(mappedBy = "startStation", fetch = FetchType.EAGER)
    public Set<StationConnectorEntity> getStartStationConnectorsByStationId() {
        return startStationConnectorsByStationId;
    }

    public void setStartStationConnectorsByStationId(Set<StationConnectorEntity> startStationConnectorsByStationId) {
        this.startStationConnectorsByStationId = startStationConnectorsByStationId;
    }

    @OneToMany(mappedBy = "endStation", fetch = FetchType.EAGER)
    public Set<StationConnectorEntity> getEndStationConnectorsByStationId() {
        return endStationConnectorsByStationId;
    }

    public void setEndStationConnectorsByStationId(Set<StationConnectorEntity> endStationConnectorsByStationId) {
        this.endStationConnectorsByStationId = endStationConnectorsByStationId;
    }

    @Override
    public String toString() {
        return "StationEntity{" +
                "stationId=" + stationId +
                ", stationName='" + stationName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", startConnectors=" + startStationConnectorsByStationId.size() +
                ", endConnectors=" + endStationConnectorsByStationId.size() +
               // ", pathStationsByStationIdAmount=" + pathStationsByStationId.size() +
                '}';
    }
}
