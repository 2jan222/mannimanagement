package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.BusDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * BusDTO mapper.
 */
@Mapper
public interface BusDTOMapper {
    BusDTOMapper INSTANCE = Mappers.getMapper(BusDTOMapper.class);

    BusDTO toDTO(Bus bus);

    Bus toMojo(BusDTO busDTO);

    void updateDTO(Bus bus, @MappingTarget BusDTO busDTO);

    Bus updateMojo(BusDTO busDTO, @MappingTarget Bus bus);

    List<BusDTO> toDTOs(List<Bus> buses);

    List<Bus> toMojos(List<BusDTO> busDTOS);


}
