package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "route_ride", schema = "public", catalog = "kmobil")
public class RouteRideEntity implements DatabaseEntityMarker {
    private int routeRideId;
    private Integer busId;
    private Integer operationId;
    private int routeId;
    private int startTimeId;
    private LocalDate routeRideDate;
    private BusEntity busByBusId;
    private OperationEntity operationByOperationId;
    private RouteEntity routeByRouteId;
    private StartTimeEntity startTimeByStartTimeId;
    private Integer operationShiftId;
    private OperationShiftEntity operationShiftEntityByOperationShiftId;

    public RouteRideEntity() {
    }

    public RouteRideEntity(RouteRideEntityFast flat) {
        this.routeRideId = flat.getRouteRideId();
        //this.operationId = flat.getOperationId();
        this.routeId = flat.getRouteId();
        this.startTimeId = flat.getStartTimeId();
        //this.routeRideDate = flat.getRouteRideDate();
        this.routeByRouteId = new RouteEntity(flat.getRouteId(), flat.getRouteEntity().getRouteNumber(), null, null, null, null, null);
        this.startTimeByStartTimeId = flat.getStartTimeByStartTimeId();
        //this.operationShiftId = flat.getOperationShiftId();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_ride_id")
    public int getRouteRideId() {
        return routeRideId;
    }

    public void setRouteRideId(int routeRideId) {
        this.routeRideId = routeRideId;
    }

    @Basic
    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @Basic
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "route_id")
    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "start_time_id")
    public int getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(int startTimeId) {
        this.startTimeId = startTimeId;
    }

    @Basic
    @Column(name = "route_ride_date")
    public LocalDate getRouteRideDate() {
        return routeRideDate;
    }

    public void setRouteRideDate(LocalDate routeRideDate) {
        this.routeRideDate = routeRideDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteRideEntity that = (RouteRideEntity) o;
        return routeRideId == that.routeRideId &&
                routeId == that.routeId &&
                startTimeId == that.startTimeId &&
                Objects.equals(busId, that.busId) &&
                Objects.equals(operationId, that.operationId) &&
                Objects.equals(routeRideDate, that.routeRideDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(routeRideId, busId, operationId, routeId, startTimeId, routeRideDate);
    }

    @ManyToOne
    @JoinColumn(name = "bus_id", referencedColumnName = "bus_id", insertable = false, updatable = false)
    public BusEntity getBusByBusId() {
        return busByBusId;
    }

    public void setBusByBusId(BusEntity busByBusId) {
        this.busByBusId = busByBusId;
    }

    @ManyToOne
    @JoinColumn(name = "operation_id", referencedColumnName = "operation_id", insertable = false, updatable = false)
    public OperationEntity getOperationByOperationId() {
        return operationByOperationId;
    }

    public void setOperationByOperationId(OperationEntity operationByOperationId) {
        this.operationByOperationId = operationByOperationId;
    }

    @ManyToOne
    @JoinColumn(name = "route_id", referencedColumnName = "route_id", nullable = false, insertable = false, updatable = false)
    public RouteEntity getRouteByRouteId() {
        return routeByRouteId;
    }

    public void setRouteByRouteId(RouteEntity routeByRouteId) {
        this.routeByRouteId = routeByRouteId;
    }

    @ManyToOne
    @JoinColumn(name = "start_time_id", referencedColumnName = "start_time_id", nullable = false, insertable = false, updatable = false)
    public StartTimeEntity getStartTimeByStartTimeId() {
        return startTimeByStartTimeId;
    }

    public void setStartTimeByStartTimeId(StartTimeEntity startTimeByStartTimeId) {
        this.startTimeByStartTimeId = startTimeByStartTimeId;
    }

    @Override
    public String toString() {
        return "RouteRideEntity{" +
                "routeRideId=" + routeRideId +
                ", busId=" + busId +
                ", operationId=" + operationId +
                ", routeId=" + routeId +
                ", startTimeId=" + startTimeId +
                ", routeRideDate=" + routeRideDate +
                ", busByBusId=" + busByBusId +
                ", operationByOperationId=" + operationByOperationId +
                ", routeByRouteId=" + routeByRouteId +
                ", startTimeByStartTimeId=" + startTimeByStartTimeId +
                ", operationShiftId=" + operationShiftId +
                '}';
    }

    @Basic
    @Column(name="operation_shift_id")
    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

    @ManyToOne
    @JoinColumn(name = "operation_shift_id", referencedColumnName = "operation_shift_id", nullable = false, insertable = false, updatable = false)
    public OperationShiftEntity getOperationShiftEntityByOperationShiftId() {
        return operationShiftEntityByOperationShiftId;
    }

    public void setOperationShiftEntityByOperationShiftId(OperationShiftEntity operationShiftEntityByOperationShiftId) {
        this.operationShiftEntityByOperationShiftId = operationShiftEntityByOperationShiftId;
    }

}
