package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

@Deprecated
public class OperationShift {
    private Integer operationShiftId;
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer operationId;
    private Integer driverId;
    private Integer busId;
    private Driver driver;
    private LinkedList<ShiftRide> shiftRides;

    public OperationShift() {}

    public OperationShift(Integer operationShiftId, LocalDate date, LocalTime startTime, LocalTime endTime, Integer operationId, Integer driverId, Integer busId) {
        this.operationShiftId = operationShiftId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.operationId = operationId;
        this.driverId = driverId;
        this.busId = busId;
    }

    @TestOnly


    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public LinkedList<ShiftRide> getShiftRides() {
        return shiftRides;
    }

    public void setShiftRides(LinkedList<ShiftRide> shiftRides) {
        this.shiftRides = shiftRides;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @Override
    public String toString() {
        return "OperationShift{" +
                "operationShiftId=" + operationShiftId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", operationId=" + operationId +
                ", driverId=" + driverId +
                ", busId=" + busId +
                ", driver=" + driver +
                ", shiftRides=" + shiftRides.size() +
                '}';
    }
}
