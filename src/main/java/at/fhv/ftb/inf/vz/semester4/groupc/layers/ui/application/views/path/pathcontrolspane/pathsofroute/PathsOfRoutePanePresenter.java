package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.Pair;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.PathCreator;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.PathControlsPanePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.old.stationpane.StationPanePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.old.stationpane.StationPaneView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.StationDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.DragAndDropManager;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Supplier;
import javax.inject.Inject;

/**
 * PathsOfRoutePanePresenter.
 * Views path of a Route.
 * normally two paths: Trip to and Trip back.
 */
@LoggerableClassDisplayName("[PathsOfRoutePanePresenter]")
public class PathsOfRoutePanePresenter implements Initializable {
    @Inject
    private static Loggerable logger;

    @FXML
    private TableView<StationDTO> pathToTableView;
    @FXML
    @Deprecated
    private TableColumn<StationDTO, Integer> toNumOnPathCol;
    @FXML
    private TableColumn<StationDTO, String> toShortNameCol;
    @FXML
    private TableColumn<StationDTO, String> toNameCol;
    @FXML
    private TableColumn<StationDTO, Integer> toNumberCol;
    @FXML
    private TableColumn<StationDTO, String>  toActionCol;
    @FXML
    private TableView<StationDTO> pathBackTableView;
    @FXML
    @Deprecated
    private TableColumn<StationDTO, Integer> backNumOnPathCol;
    @FXML
    private TableColumn<StationDTO, String> backShortNameCol;
    @FXML
    private TableColumn<StationDTO, String> backNameCol;
    @FXML
    private TableColumn<StationDTO, Integer> backNumberCol;
    @FXML
    private TableColumn<StationDTO, String>  backActionCol;
    @FXML
    private Button validateBackBtn;
    @FXML
    private Button validateToBtn;
    @FXML
    private Button saveToBtn;
    @FXML
    private Button saveBackBtn;

    private PathControlsPanePresenter wrapperPresenter;
    private Supplier<FontAwesomeIconView> questionIconSupplier = () -> new FontAwesomeIconView(FontAwesomeIcon.
            QUESTION_CIRCLE);
    private Supplier<FontAwesomeIconView> crossIconSupplier = () -> new FontAwesomeIconView(FontAwesomeIcon.
            TIMES_CIRCLE_ALT);
    private Supplier<FontAwesomeIconView> checkIconSupplier = () -> new FontAwesomeIconView(FontAwesomeIcon.
            CHECK_CIRCLE_ALT);


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("initialize");
        initTableView(pathToTableView, toNumOnPathCol, toShortNameCol, toNameCol, toNumberCol, toActionCol);
        initTableView(pathBackTableView, backNumOnPathCol, backShortNameCol, backNameCol, backNumberCol, backActionCol);
        logger.debug("initialized");
    }

    private void initTableView(@NotNull TableView<StationDTO> tableView,
                               @NotNull TableColumn<StationDTO, Integer> numOnPathCol,
                               @NotNull TableColumn<StationDTO, String> shortNameCol,
                               @NotNull TableColumn<StationDTO, String> nameCol,
                               @NotNull TableColumn<StationDTO, Integer> numberCol,
                               @NotNull TableColumn<StationDTO, String> actionCol) {
        numOnPathCol.setCellValueFactory(p -> new ReadOnlyObjectWrapper(tableView.getItems().indexOf(p.getValue())));
        shortNameCol.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getShortName()));
        nameCol.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getStationName()));
        numberCol.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue().getStationId()));
        actionCol.setCellValueFactory(ignored -> null);
        actionCol.setCellFactory(createCallBack());
        initSetDragAndDropListeners(tableView);
    }

    @NotNull
    @Contract(value = " -> new", pure = true)
    private Callback<TableColumn<StationDTO, String>, TableCell<StationDTO, String>> createCallBack() {
        return new Callback<TableColumn<StationDTO, String>, TableCell<StationDTO, String>>() {
            @Override
            public TableCell<StationDTO, String> call(final TableColumn<StationDTO, String> param) {
                return new TableCell<StationDTO, String>() {

                    final FontAwesomeIconView arrowUp = new FontAwesomeIconView(FontAwesomeIcon.ARROW_UP);
                    final Button upBtn = new Button("", arrowUp);
                    final FontAwesomeIconView arrowDown = new FontAwesomeIconView(FontAwesomeIcon.ARROW_DOWN);
                    final Button downBtn = new Button("", arrowDown);
                    final FontAwesomeIconView remove = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                    final Button removeBtn = new Button("", remove);

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            upBtn.setOnAction(event -> {
                                int index = getIndex();
                                StationDTO station = getTableView().getItems().remove(index);
                                index = (index - 1 > -1) ? index - 1 : 0;
                                getTableView().getItems().add(index, station);
                                PathCreationInfo info = (getTableView() == pathToTableView) ?
                                        wrapperPresenter.getPathToInfo() : wrapperPresenter.getPathBackInfo();
                                info.setCreator(PathCreator.of(getTableView().getItems()));
                                logger.debug("Up: " + station);

                            });
                            downBtn.setOnAction(event -> {
                                int index = getIndex();
                                ObservableList<StationDTO> items = getTableView().getItems();
                                StationDTO station = items.remove(index);
                                index = (index + 1 < items.size()) ? index + 1 : items.size(); //size() as addLast()
                                items.add(index, station);
                                PathCreationInfo info = (getTableView() == pathToTableView) ?
                                        wrapperPresenter.getPathToInfo() : wrapperPresenter.getPathBackInfo();
                                info.setCreator(PathCreator.of(getTableView().getItems()));
                                logger.debug("Down: " + station);
                            });
                            removeBtn.setOnAction(event -> {
                                StationDTO station = getTableView().getItems().get(getIndex());
                                getTableView().getItems().remove(station);
                                PathCreationInfo info = (getTableView() == pathToTableView) ?
                                        wrapperPresenter.getPathToInfo() : wrapperPresenter.getPathBackInfo();
                                info.setCreator(PathCreator.of(getTableView().getItems()));
                                logger.debug("DELETE: " + station);
                            });
                            upBtn.setStyle("-fx-background-color: none");
                            downBtn.setStyle("-fx-background-color: none");
                            removeBtn.setStyle("-fx-background-color: none");
                            HBox hBox = new HBox(upBtn, downBtn, removeBtn);
                            hBox.setSpacing(3);
                            setGraphic(hBox);
                            setText(null);
                        }
                    }
                };
            }
        };
    }

    public void setWrapperPresenter(PathControlsPanePresenter wrapperPresenter) {
        this.wrapperPresenter = wrapperPresenter;
    }

    public void refreshUp() {
        wrapperPresenter.refreshFromBelow();
    }

    public void refresh() {
        logger.debug("Refresh");
        PathCreationInfo pathToInfo = wrapperPresenter.getPathToInfo();
        PathCreationInfo pathBackInfo = wrapperPresenter.getPathBackInfo();
        setPathToStations(pathToInfo.getCreator());
        setPathBackStations(pathBackInfo.getCreator());
        validateButtonListeners(pathToInfo, validateToBtn);
        validateButtonListeners(pathBackInfo, validateBackBtn);
        validateToBtn.setGraphic((pathToInfo.isHasChanged()) ? questionIconSupplier.get() :
                (pathToInfo.isIsValid()) ? checkIconSupplier.get() : crossIconSupplier.get());
        validateBackBtn.setGraphic((pathBackInfo.isHasChanged()) ? questionIconSupplier.get() :
                (pathBackInfo.isIsValid()) ? checkIconSupplier.get() : crossIconSupplier.get());
        saveBtnChangeListenerSetup(pathToInfo, saveToBtn);
        saveBtnChangeListenerSetup(pathBackInfo, saveBackBtn);
        //refreshDown(); has no children presenter
    }

    private void saveBtnChangeListenerSetup(@NotNull PathCreationInfo pathInfo, @NotNull Button button) {
        pathInfo.registerIsSavedChangeListener((observable, oldValue, newValue) -> {
            logger.debug("Change Listener For IsSave Property has fired");
            button.setVisible(!newValue);
        });
    }

    private void validateButtonListeners(@NotNull PathCreationInfo pathInfo, @NotNull Button button) {
        pathInfo.registerIsValidChangeListener((observable, oldValue, newValue) -> {
            logger.debug("Change Listener For IsValid Property has fired");
            if (newValue) {
                button.setGraphic(checkIconSupplier.get());
            } else {
                button.setGraphic(crossIconSupplier.get());
            }
        });
        pathInfo.registerHasChangedChangeListener((observable, oldValue, newValue) -> {
            logger.debug("Change Listener For hasChanged Property has fired");
            if (newValue) {
                button.setGraphic(questionIconSupplier.get());
            }
        });
    }

    private void setPathToStations(PathCreator creator) {
        if (creator != null) {
            pathToTableView.setItems(
                    FXCollections.observableArrayList(
                            StationDTOMapper.INSTANCE.toDTOs(creator.getAllStations())
                    ));
        }
    }

    private void setPathBackStations(PathCreator creator) {
        if (creator != null) {
            pathBackTableView.setItems(
                    FXCollections.observableArrayList(
                            StationDTOMapper.INSTANCE.toDTOs(creator.getAllStations())
                    ));
        }
    }

    @Deprecated
    private LinkedList<Node> getPathElements(@NotNull PathCreator pathCreator) {
        LinkedList<Map.Entry<Station, Pair<Station, LinkedList<StationConnector>>>> consAsList;
        LinkedList<Node> nodes = new LinkedList<>();
        Map.Entry<Station, Pair<Station, LinkedList<StationConnector>>> entry;
        HashMap<Station, Pair<Station, LinkedList<StationConnector>>> connections = pathCreator.getConnections();
        consAsList = new LinkedList<>(connections.entrySet());
        LinkedList<Station> allStations = pathCreator.getAllStations();
        consAsList.sort(Comparator.comparingInt(o -> allStations.indexOf(o.getKey())));
        for (int i = 0; i < consAsList.size(); i++) {
            entry = consAsList.get(i);
            StationPaneView stationPaneView = new StationPaneView();
            StationPanePresenter presenter = (StationPanePresenter) stationPaneView.getPresenter();
            presenter.setLabelsStationDTO(new StationDTO(entry.getKey().getStationId(),
                    entry.getKey().getStationName(), entry.getKey().getShortName()));
            nodes.add(stationPaneView.getView());

            if (i + 1 == consAsList.size()) {     //on last element add end station
                StationPaneView stationPaneView2 = new StationPaneView();
                StationPanePresenter presenter2 = (StationPanePresenter) stationPaneView2.getPresenter();
                presenter2.setLabelsStationDTO(new StationDTO(entry.getKey().getStationId(),
                        entry.getKey().getStationName(), entry.getKey().getShortName()));
                nodes.add(stationPaneView2.getView());
            }
        }
        return nodes;
    }

    private void initSetDragAndDropListeners(@NotNull TableView<StationDTO> tableView) {
        tableView.setOnDragEntered(Event::consume);
        tableView.setOnDragDropped(e -> {
            logger.debug("Drag Dropped");
            logger.debug(e.getGestureSource().toString());
            String string = e.getDragboard().getString();
            Object o = DragAndDropManager.get(string);
            if (o instanceof StationDTO) {
                logger.debug("Add Station to List");
                StationDTO beforeDTO = tableView.getItems().get(tableView.getItems().size() - 1);
                PathCreationInfo info = (tableView == pathToTableView) ?
                        wrapperPresenter.getPathToInfo() : wrapperPresenter.getPathBackInfo();
                boolean success = info.addStation(beforeDTO, (StationDTO) o);
                if (success) {
                    tableView.getItems().add(((StationDTO) o));
                } else {
                    logger.debug("Station could not be added");
                    //TODO Send Notification
                }
            }
            //DragAndDropManager.remove(o);
            e.consume();
        });

        tableView.setOnDragOver(e -> {
            e.acceptTransferModes(TransferMode.ANY);
            e.consume();
        });
        tableView.setOnDragDone(e -> {
            logger.debug("Drag done");
            e.getDragboard().clear();
            e.setDropCompleted(true);
            e.consume();
        });
    }

    @FXML
    private void saveToPath() {
        logger.debug("UI CALL SAVE PATH TO");
        wrapperPresenter.getPathToInfo().save(wrapperPresenter.getCurrentRoute(), true);
    }

    @FXML
    private void saveBackPath() {
        logger.debug("UI CALL SAVE PATH BACK");
        wrapperPresenter.getPathToInfo().save(wrapperPresenter.getCurrentRoute(), false);
    }

    @FXML
    private void validateToPath() {
        logger.debug("UI CALL VALIDATE PATH TO");
        boolean validate = wrapperPresenter.getPathToInfo().validate();
        if (!validate) {
            pathNeedsTwoStationsMassage();
        }
    }

    @FXML
    private void validateBackPath() {
        logger.debug("UI CALL VALIDATE PATH BACK");
        boolean validate = wrapperPresenter.getPathBackInfo().validate();
        if (!validate) {
            pathNeedsTwoStationsMassage();
        }
    }

    private void pathNeedsTwoStationsMassage() {
        logger.debug("Path needs at least 2 stations to be validated");
    }
}
