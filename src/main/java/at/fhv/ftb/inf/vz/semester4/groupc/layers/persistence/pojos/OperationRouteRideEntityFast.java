package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@SuppressWarnings({"JpaDataSourceORMInspection"})
@Entity
@Table(name = "operation_ride", schema = "public", catalog = "kmobil")
public class OperationRouteRideEntityFast implements DatabaseEntityMarker {
    private Integer operationId;
    private FlatOperationEntity operation;
    private Integer routeRideId;
    private RouteRideEntityFast routeRide;
    //private Integer operationShiftId;
    //private OperationShiftEntity operationShift;    //TODO One to Many


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_ride_id")
    public Integer getRouteRideId() {
        return routeRideId;
    }

    public void setRouteRideId(Integer routeRideId) {
        this.routeRideId = routeRideId;
    }

    @Basic
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    /*
    @Basic
    @Column(name = "operation_shift_id")
    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

     */


    @OneToOne
    @JoinColumn(name = "operation_id", referencedColumnName = "operation_id", nullable = false, insertable = false, updatable = false)
    public FlatOperationEntity getOperation() {
        return operation;
    }

    public void setOperation(FlatOperationEntity operation) {
        this.operation = operation;
    }

    @OneToOne
    @JoinColumn(name = "route_ride_id", referencedColumnName = "route_ride_id", nullable = false, insertable = false, updatable = false)
    public RouteRideEntityFast getRouteRide() {
        return routeRide;
    }

    public void setRouteRide(RouteRideEntityFast routeRide) {
        this.routeRide = routeRide;
    }

    /*
    @OneToOne
    @JoinColumn(name = "operation_shift_id", referencedColumnName = "operation_shift_id", nullable = false, insertable = false, updatable = false)
    public OperationShiftEntity getOperationShift() {
        return operationShift;
    }

    public void setOperationShift(OperationShiftEntity operationShift) {
        this.operationShift = operationShift;
    }
     */

    @Override
    public String toString() {
        return "OperationRouteRideEntityFast{" +
                "operationId=" + operationId +
                ", operation=" + operation +
                ", routeRideId=" + routeRideId +
                ", routeRide=" + routeRide +
              //  ", operationShiftId=" + operationShiftId +
                //", operationShift=" + operationShift +
                '}';
    }
}

