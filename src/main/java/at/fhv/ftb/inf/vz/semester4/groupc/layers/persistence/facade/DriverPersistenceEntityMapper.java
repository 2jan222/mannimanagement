package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class DriverPersistenceEntityMapper<DriverEntity> extends PersistenceEntityMapper<DriverEntity> {
    @Override
    public DriverEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (DriverEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.DriverEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("DriverEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<DriverEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from DriverEntity ");
            return (List<DriverEntity>) query.getResultList();
        }
    }

    @Override
    public List<DriverEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from DriverEntity " + whereQueryFragment);
            return (List<DriverEntity>) query.getResultList();
        }
    }
}
