package at.fhv.ftb.inf.vz.semester4.groupd.domain;

public enum ChangeStatus {
	OK("ok"), CHANGED("changed, not saved");
	
	private String _value;
	
	private ChangeStatus(String value) {
		_value = value;
	}
	
	public String toString() {
		return _value;
	}
}
