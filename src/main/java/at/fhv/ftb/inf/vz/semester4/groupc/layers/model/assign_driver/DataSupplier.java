package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.DriverEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.FlatOperationEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntryEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.DriverEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatOperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationShiftEntity;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.persistence.PersistenceException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unchecked")
@LoggerableClassDisplayName("[DB-Connection]")
public class DataSupplier implements AssignDriverDataTransfer {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final Loggerable logger = Loggerable.getInstance();

    @NotNull
    @Override
    public LinkedList<Driver> getDriverForDay(@NotNull LocalDate date) {
        logger.debug("getDriverForDay");
        return new LinkedList<>(DriverEntityMapper
                .INSTANCE.toMojos(PersistenceFacade.getInstance().getAll(DriverEntity.class)));
    }

    @NotNull
    @Override
    public LinkedList<OperationFlat> getOperationsForDay(@NotNull LocalDate date) {
        logger.debug("getOperationsForDay");
        String where = "WHERE date = '" + date.format(formatter) + "' AND busId is not null";
        return new LinkedList<>(FlatOperationEntityMapper
                .INSTANCE.toMojos(PersistenceFacade.getInstance().getAllWhere(FlatOperationEntity.class, where)));
    }

    @NotNull
    @Override
    public LinkedList<ShiftEntry> getShiftEntriesForDay(@NotNull LocalDate date) {
        logger.debug("getShiftEntriesForDay");
        String where = "WHERE date = '" + date.format(formatter) + "'";
        return new LinkedList<>(ShiftEntryEntityMapper
                .INSTANCE.toMojos(PersistenceFacade.getInstance().getAllWhere(OperationShiftEntity.class, where)));
    }

    @NotNull
    @Override
    public LinkedList<LocalDate> getDatesWithOperationWhichHaveBus() {
        logger.debug("GetDatesWithOperationWhichHaveBus");
        LinkedList<LocalDate> dates;
        dates = (LinkedList<LocalDate>) PersistenceFacade.getInstance().executeTransaction(session -> {
            Query query = session.createQuery("Select o.date FROM OperationEntity o WHERE o.busId is not null And o.date is not null");
            Stream<Object> resultStream = query.getResultStream();
            return resultStream.collect(Collectors.toCollection(LinkedList::new));
        });
        return (dates != null) ? dates : new LinkedList<>();
    }

    @Override
    public void updateDriver(Driver driver) throws AssignDriverDataTransferUpdateException {
        logger.debug("UpdateDriver " + driver);
        try {
            PersistenceFacade.getInstance().update(DriverEntityMapper.INSTANCE.toPojo(driver));
        } catch (PersistenceException pex) {
            throw new AssignDriverDataTransferUpdateException(pex);
        }
        logger.debug("UPDATE DRIVER DB");
    }

    @Override
    public void updateOrCreateShiftEntry(ShiftEntry shiftEntry) throws AssignDriverDataTransferUpdateException {
        logger.debug("UpdateOrCreateShiftEntry");
        try {
            OperationShiftEntity operationShiftEntity = ShiftEntryEntityMapper.INSTANCE.toPojo(shiftEntry);
            System.out.println("operationShiftEntity = " + operationShiftEntity);
            PersistenceFacade pf = PersistenceFacade.getInstance();
            if (shiftEntry.getShiftEntryId() != null) {
                pf.executeTransaction(session -> {
                    String sql = "UPDATE operation_shift SET " +
                            " operation_id = " + ndls(shiftEntry.getOperationId()) + "," +
                            " bus_id = " + ndls(shiftEntry.getBusID()) + "," +
                            " date = " + ndls(shiftEntry.getDate().format(formatter)) + "," +
                            " driver_id = " + ndls(shiftEntry.getDriverId()) + "," +
                            " start_time = " + ndls(Time.valueOf(shiftEntry.getStartTime())) + "," +
                            " end_time = " + ndls(Time.valueOf(shiftEntry.getEndTime())) +
                            " WHERE operation_shift_id = " + ndls(shiftEntry.getShiftEntryId());
                    System.out.println(sql);
                    NativeQuery sqlQuery = session.createSQLQuery(sql);
                    return sqlQuery.executeUpdate();
                });
            } else {
                String g = "INSERT INTO operation_shift (operation_id, bus_id, date, driver_id, start_time, end_time) VALUES("
                        + ndls(shiftEntry.getOperationId()) + ","
                        + ndls(shiftEntry.getBusID()) + ","
                        + ndls(shiftEntry.getDate()) + ","
                        + ndls(shiftEntry.getDriverId()) + ","
                        + ndls(Time.valueOf(shiftEntry.getStartTime())) + ","
                        + ndls(Time.valueOf(shiftEntry.getEndTime()))
                        + ") RETURNING operation_shift_id";
                logger.debug(g);

                pf.executeTransaction(session -> {
                    Optional op = session.createSQLQuery(g).getResultStream().findFirst();
                    if (op.isPresent()) {
                        int id = (int) op.get();
                        logger.debug("id = " + id);
                        shiftEntry.setShiftEntryId(id);
                    }
                    return null;
                });
                shiftEntry.getShiftRides().forEach(e -> {
                    e.setOperationShiftId(shiftEntry.getShiftEntryId());
                    pf.executeTransaction(s2 -> s2.createSQLQuery("INSERT INTO operation_shift_ride(operation_ride_id, operation_shift_id) VALUES ('" + e.getShiftRideId() + "', '" + e.getOperationShiftId() + "')").executeUpdate());
                });
            }
        } catch (PersistenceException pex) {
            throw new AssignDriverDataTransferUpdateException(pex);
        }
        logger.debug("UPDATE SHIFT ENTRY DB");
    }

    @NotNull
    @Contract(pure = true)
    private String ndls(Object o) {
        return (o != null) ? "'" + o + "'" : "null";
    }
}
