package at.fhv.ftb.inf.vz.semester4.groupd.domain.security;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.ChangeStatus;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.DayType;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.RouteRide;

import java.time.LocalDate;
import java.util.List;

public interface IOperation {

    public int getOperationId();

    public DayType getDayType();

    public String getName();

    public List<RouteRide> getRouteRides();

    public long getCheckSum();

    public LocalDate getDate();

    public FlatBusEntity getBus();

    public String getBusLicence();

    public ChangeStatus getStatus();
}
