package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.old.stationpane;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * StationPanePresenter.
 * Views the station after it is assigned to a path of a route.
 */
@Deprecated
public class StationPanePresenter implements Initializable {

    @FXML
    private Label stationNumberLabel;
    @FXML
    private Label stationNameLabel;
    @FXML
    private Label stationShortNameLabel;

    @Contract(pure = true)
    public StationPanePresenter() {
    }

    public void setLabelsStationDTO(@NotNull StationDTO dto) {
        setLabelStationName(dto.getStationName());
        setLabelStationNumber(dto.getStationId());
        setLabelStationShortName(dto.getShortName());
    }

    public void setLabelStationNumber(int stationNumber) {
        this.stationNumberLabel.setText(Integer.toString(stationNumber));
    }


    public void setLabelStationName(@NotNull String stationName) {
        this.stationNameLabel.setText(stationName);
    }


    public void setLabelStationShortName(String stationShortName) {
        this.stationShortNameLabel.setText(stationShortName);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
