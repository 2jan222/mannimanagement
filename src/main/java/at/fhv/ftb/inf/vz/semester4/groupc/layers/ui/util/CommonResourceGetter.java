package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.stage.Window;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Common resource getters.
 */
public class CommonResourceGetter {
    @NotNull
    @Contract(" -> new")
    public static Image getBusIcon() {
        return new Image("/ui-resources/other/bus_icon.png");
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    public static Window getWindowFromActionEvent(@NotNull ActionEvent actionEvent) {
        return ((Node) (actionEvent.getSource())).getScene().getWindow();
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    public static Window getWindowFromNode(@NotNull Node node) {
        return node.getScene().getWindow();
    }
}
