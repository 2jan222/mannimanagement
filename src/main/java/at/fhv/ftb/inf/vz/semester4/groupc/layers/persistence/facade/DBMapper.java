package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import java.util.List;

public interface DBMapper<Object> {

    void create(Object value);

    Object read(Integer id);

    void update(Object value);

    void update(String setQueryFragment, String whereQueryFragment);

    void delete(Object value);

    List<Object> getAll();

    List<Object> getAllWhere(String whereQueryFragment);

}
