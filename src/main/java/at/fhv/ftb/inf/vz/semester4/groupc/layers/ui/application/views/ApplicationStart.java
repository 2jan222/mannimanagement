package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views;


import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.error.ErrorPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.error.ErrorView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.login.LoginView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.ArgsParser;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.CommonResourceGetter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.FutureRunnable;
import com.airhacks.afterburner.injection.Injector;
import com.airhacks.afterburner.views.FXMLView;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import com.github.jan222ik.loggers.FileLogger;
import com.github.jan222ik.loggers.MultiLogger;
import com.github.jan222ik.loggers.SystemStreamLogger;
import com.github.jan222ik.loggers.VoidLogger;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Consumer;


/**
 * ApplicationStart is the start point of the application.
 * Global resources are instantiated and closed here.
 */
@LoggerableClassDisplayName("[ApplicationStart]")
public class ApplicationStart extends Application {
    private static boolean testRun = false;
    private static boolean isReset = false;
    private boolean isError = false;
    private Loggerable logger;
    private Stage primStage;


    @SuppressWarnings({"Duplicates"})
    public void externalInit(List<String> raw) {
        /*<SETUP> METHODS*/
        /*<Args Register>*/
        ArgsParser.parseArgs(raw);
        ArgsParser.checkForKeyword(
                "-test", o -> ApplicationStart.testRun = true);
        /*<Logger>*/
        setUpLogger();
        logger.debug("<init>", LoggerableColor.ANSI_BLUE);
        logger.debug("\t<setup>", LoggerableColor.ANSI_YELLOW);
        logger.debug("\t\t<Logger>", LoggerableColor.ANSI_CYAN);
        logger.debug("\t\t\tLogger Active");
        /*<\Logger>*/
        logger.debug("\t\t<\\Logger>", LoggerableColor.ANSI_CYAN);
        logger.debug("\t\t<Services>", LoggerableColor.ANSI_CYAN);
        /*<Services>*/
        logger.debug("\t\t\t<Database>", LoggerableColor.ANSI_CYAN);
        /*<Database>*/
        try {
            DatabaseConnector.init();
        } catch (ExceptionInInitializerError e) {
            e.printStackTrace();
            isError = true;
        }
        /*<\Database>*/
        logger.debug("\t\t\t<\\Database>", LoggerableColor.ANSI_CYAN);
        //Start services here
        /*<\Services>*/
        logger.debug("\t\t<\\Services>", LoggerableColor.ANSI_CYAN);
        /*</SETUP>*/
        logger.debug("\t<\\setup>", LoggerableColor.ANSI_YELLOW);
        logger.debug("\t<injectors>", LoggerableColor.ANSI_YELLOW);
        /*<INJECTOR>*/
        logger.debug("\t\t<Logger>", LoggerableColor.ANSI_CYAN);
        /*<Logger>*/
        Injector.setModelOrService(Loggerable.class, logger);
        logger.debug("\t\t\tInjector for Logger set");
        /*<\Logger>*/
        logger.debug("\t\t<\\Logger>", LoggerableColor.ANSI_CYAN);
        logger.debug("\t\t<Services>", LoggerableColor.ANSI_CYAN);
        /*<Services>*/
        //Add services to injector here
        logger.debug("\t\t\tempty");
        /*<\Services>*/
        logger.debug("\t\t<\\Services>", LoggerableColor.ANSI_CYAN);
        /*</INJECTOR>*/
        logger.debug("\t<\\injectors>", LoggerableColor.ANSI_YELLOW);
        logger.debug("\t<Other init>", LoggerableColor.ANSI_YELLOW);
        /*<OTHER>*/
        //other work before application start
        /*<\OTHER>*/
        logger.debug("\t<\\Other init>", LoggerableColor.ANSI_YELLOW);
        logger.debug("<\\init>", LoggerableColor.ANSI_BLUE);
    }

    public void externalStart(Stage primaryStage) {
        primStage = primaryStage;
        boolean connect2Database = false;
        if (!isError) {
            connect2Database = DatabaseConnector.hasConnection();
        }
        logger.info("Connection to database: " + connect2Database);
        if (connect2Database) {
            LoginView loginView = new LoginView();
            generateRoot(primaryStage, loginView, "Mannimanagement - Login", false,
                    (Consumer<Stage>) stage -> stage.setResizable(false));
        } else {
            ErrorView errorView = new ErrorView();
            ((ErrorPresenter) errorView.getPresenter()).setReason("No connection to database");
            generateRoot(primaryStage, errorView, "Mannimanagement - Connection Error", false);
        }
    }

    @Override
    public final void start(Stage primaryStage) {
        externalInit(getParameters().getRaw());
        externalStart(primaryStage);
    }

    @SuppressFBWarnings("ST_WRITE_TO_STATIC_FROM_INSTANCE_METHOD")
    public void restart() {
        Platform.runLater(new FutureRunnable(() -> externalStart(primStage)));
        isReset = true;
    }

    @Override
    public void stop() {
        logger.info("Stopping application. Closing resources.");
        /*Close Global Resources here or call close methods.*/
        Injector.forgetAll();
        if (!isError) {
            DatabaseConnector.shutdown();
        }
        logger.info("Resources closed. Stopping platform");
        logger.closeStreams();
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    @SuppressWarnings("SameParameterValue")
    @SafeVarargs
    private final void generateRoot(@NotNull Stage stage, @NotNull FXMLView view,
                                    @NotNull @Nls(capitalization = Nls.Capitalization.Title) String title,
                                    boolean undecorated,
                                    @NotNull Consumer<Stage>... functions) {
        Scene main = new Scene(view.getView());
        stage.setTitle(title);
        stage.getIcons().add(CommonResourceGetter.getBusIcon());
        for (Consumer<Stage> consumer : functions) {
            consumer.accept(stage);
        }
        if (!isReset) {
            stage.initStyle((undecorated && !testRun) ? StageStyle.UNDECORATED : StageStyle.DECORATED);
        }
        stage.setScene(main);
        stage.show();
        stage.toFront();
    }

    /**
     * Application main.
     * Known args:
     * '-debug': Enables debug print
     * '-noinfo': Disables info print
     * '-nowarnings': Disables warnings print
     * '-noerrors': Disables errors print
     * '-nofatal': Disables fatal error print
     * '-nopathlog': Disables long ClassContext paths in logger
     * '-test': for test case execution
     * @param args program arguments.
     */
    public static void main(@Nullable String... args) {
        launch(args);
    }

    @SuppressFBWarnings("RV_RETURN_VALUE_IGNORED_BAD_PRACTICE")
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void setUpLogger() {
        MultiLogger multiLogger = null;
        try {
            Loggerable systemStreamLogger = new SystemStreamLogger();
            systemStreamLogger.setDebugVisibility(false);
            ArgsParser.checkForKeyword("-debug", o -> systemStreamLogger.setDebugVisibility(true));
            File loggerOutput = new File("target/logs/log"
                    + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy_hh.mm.ss"))
                    + ".txt");
            loggerOutput.getParentFile().mkdirs();
            loggerOutput.createNewFile();
            FileLogger fileLogger = new FileLogger(new PrintWriter(loggerOutput, "utf-8"));
            multiLogger = new MultiLogger(fileLogger, systemStreamLogger);
            logger = multiLogger;
            logger.setChainPath(true);
            logger.setMaxDepth(20);
        } catch (IOException e) {
            logger = new VoidLogger();
        }
        Loggerable.setLogger(logger);
        if (multiLogger != null) {
            multiLogger.setDebugVisibility(false);
        }
        logger.setDebugVisibility(false);

        ArgsParser.checkForKeyword("-debug", o -> logger.setDebugVisibility(true));
        ArgsParser.checkForKeyword("-noinfo", o -> logger.setInfoVisibility(false));
        ArgsParser.checkForKeyword("-nowarnings", o -> logger.setWarnVisibility(false));
        ArgsParser.checkForKeyword("-noerrors", o -> logger.setErrorVisibility(false));
        ArgsParser.checkForKeyword("-nofatal", o -> logger.setFatalVisibility(false));
        ArgsParser.checkForKeyword("-nopathlog",
                o -> {
                    logger.setChainPath(false);
                    logger.setMaxDepth(6);
                });
    }
}
