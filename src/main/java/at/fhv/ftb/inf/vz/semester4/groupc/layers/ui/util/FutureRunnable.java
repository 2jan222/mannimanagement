package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util;

import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static com.github.jan222ik.LoggerableColor.ANSI_YELLOW;

@LoggerableClassDisplayName(value = "[FUTURE]", color = ANSI_YELLOW)
public class FutureRunnable implements Runnable {
    private Runnable runnable;

    @Contract(pure = true)
    public FutureRunnable(@NotNull Runnable runnable) {
        this.runnable = runnable;
    }

    public void run() {
        runnable.run();
    }
}
