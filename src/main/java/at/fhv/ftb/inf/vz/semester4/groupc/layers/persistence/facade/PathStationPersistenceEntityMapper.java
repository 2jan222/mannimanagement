package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class PathStationPersistenceEntityMapper<PathStationEntity> extends PersistenceEntityMapper<PathStationEntity> {
    @Override
    public PathStationEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (PathStationEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("PathStationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<PathStationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from PathStationEntity ");
            return (List<PathStationEntity>) query.getResultList();
        }
    }

    @Override
    public List<PathStationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from PathStationEntity " + whereQueryFragment);
            return (List<PathStationEntity>) query.getResultList();
        }
    }


}
