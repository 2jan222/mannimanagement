package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.Contract;

public class StationConnector {
    private Integer connectionId;
    private Station start;
    private Station end;
    private Integer distanceFromPrevious;
    private Integer timeFromPrevious;
    private Integer startId;
    private Integer endId;
    private Integer pathStationId;


    @Contract(pure = true)
    public StationConnector() {
    }

    @Contract(pure = true)
    public StationConnector(Station start, Station end, Integer distanceFromPrevious, Integer timeFromPrevious) {
        this.start = start;
        this.end = end;
        this.distanceFromPrevious = distanceFromPrevious;
        this.timeFromPrevious = timeFromPrevious;
    }


    public Station getStart() {
        return start;
    }

    public void setStart(Station start) {
        this.start = start;
    }

    public Station getEnd() {
        return end;
    }

    public void setEnd(Station end) {
        this.end = end;
    }

    public Integer getDistanceFromPrevious() {
        return distanceFromPrevious;
    }

    public void setDistanceFromPrevious(Integer distanceFromPrevious) {
        this.distanceFromPrevious = distanceFromPrevious;
    }

    public Integer getTimeFromPrevious() {
        return timeFromPrevious;
    }

    public void setTimeFromPrevious(Integer timeFromPrevious) {
        this.timeFromPrevious = timeFromPrevious;
    }

    @Override
    public String toString() {
        return "StationConnector{" +
                "connectionId=" + connectionId +
                ", start=" + start +
                ", end=" + end +
                ", distanceFromPrevious=" + distanceFromPrevious +
                ", timeFromPrevious=" + timeFromPrevious +
                '}';
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public Integer getStartId() {
        return startId;
    }

    public void setStartId(Integer startId) {
        this.startId = startId;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    public Integer getPathStationId() {
        return pathStationId;
    }

    public void setPathStationId(Integer pathStationId) {
        this.pathStationId = pathStationId;
    }


}
