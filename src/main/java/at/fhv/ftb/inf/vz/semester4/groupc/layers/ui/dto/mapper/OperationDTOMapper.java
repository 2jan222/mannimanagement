package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Operation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.OperationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * OperationDTO mapper.
 */
@Mapper(uses = {RouteRideDTOMapper.class, BusDTOMapper.class})
public interface OperationDTOMapper {
    OperationDTOMapper INSTANCE = Mappers.getMapper(OperationDTOMapper.class);

    @Mapping(target = "routeRideDTOS", source = "routeRides")
    OperationDTO toDTO(Operation operation);

    @Mapping(target = "routeRides", source = "routeRideDTOS")
    Operation toMojo(OperationDTO operationDTO);

    @Mapping(target = "routeRideDTOS", source = "routeRides")
    OperationDTO updateDTO(Operation operation, @MappingTarget OperationDTO operationDTO);

    @Mapping(target = "routeRides", source = "routeRideDTOS")
    Operation updateMojo(OperationDTO operationDTO, @MappingTarget Operation operation);

    List<OperationDTO> toDTOs(List<Operation> operations);

    List<Operation> toMojos(List<OperationDTO> operationDTOS);
}



