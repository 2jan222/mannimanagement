package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

@SuppressWarnings("WeakerAccess")
public class AssignDriverContextCreationException extends Exception {
    public AssignDriverContextCreationException(String msg) {
        super(msg);
    }
}
