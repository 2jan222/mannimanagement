package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftRideDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ShiftRideDTOMapper {

    ShiftRideDTOMapper INSTANCE = Mappers.getMapper(ShiftRideDTOMapper.class);

    @Mapping(target = "startStationName", source = "start.shortName")
    @Mapping(target = "endStationName", source = "end.shortName")
    ShiftRideDTO toDTO(ShiftRide shiftRide);

    @Mapping(target = "start", ignore = true)
    @Mapping(target = "end", ignore = true)
    ShiftRide toMojo(ShiftRideDTO shiftRideDTO);

    @Mapping(target = "startStationName", source = "start.shortName")
    @Mapping(target = "endStationName", source = "end.shortName")
    void updateDTO(ShiftRide shiftRide, @MappingTarget ShiftRideDTO shiftRideDTO);

    @Mapping(target = "start", ignore = true)
    @Mapping(target = "end", ignore = true)
    void updateMojo(ShiftRideDTO shiftRideDTO, @MappingTarget ShiftRide shiftRide);

    List<ShiftRideDTO> toDTOs(List<ShiftRide> shiftRides);

    List<ShiftRide> toMojos(List<ShiftRideDTO> shiftRideDTOS);
}
