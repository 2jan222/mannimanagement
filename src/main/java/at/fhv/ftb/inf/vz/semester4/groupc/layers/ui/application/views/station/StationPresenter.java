package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.station;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.stationmanagement.StationCRUDService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.station.create.StationCreateView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.CommonResourceGetter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.PopupWindowCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ResourceBundle;
import javax.inject.Inject;

/**
 * Presenter for all StationOperations.
 */
@LoggerableClassDisplayName("[StationPresenter]")
public class StationPresenter implements MenuViewItem, Initializable {

    @Inject
    private StationCRUDService crudService;
    @FXML
    private TableView<StationDTO> table;
    @FXML
    private TableColumn<StationDTO, String> nameCol;
    @FXML
    private TableColumn<StationDTO, Long> idCol;
    @FXML
    private TableColumn<StationDTO, String> shortNameCol;
    @FXML
    private TableColumn<StationDTO, String> descriptionCol;
    @FXML
    private TableColumn actionCol;
    @Inject
    private static Loggerable logger;

    @Override
    public String getMenuDisplayName() {
        return "Stop Management";
    }

    public void addStationAction(ActionEvent actionEvent) {
        StationCreateView stationCreateView = new StationCreateView();
        Stage popupWindow = PopupWindowCreator.createWindow(
                stationCreateView,
                "Stop management - Add stop",
                CommonResourceGetter.getBusIcon(),
                CommonResourceGetter.getWindowFromActionEvent(actionEvent),
                Modality.WINDOW_MODAL);
        popupWindow.show();
    }

    public void init() {
        /*
        nameCol.setMaxWidth(1f * Integer.MAX_VALUE * 25);
        idCol.setMaxWidth(1f * Integer.MAX_VALUE * 25);
        shortNameCol.setMaxWidth(1f * Integer.MAX_VALUE * 25);
        descriptionCol.setMaxWidth(1f * Integer.MAX_VALUE * 25);
        nameCol.setPrefWidth(1f * Integer.MAX_VALUE * 25);
        idCol.setPrefWidth(1f * Integer.MAX_VALUE * 25);
        shortNameCol.setPrefWidth(1f * Integer.MAX_VALUE * 25);
        descriptionCol.setPrefWidth(1f * Integer.MAX_VALUE * 25);
        */
        nameCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getStationName()));
        idCol.setCellValueFactory(cellData -> new SimpleLongProperty(cellData.getValue().getStationId()).asObject());
        shortNameCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getShortName()));
        actionCol.setCellValueFactory(ignored -> null);
        actionCol.setCellFactory(createCallBack());
        table.setItems(crudService.getStationObservables());
    }


    @NotNull
    @Contract(value = " -> new", pure = true)
    private static Callback<TableColumn<StationDTO, String>, TableCell<StationDTO, String>> createCallBack() {
        return new Callback<TableColumn<StationDTO, String>, TableCell<StationDTO, String>>() {
            @Override
            public TableCell call(final TableColumn<StationDTO, String> param) {
                return new TableCell<StationDTO, String>() {

                    final Button editBtn = new Button("Edit");
                    final Button trashBtn = new Button("Trash");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            editBtn.setOnAction(event -> {
                                StationDTO station = getTableView().getItems().get(getIndex());
                                logger.debug("EDIT: " + station);
                            });
                            trashBtn.setOnAction(event -> {
                                StationDTO station = getTableView().getItems().get(getIndex());
                                logger.debug("DELETE: " + station);
                            });
                            setGraphic(new HBox(editBtn, trashBtn));
                            setText(null);
                        }
                    }
                };
            }
        };
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }
}
