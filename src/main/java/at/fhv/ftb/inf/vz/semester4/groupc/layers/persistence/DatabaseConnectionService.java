package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence;


/**
 * Service Class for direct database interactions from UI.
 */
public class DatabaseConnectionService {
    /**
     * Returns status of connection.
     *
     * @return true, if a connection to database exists.
     */
    public static boolean checkForConnection() {
        return DatabaseConnector.hasConnection();
    }
}
