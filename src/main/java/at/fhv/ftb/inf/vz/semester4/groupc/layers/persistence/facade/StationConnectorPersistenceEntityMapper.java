package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StationConnectorPersistenceEntityMapper<StationConnectorEntity> extends PersistenceEntityMapper<StationConnectorEntity> {
    @Override
    public StationConnectorEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (StationConnectorEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationConnectorEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("StationConnectorEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<StationConnectorEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from StationConnectorEntity ");
            return (List<StationConnectorEntity>) query.getResultList();
        }
    }

    @Override
    public List<StationConnectorEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from StationConnectorEntity " + whereQueryFragment);
            return (List<StationConnectorEntity>) query.getResultList();
        }
    }
}
