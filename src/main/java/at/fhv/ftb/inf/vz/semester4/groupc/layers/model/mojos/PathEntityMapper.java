package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = PathStationEntityMapper.class)
public interface PathEntityMapper {
    PathEntityMapper INSTANCE = Mappers.getMapper(PathEntityMapper.class);


    @Mapping(target = "pathStations", source = "pathStationsByPathId")
    Path toMojo(PathEntity pathEntity);

    @Mapping(target = "routeByRouteId", ignore = true)
    @Mapping(target = "startTimesByPathId", ignore = true)
    @Mapping(target = "pathDescription", ignore = true)
    @Mapping(target = "pathStationsByPathId", source = "pathStations")
    PathEntity toPojo(Path path);

    @Mapping(target = "pathStations", source = "pathStationsByPathId")
    void updateMojo(PathEntity pathEntity, @MappingTarget Path path);

    @Mapping(target = "routeByRouteId", ignore = true)
    @Mapping(target = "startTimesByPathId", ignore = true)
    @Mapping(target = "pathDescription", ignore = true)
    @Mapping(target = "pathStationsByPathId", source = "pathStations")
    void updatePojo(Path path, @MappingTarget PathEntity pathEntity);

    List<Path> toMojos(List<PathEntity> pathEntities);

    List<PathEntity> toPojos(List<Path> paths);

}
