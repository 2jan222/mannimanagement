package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public abstract class PersistenceEntityMapper<Object> implements DBMapper<Object> {

    protected static void updateHelper(String table, String setString, String whereString) {
        System.out.println("Query = " + "UPDATE " + table + " " + setString + " " + whereString);
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            Query query = sess.createQuery("UPDATE " + table + " " + setString + " " + whereString);
            query.executeUpdate();
            sess.flush();
            tx.commit();
        }
    }

    @Override
    public void create(Object value) {
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            sess.save(value);
            sess.flush();
            tx.commit();
        }

    }


    @Override
    abstract public Object read(Integer id);

    @Override
    public void update(Object value) {
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            sess.saveOrUpdate(value);
            sess.flush();
            tx.commit();
        }
    }

    @Override
    public abstract void update(String setQueryFragment, String whereQueryFragment);

    @Override
    public void delete(Object value) {
        Transaction tx;
        try (Session sess = DatabaseConnector.getSession()) {
            tx = sess.beginTransaction();
            sess.delete(value);
            sess.flush();
            tx.commit();
        }

    }

    @Override
    abstract public List<Object> getAll();

    @Override
    abstract public List<Object> getAllWhere(String whereQueryFragment);
}
