package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.DriverDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DriverDTOMapper {

    DriverDTOMapper INSTANCE = Mappers.getMapper(DriverDTOMapper.class);

    DriverDTO toDTO(Driver driver);

    Driver toMojo(DriverDTO driverDTO);

    DriverDTO updateDTO(Driver driver, @MappingTarget DriverDTO driverDTO);

    void updateMojo(DriverDTO driverDTO, @MappingTarget Driver driver);

    List<DriverDTO> toDTOs(List<Driver> drivers);

    List<Driver> toMojos(List<DriverDTO> driverDTOS);
}
