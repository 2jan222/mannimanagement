package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.util.Objects;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Table(name = "path_station", schema = "public", catalog = "kmobil")

public class PathStationEntity implements DatabaseEntityMarker {
    private int pathStationId;
    private int pathId;
    private Integer stationConnectorId;
    private int positionOnPath;
    private PathEntity pathByPathId;
    private StationConnectorEntity stationConnector;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "path_station_id")
    public int getPathStationId() {
        return pathStationId;
    }

    public void setPathStationId(int pathStationId) {
        this.pathStationId = pathStationId;
    }

    @Basic
    @Column(name = "position_on_path")
    public int getPositionOnPath() {
        return positionOnPath;
    }

    public void setPositionOnPath(int positionOnPath) {
        this.positionOnPath = positionOnPath;
    }


    @Column(name = "path_id")
    public int getPathId() {
        return pathId;
    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    @Column(name="station_connection_id")
    public Integer getStationConnectorId() {
        return stationConnectorId;
    }

    public void setStationConnectorId(Integer stationConnectorId) {
        this.stationConnectorId = stationConnectorId;
    }




    @ManyToOne
    @JoinColumn(name = "path_id", referencedColumnName = "path_id", nullable = false, insertable = false, updatable = false)
    public PathEntity getPathByPathId() {
        return pathByPathId;
    }

    public void setPathByPathId(PathEntity pathByPathId) {
        this.pathByPathId = pathByPathId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PathStationEntity)) return false;
        PathStationEntity that = (PathStationEntity) o;
        return pathStationId == that.pathStationId &&
                pathId == that.pathId &&
                positionOnPath == that.positionOnPath &&
                pathByPathId.equals(that.pathByPathId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pathStationId, pathId, positionOnPath, pathByPathId);
    }

    @Override
    public String toString() {
        return "PathStationEntity{" +
                "pathStationId=" + pathStationId +
                ", pathId=" + pathId +
                ", stationConnectorId=" + stationConnectorId +
                ", positionOnPath=" + positionOnPath +
                ", pathByPathId=" + pathByPathId +
                ", stationConnector=" + stationConnector +
                '}';
    }

    @OneToOne
    @JoinColumn(name = "station_connection_id", updatable = false, insertable = false)
    public StationConnectorEntity getStationConnector() {
        return stationConnector;
    }

    public void setStationConnector(StationConnectorEntity stationConnector) {
        this.stationConnector = stationConnector;
    }
}
