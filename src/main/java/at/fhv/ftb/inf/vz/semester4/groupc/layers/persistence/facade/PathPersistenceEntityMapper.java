package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class PathPersistenceEntityMapper<PathEntity> extends PersistenceEntityMapper<PathEntity> {
    @Override
    public PathEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (PathEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("PathEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<PathEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from PathEntity ");
            return (List<PathEntity>) query.getResultList();
        }
    }

    @Override
    public List<PathEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from PathEntity " + whereQueryFragment);
            return (List<PathEntity>) query.getResultList();
        }
    }
}