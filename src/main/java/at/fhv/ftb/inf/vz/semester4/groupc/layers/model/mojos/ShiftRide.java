package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.time.LocalTime;

import static java.time.temporal.ChronoUnit.SECONDS;

public class ShiftRide {
    private Integer shiftRideId;
    private Station start;
    private Station end;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer operationId;
    private Integer operationShiftId;
    private LocalDate date;

    @Contract(pure = true)
    public ShiftRide() {
    }


    @Contract(pure = true)
    @TestOnly
    public ShiftRide(Integer shiftRideId, Station start, Station end, LocalTime startTime, LocalTime endTime,
                     Integer operationId, Integer operationShiftId, LocalDate date) {
        this.shiftRideId = shiftRideId;
        this.start = start;
        this.end = end;
        this.startTime = startTime;
        this.endTime = endTime;
        this.operationId = operationId;
        this.operationShiftId = operationShiftId;
        this.date = date;
    }




    public Integer getShiftRideId() {
        return shiftRideId;
    }

    public void setShiftRideId(Integer shiftRideId) {
        this.shiftRideId = shiftRideId;
    }

    public Station getStart() {
        return start;
    }

    public void setStart(Station start) {
        this.start = start;
    }

    public Station getEnd() {
        return end;
    }

    public void setEnd(Station end) {
        this.end = end;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public long getDuration() {
        return SECONDS.between(startTime, endTime);
    }

    @Override
    public String toString() {
        return "ShiftRide{" +
                "shiftRideId=" + shiftRideId +
                ", start=" + start +
                ", end=" + end +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", operationId=" + operationId +
                ", operationShiftId=" + operationShiftId +
                ", date=" + date +
                '}';
    }
}
