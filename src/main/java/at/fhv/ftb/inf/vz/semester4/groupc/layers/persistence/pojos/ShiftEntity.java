package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "shift", schema = "public", catalog = "kmobil")
public class ShiftEntity implements DatabaseEntityMarker {
    private int shiftId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shift_id")
    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShiftEntity that = (ShiftEntity) o;
        return shiftId == that.shiftId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(shiftId);
    }

    @Override
    public String toString() {
        return "ShiftEntity{" +
                "shiftId=" + shiftId +
                '}';
    }
}
