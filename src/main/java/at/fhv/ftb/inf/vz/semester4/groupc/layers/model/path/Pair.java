package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path;

import org.jetbrains.annotations.Contract;

/**
 * Container for generic objects.
 * @param <K> type k known as key
 * @param <V> type v known as value
 */
public class Pair<K, V> {
    private K key;
    private V value;

    @Contract(pure = true)
    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}
