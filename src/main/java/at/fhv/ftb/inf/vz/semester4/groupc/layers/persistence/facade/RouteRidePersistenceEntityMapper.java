package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class RouteRidePersistenceEntityMapper<RouteRideEntity> extends PersistenceEntityMapper<RouteRideEntity> {
    @Override
    public RouteRideEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (RouteRideEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("RouteRideEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<RouteRideEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from RouteRideEntity ");
            return (List<RouteRideEntity>) query.getResultList();
        }
    }

    @Override
    public List<RouteRideEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from RouteRideEntity " + whereQueryFragment);
            return (List<RouteRideEntity>) query.getResultList();
        }
    }
}