package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.grouping;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.RouteRide;
import org.jetbrains.annotations.Contract;

/**
 * Types of errors which can occur in Grouper.
 */

public enum GrouperErrors {
    TIME_OVERLAPPING(null);
    private RouteRide failOn;

    @Contract(pure = true)
    GrouperErrors(RouteRide ride) {
        failOn = ride;
    }

    @Contract(pure = true)
    public RouteRide getFailOn() {
        return failOn;
    }

    @Contract("_ -> this")
    public GrouperErrors setFailObj(RouteRide failObj) {
        failOn = failObj;
        return this;
    }
}
