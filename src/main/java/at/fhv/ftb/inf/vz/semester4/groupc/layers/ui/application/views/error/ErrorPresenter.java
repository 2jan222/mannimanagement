package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.error;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import javax.inject.Inject;


/**
 * Handles behavior and interaction of the error screen.
 */
@LoggerableClassDisplayName("[ErrorPresenter]")
public class ErrorPresenter implements MenuViewItem {
    @FXML
    private Label reason;
    @Inject
    Loggerable logger;

    public void setReason(String reason) {
        this.reason.setText(reason);
        logger.info("Reason set to '" + reason + "'");
    }

    @Override
    public String getMenuDisplayName() {
        return "Error Screen";
    }
}
