package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import javafx.beans.property.SimpleBooleanProperty;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;

class DataContextBuilder {
    private LocalDate date;
    private HashMap<Integer, OperationFlat> operationById = new HashMap<>();
    private HashMap<Integer, LinkedList<ShiftEntry>> shiftsByOperationId = new HashMap<>();
    private HashMap<Integer, LinkedList<ShiftEntry>> shiftsByDriverId = new HashMap<>();
    private HashMap<Integer, ShiftEntry> shiftEntryById = new HashMap<>();
    private HashMap<Integer, Driver> driverByDriverId = new HashMap<>();
    private SimpleBooleanProperty isSaved;

    @Contract(pure = true)
    DataContextBuilder(LocalDate date) {
        this.date = date;
        isSaved = new SimpleBooleanProperty(false);
    }

    DataContextBuilder addOperation(OperationFlat operation) throws ValidationException {
        RawDataValidator.validateOperation(operation);
        operationById.putIfAbsent(operation.getOperationId(), operation);
        shiftsByOperationId.putIfAbsent(operation.getOperationId(), new LinkedList<>());
        return this;
    }

    DataContextBuilder addDriverOnDuty(Driver driver) throws ValidationException {
        RawDataValidator.validateDriver(driver, date);
        shiftsByDriverId.putIfAbsent(driver.getDriverId(), new LinkedList<>());
        driverByDriverId.putIfAbsent(driver.getDriverId(), driver);
        return this;
    }


    DataContext buildContext() {
        return new DataContext(date, operationById, shiftsByOperationId, shiftsByDriverId, shiftEntryById, driverByDriverId, isSaved);
    }

    DataContextBuilder addShift(@NotNull ShiftEntry shiftEntry) throws ValidationException {
        RawDataValidator.validateShift(shiftEntry);
        shiftEntryById.put(shiftEntry.getShiftEntryId(), shiftEntry);
        shiftsByOperationId.computeIfPresent(shiftEntry.getOperationId(), (key, value) -> {
            value.add(shiftEntry);
            return value;
        });
        shiftsByDriverId.computeIfPresent(shiftEntry.getDriverId(), (key, value) -> {
           value.add(shiftEntry);
           return value;
        });
        return this;
    }
}
