package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.sidebar;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuIntegratedPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.FutureRunnable;
import com.airhacks.afterburner.views.FXMLView;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * SidebarPresenter.
 * Presenter for MenuSidebar.
 */
@LoggerableClassDisplayName("[Sidebar]")
public class SidebarPresenter {
    @Inject
    private Loggerable logger;
    @FXML
    private VBox sidebarContainer;
    private HashMap<String, FXMLView> viewByName = new HashMap<>();
    private Button currentTab;
    private MenuIntegratedPresenter mainController;


    private void changeTab(@NotNull FXMLView view) {
        logger.info("Change tab to '" + ((MenuViewItem) view.getPresenter()).getMenuDisplayName() + "'");
        mainController.changeContentView(view);
        Platform.runLater(new FutureRunnable(() -> {
            long start = System.currentTimeMillis();
            ((MenuViewItem) view.getPresenter()).onMenuSwitch();
            logger.debug("Menu Switched Done (" + (System.currentTimeMillis() - start) + " ms)");
        }));

    }

    private void switchStyle(Button current, boolean setWindowTitle) {
        if (currentTab != null) {
            currentTab.setStyle("-fx-background-color: #3F3F3F;");
        }
        current.setStyle("-fx-background-color: #0096c9;");
        mainController.updateText(current.getText().replace("\n", ""), setWindowTitle);
        currentTab = current;
    }

    public void setMainController(MenuIntegratedPresenter controller) {
        mainController = controller;
        sidebarContainer.setOnMouseExited(event -> mainController.hamburgerAction());
    }

    private void changeToTab(@NotNull ActionEvent actionEvent, boolean setWindowTitle) {
        if (currentTab != null) {
            ((MenuViewItem) viewByName.get(currentTab.getText()).getPresenter()).onTabExit();
        }
        switchStyle((Button) actionEvent.getSource(), setWindowTitle);
        changeTab(viewByName.get(((Button) actionEvent.getSource()).getText()));
    }

    public void init(@NotNull LinkedList<FXMLView> views) {
        String menuDisplayName;
        Button first = null;
        Button btn;
        for (FXMLView view : views) {
            if (view.getPresenter() == null) {
                logger.error("Presenter is null of view: " + view.toString());
            } else {
                try {
                    MenuViewItem presenter = (MenuViewItem) view.getPresenter();
                    menuDisplayName = presenter.getMenuDisplayName();
                    viewByName.put(menuDisplayName, view);
                    btn = createSidebarButton(menuDisplayName);
                    sidebarContainer.getChildren().add(btn);
                    if (first == null) {
                        first = btn;
                    }
                } catch (ClassCastException castException) {
                    logger.error(castException.getMessage());
                }
            }
        }
        if (first != null) {
            changeToTab(new ActionEvent(first, null), false);
        }
    }

    private Button createSidebarButton(String text) {
        Button btn = new Button(text);
        btn.setId(text);
        btn.getStyleClass().add("sidebarMenu");
        btn.getStylesheets().add(this.getClass().getResource("./sidebarButton.css").toExternalForm());
        btn.setOnAction(this::changeToTab);
        VBox.setMargin(btn, new Insets(15, 0, 0, 0));
        return btn;
    }

    @FXML
    private void changeToTab(ActionEvent actionEvent) {
        changeToTab(actionEvent, true);
    }

}
