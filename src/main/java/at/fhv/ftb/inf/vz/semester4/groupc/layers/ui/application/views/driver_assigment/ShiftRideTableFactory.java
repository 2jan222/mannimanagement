package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.driver_assigment;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftRideDTO;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

@SuppressWarnings("WeakerAccess")
public class ShiftRideTableFactory {

    public static TableView<ShiftRideDTO> createTableView() {
        TableView<ShiftRideDTO> tableView = new TableView<>();

        TableColumn<ShiftRideDTO, Integer> shiftRideIdCol = new TableColumn<>("#");
        TableColumn<ShiftRideDTO, String> shiftRideStartStationName = new TableColumn<>("First Stop");
        TableColumn<ShiftRideDTO, String> shiftRideEndStationName = new TableColumn<>("Last Stop");
        TableColumn<ShiftRideDTO, String> shiftRideStartTime = new TableColumn<>("Start Time");
        TableColumn<ShiftRideDTO, String> shiftRideEndTime = new TableColumn<>("End Time");

        tableView.getColumns().add(shiftRideIdCol);
        tableView.getColumns().add(shiftRideStartStationName);
        tableView.getColumns().add(shiftRideEndStationName);
        tableView.getColumns().add(shiftRideStartTime);
        tableView.getColumns().add(shiftRideEndTime);


        shiftRideIdCol.setCellValueFactory(cellData ->
                new SimpleObjectProperty<>(cellData.getValue().getShiftRideId()));
        shiftRideStartStationName.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getStartStationName()));
        shiftRideEndStationName.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getEndStationName()));
        shiftRideStartTime.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getStartTime().toString()));
        shiftRideEndTime.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getEndTime().toString()));


        final Label label = new Label("No Content");
        tableView.setPlaceholder(label);

        return tableView;
    }
}
