package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.SuspendedEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = BusEntityMapper.class)
public interface SuspendedEntityMapper {
    SuspendedEntityMapper INSTANCE = Mappers.getMapper(SuspendedEntityMapper.class);

    @Mapping(target = "bus", source = "busByBusId")
    Suspended toMojo(SuspendedEntity suspendedEntity);


    @Mapping(target = "busByBusId", source = "bus")
    SuspendedEntity toPojo(Suspended suspended);

    @Mapping(target = "bus", source = "busByBusId")
    void updateMojo(SuspendedEntity suspendedEntity, @MappingTarget Suspended suspended);

    @Mapping(target = "busByBusId", source = "bus")
    void updatePojo(Suspended suspended, @MappingTarget SuspendedEntity suspendedEntity);


    List<Suspended> suspendedEntitiesToSuspendeds(List<SuspendedEntity> suspendedEntities);


    List<SuspendedEntity> suspendedsToSuspendedEntities(List<Suspended> suspendeds);

}
