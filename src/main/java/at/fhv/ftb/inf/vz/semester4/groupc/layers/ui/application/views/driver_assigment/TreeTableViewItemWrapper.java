package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.driver_assigment;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftEntryDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import org.jetbrains.annotations.Contract;

import java.util.LinkedList;
@SuppressWarnings("WeakerAccess")
public class TreeTableViewItemWrapper<ParentRowType, InfoRowType, DataRowType> {
    private final ShiftEntryDTO shiftEntry;
    private InfoRowType information;
    private ParentRowType groupName;
    private ObservableList<DataRowType> data;
    private TableView<DataRowType> tableView;

    @Contract(pure = true)
    public TreeTableViewItemWrapper(ParentRowType groupName, InfoRowType information, LinkedList<DataRowType> data, TableView<DataRowType> tableView, ShiftEntryDTO entry) {
        this.groupName = groupName;
        this.tableView = tableView;
        this.information = information;
        this.shiftEntry = entry;
        setData(data);
    }

    public void setData(LinkedList<DataRowType> data) {
        if (data == null) {
            data = new LinkedList<>();
        }
        this.data = FXCollections.observableArrayList(data);
        if (tableView != null) {
            tableView.setItems(this.data);
        }
    }

    public TableView<DataRowType> getTableView() {
        return tableView;
    }

    public ParentRowType getGroupName() {
        return groupName;
    }

    public ObservableList<DataRowType> getData() {
        return data;
    }

    public InfoRowType getInformation() {
        return information;
    }

    public ShiftEntryDTO getShiftEntry() {
        return shiftEntry;
    }
}
