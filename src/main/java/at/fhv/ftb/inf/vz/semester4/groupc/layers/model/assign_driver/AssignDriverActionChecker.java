package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.LinkedList;

import static java.time.temporal.ChronoUnit.SECONDS;

@LoggerableClassDisplayName("[A-D-ActionsChecker]")
public class AssignDriverActionChecker {
    private static Loggerable logger = Loggerable.getInstance();

    private static final long EIGHT_HOURS_IN_SECONDS = 28800;
    private static final long NINETY_MINUTES_IN_SECONDS = 5400;
    private static final long FIFTEEN_MINUTES_IN_SECONDS = 900;

    static boolean checkShiftsOverlappingTime(@NotNull ShiftEntry target, @NotNull DataContext context) {
        logger.debug("Check target shift overlapping with shifts in targets desired group");
        LinkedList<ShiftEntry> shiftEntriesForOperation = context.getShiftEntriesForOperation(target.getOperationId());
        return timeOverlapping(target, shiftEntriesForOperation);
    }

    static boolean checkShiftComplianceToLaw(@NotNull ShiftEntry shiftEntry,
                                                  int driverEmploymentType,
                                                  @NotNull DataContext context) {
        return true || checkShiftComplianceToLaw(shiftEntry, driverEmploymentType, 0, context);     //TODO REMOVE RETURN TRUE
    }

    static boolean checkShiftComplianceToLaw(@NotNull ShiftEntry shiftEntry,
                                             int driverEmploymentType, long drivenTimeForDay,
                                             @NotNull DataContext context) {
        boolean compliance = true;
        long timeWithoutPause = 0;
        long totalTimeForDay = Math.abs(drivenTimeForDay);
        long pauseTime = 0;
        Station lastStationOfRide = null;
        LocalTime endTimeOfRide = null;
        logger.debug(shiftEntry.getShiftRides().size(), LoggerableColor.ANSI_RED);
        for (ShiftRide sr : shiftEntry.getShiftRides()) {

            if (compliance) {

                timeWithoutPause += sr.getDuration();
                totalTimeForDay += sr.getDuration();
                if (lastStationOfRide != null && endTimeOfRide != null) {

                    totalTimeForDay += SECONDS.between(endTimeOfRide, sr.getStartTime());
                    Station start = sr.getStart();
                    if (start.equals(lastStationOfRide)) {

                        pauseTime += SECONDS.between(endTimeOfRide, sr.getStartTime());
                        if (checkPauseGreaterThanFifteen(pauseTime)) {

                            timeWithoutPause = 0;
                            pauseTime = 0;
                        }
                    } else {

                        long timeFromPrevious = 0;
                        Integer stationId = lastStationOfRide.getStationId();

                        Integer startStationId = start.getStationId();

                        int toIntExact = Math.toIntExact(SECONDS.between(endTimeOfRide, sr.getStartTime()));

                        HashMap<StationIDPair, Integer> mapTimeFromPrevious = context.getMapTimeFromPrevious();

                        mapTimeFromPrevious.putIfAbsent(
                                new StationIDPair(stationId, startStationId),
                                toIntExact
                        );


                        timeFromPrevious += context.getTimeFromPrevious(
                                stationId,
                                startStationId
                        );

                        LocalTime driveToNewStation = endTimeOfRide.plus(
                                context.getTimeFromPrevious(stationId,
                                        startStationId)
                                , SECONDS);

                        pauseTime += SECONDS.between(driveToNewStation, sr.getStartTime());
                        if (checkPauseGreaterThanFifteen(pauseTime)) {

                            timeWithoutPause = 0;
                            pauseTime = 0;
                        } else {
                            timeWithoutPause += timeFromPrevious;
                        }

                    }
                }

            }
            if (timeWithoutPause > NINETY_MINUTES_IN_SECONDS ||
                    (driverEmploymentType == 0 && totalTimeForDay > EIGHT_HOURS_IN_SECONDS) ||
                    (driverEmploymentType == 1 && totalTimeForDay > EIGHT_HOURS_IN_SECONDS / 2)) {
                compliance = false; //assumption: 0 = full time, 1 = part time
            }
            lastStationOfRide = sr.getEnd();
            endTimeOfRide = sr.getEndTime();


        }
        return compliance;
    }

    @Contract(pure = true)
    private static boolean checkPauseGreaterThanFifteen(long pause) {
        return pause >= FIFTEEN_MINUTES_IN_SECONDS;
    }

    static boolean checkDriverShiftsNewShiftOverlapping(ShiftEntry target, Driver driver,
                                                        @NotNull DataContext context) {
        LinkedList<ShiftEntry> shiftEntriesForOperation = context.getShiftEntriesForDriver(driver);
        boolean b = timeOverlapping(target, shiftEntriesForOperation);
        logger.debug("Check target shift overlapping with shifts of targets desired driver: " + b);
        return b;
    }


    private static boolean timeOverlapping(@NotNull ShiftEntry target,
                                           @NotNull LinkedList<ShiftEntry> existingEntries) {
        for (ShiftEntry e : existingEntries) {
            if (target.getEndTime().isAfter(e.getStartTime())) {
                if (target.getStartTime().isBefore(e.getEndTime())) {
                    logger.debug("Time Overlapping");
                    return true;
                }
            }
        }
        return false;
    }

    static boolean checkDriverAllowanceToPerformShiftEntry(ShiftEntry target,
                                                           Driver driver,
                                                           @NotNull DataContext context) {
        LinkedList<ShiftEntry> entries = context.getShiftEntriesForDriver(driver);
        long l = entries.stream().mapToLong(ShiftEntry::getDuration).sum();
        logger.debug("Long:" + l, LoggerableColor.ANSI_RED);
        boolean b = checkShiftComplianceToLaw(target, driver.getEmploymentType(), l, context);
        logger.debug("Check if Driver can and may perform the shift: " + b);
        return true;
    }


    static boolean isOnVacation(@NotNull Driver driver, LocalDate date) {
        return driver.isDriverAbsentOnDate(date);
    }

    private static long calcWorkSecsInEntry(@NotNull ShiftEntry entry) {
       /* long workTime = 0;
        Station lastStationOfRide = null;
        LocalTime endTimeOfRide = null;
        for (ShiftRide sr : entry.getShiftRides()) {
            workTime += sr.getDuration();
            if (lastStationOfRide != null && endTimeOfRide != null) {
                workTime += SECONDS.between(endTimeOfRide, sr.getStartTime());
                if (sr.getStart().equals(lastStationOfRide)) {
                    if (SECONDS.between(endTimeOfRide, sr.getStartTime()) < FIFTEEN_MINUTES_IN_SECONDS) {
                        workTime += SECONDS.between(endTimeOfRide, sr.getStartTime());
                    }
                } else {
                    long timeFromPrevious = 0;
                    timeFromPrevious += context.getTimeFromPrevious(lastStationOfRide.getStationId(),
                     sr.getStart().getStationId());
                    workTime += timeFromPrevious;

                    //TO DO what to do when no station Connector exists (exception? default value?)
                        <- no longer applies right now
                }


            }
            lastStationOfRide = sr.getEnd();
            endTimeOfRide = sr.getEndTime();
        }*/
        return entry.getDuration();
    }


    @Contract("null, _ -> false")
    public static boolean compareContextDate(LocalDate date, @NotNull DataContext context) {
        return context.getDate().equals(date);
    }

    public static boolean canDriverTakeShiftForDay(Driver driver, @NotNull DataContext context,
                                            ShiftEntry shiftEntry, int employmentType) {
        long driverTime = 0;
        for (ShiftEntry e : context.getShiftEntriesForDriver(driver)) {
            driverTime += calcWorkSecsInEntry(e);
        }
        driverTime += calcWorkSecsInEntry(shiftEntry);
        if (employmentType == 0) {
            return (driverTime <= EIGHT_HOURS_IN_SECONDS);
        } else {
            return (driverTime <= EIGHT_HOURS_IN_SECONDS / 2);
        }
    }


    @TestOnly
    static boolean timeOverlappingTestAccess(@NotNull ShiftEntry target,
                                             @NotNull LinkedList<ShiftEntry> existingEntries) {
        return timeOverlapping(target, existingEntries);
    }


}
