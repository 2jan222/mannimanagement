package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos;

import java.util.LinkedList;

/**
 * PathDTO.
 */
public class PathDTO {

    private Integer pathId;
    private Integer routeId;
    private LinkedList<PathStationDTO> pathStations;

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public LinkedList<PathStationDTO> getPathStations() {
        return pathStations;
    }

    public void setPathStations(LinkedList<PathStationDTO> pathStations) {
        this.pathStations = pathStations;
    }

    @Override
    public String toString() {
        return "PathDTO{" +
                "pathId=" + pathId +
                ", routeId=" + routeId +
                ", pathStations=" + pathStations.size() +
                '}';
    }
}
