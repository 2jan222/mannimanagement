package at.fhv.ftb.inf.vz.semester4.groupc.layers.service.grouping;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.grouping.GroupActions;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.grouping.GrouperException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.FlatBusEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Operation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.RouteRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.RouteRideEntityFastMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.QueryService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.AbsencePersistenceEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.BusDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.OperationDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.RouteRideDTOMapper;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * OperationService.
 * Service for combined view BusAssignment and RouteAssignment
 */
@SuppressWarnings("unchecked")
@LoggerableClassDisplayName("[OperationService]")
public class OperationService {
    private Loggerable logger = Loggerable.getInstance();
    private static final PersistenceFacade pf = PersistenceFacade.getInstance();

    public OperationDTO removeBus(@NotNull OperationDTO operationDTO) {
        logger.debug("Remove BusDTO{id =" + operationDTO.getBusId() + "} from OperationDTO{" +
                "id = " + operationDTO.getOperationId() + "}");
        Operation operation = OperationDTOMapper.INSTANCE.toMojo(operationDTO);
        //operation = new Operation();
        //operation.setOperationId(operationDTO.getOperationId());
        GroupActions.removeBusFromGroup(operation);
        QueryService.removeBusFromOperation(operation.getOperationId());
        operationDTO = OperationDTOMapper.INSTANCE.toDTO(operation);
        return operationDTO;
    }

    /**
     * Translates DTOs to Mojo, Redirects to GroupActions.assignBusToGroup(...) and updates database afterwards.
     *
     * @param operationDTO operation/group to add bus to
     * @param busDTO       bus to be added to group
     * @return updated operationDTO
     * @throws RuntimeException if bus does not exist on database.
     */
    public OperationDTO addBus(@NotNull OperationDTO operationDTO, @NotNull BusDTO busDTO) {
        logger.debug("Add BusDTO{id = " + busDTO.getBusId() + "} to OperationDTO{id =" +
                operationDTO.getOperationId() + "}");
        Bus bus = BusDTOMapper.INSTANCE.toMojo(busDTO);
        Operation operation = OperationDTOMapper.INSTANCE.toMojo(operationDTO);
        GroupActions.assignBusToGroup(operation, bus);
        QueryService.assignBusToGroup(busDTO.getBusId(), operationDTO.getOperationId());
        logger.debug("Added bus");
        return OperationDTOMapper.INSTANCE.toDTO(operation);
    }

    public LinkedList<OperationDTO> getGroupsForDate(@NotNull LocalDate dateIn) {
        //DatabaseConnector.getSession().createQuery("FROM OperationEntity WHERE date = '" +
        // dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "'");
        List<OperationEntity> daysOperations = pf.getAllWhere(OperationEntity.class,
                "WHERE date = '" + dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "'");
        logger.debug("SIZE:" + daysOperations.size(),  LoggerableColor.ANSI_RED);
        daysOperations.forEach(e -> e.getRouteRidesByOperationId().forEach(p -> {
            System.out.println(p    );
            System.out.println(p.getStartTimeByStartTimeId().getPathByPathId().getPathStationsByPathId());
        }));
        return new LinkedList<>(OperationDTOMapper.INSTANCE.toDTOs(
                OperationEntityMapper.INSTANCE.toMojos(daysOperations)));
        /*
        EntityManager session = DatabaseConnector.getSession();
        ArrayList<Operation> ops = (ArrayList<Operation>) OperationEntityMapper.INSTANCE.toPojos(
                session
                        .createQuery("SELECT o FROM OperationEntity o WHERE date = '" + dateIn + "'",
                        OperationEntity.class).getResultList());
        LinkedList<OperationDTO> operations = new LinkedList<>();
        ops.forEach(op -> {
            ArrayList<RouteRide> routeRides = new ArrayList<>(RouteRideEntityMapper.INSTANCE.toMojos(session
                    .createQuery("FROM RouteRideEntity WHERE operationId = '" + op.getOperationId() + "'")
                    .getResultList()));
            LinkedList<RouteRideDTO> routeRideDTOs = new LinkedList<>();
            LinkedList<RouteRide> list = new LinkedList<>();
            routeRides.forEach(e -> {
                if (e.getDate() != null && e.getDate().isEqual(dateIn)) {
                    Path p = (Path) PathEntityMapper.INSTANCE.toMojos(session.createQuery
                            ("FROM PathEntity WHERE pathId = '" + e.getPathId() + "'").getResultList()).get(0);
                    LinkedList<PathStation> stations =
                            new LinkedList<PathStation>(PathStationEntityMapper.INSTANCE.toMojos(
                                    DatabaseConnector.getSession().createQuery("FROM PathStationEntity " +
                                            "WHERE id.pathId = '" + p.getPathId() + "'").getResultList()));
                    p.setPathStations(stations);
                    e.setPathId(p.getPathId());
                    routeRideDTOs.add(RouteRideDTOMapper.INSTANCE.toDTO(e));
                    list.add(e);
                }
            });

            op.setRouteRides(list);
        });

        session.close();
        //ops.forEach(System.out::println);
        ops.forEach(e -> operations.add(OperationDTOMapper.INSTANCE.toDTO(e)));
        return operations;
        /*
        //!!!!DUMMY DATA
        operations.add(new OperationDTO(19, LocalDate.now(),
                new RouteRideDTO(getUngroupedBusesForDate(LocalDate.MIN).getFirst(),
                        new StartTimeDTO(LocalTime.now().plus(5, ChronoUnit.MINUTES), 50),
                        LocalTime.now().plus(50, ChronoUnit.MINUTES),
                        new RouteDTO(22, "test")),
                new RouteRideDTO(getUngroupedBusesForDate(LocalDate.MIN).getFirst(),
                new StartTimeDTO(LocalTime.now(), 40), LocalTime.now().plus(50, ChronoUnit.MINUTES),
                new RouteDTO(22, "test"))));
        */
    }

    public LinkedList<RouteRideDTO> getUngroupedRidesForDate(@NotNull LocalDate dateIn) {
        /*List<RouteRideEntityFast> allWhere = (List<RouteRideEntityFast>) pf.executeTransaction(session -> {
            Query query = session.createQuery("FROM RouteRideEntityFast r WHERE r.operationId is null AND r.routeRideDate = '"
                    + dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "'");
            return query.getResultStream().collect(Collectors.toCollection(LinkedList::new));
        });
         */
        List<RouteRideEntityFast> allWhere = new LinkedList<>(); //TODO REMOVE
        return new LinkedList<>(RouteRideDTOMapper.INSTANCE.toDTos(RouteRideEntityFastMapper.INSTANCE.toMojos(allWhere)));
        //TODO QUERY for rides at given date and which are !not part! of a group
        /*
        List<RouteRideEntityFast> ungrouped = pf.getAllWhere(RouteRideEntityFast.class,
                "WHERE operationId IS null AND routeRideDate = '" + dateIn.format(DateTimeFormatter.ofPattern
                        ("yyyy-MM-dd")) + "'");
        /*
        ArrayList<RouteRide> routeRides = (ArrayList<RouteRide>) RouteRideEntityMapper.INSTANCE
        .routeRideEntitesToRouteRides(DatabaseConnector.getSession()
                .createQuery("FROM RouteRideEntity rr WHERE routeRideDate ='"  + dateIn
                + "' AND operationByOperationId IS NULL", RouteRideEntity.class)
                .getResultList());
                */
        /*
        Session session = DatabaseConnector.getSession();
        List fromRouteRideEntity = session
                .createQuery("FROM RouteRideEntity")
                .getResultList();
        //fromRouteRideEntity.forEach(e -> logger.fatal(e.toString()));
        ArrayList<RouteRide> routeRides = (ArrayList<RouteRide>)
                RouteRideEntityMapper.INSTANCE.toMojos(fromRouteRideEntity);
        LinkedList<RouteRideDTO> routeRideDTOs = new LinkedList<>();
        routeRides.forEach(e -> {
            if (e.getDate() != null && e.getDate().isEqual(dateIn) && e.getOperationId() != null) {
                //logger.fatal(e.toString());
                //Path p = (Path) PathEntityMapper.INSTANCE.toMojos(session.createQuery("FROM PathEntity
                WHERE pathId = " + e.getPathId()).getResultList()).get(0);
                Path p = (Path) pf.getAllWhere(PathEntity.class, "WHERE pathId = " +
                e.getPathId()).get(0);
                //LinkedList<PathStation> stations = PathStationEntityMapper.INSTANCE.pathStationEntitiesToPathStations
                // (DatabaseConnector.getSession().createQuery("FROM PathStationEntity WHERE id.pathId = '" +
                // p.getPathId() + "'").getResultList());
                e.setPathId(p.getPathId());
                routeRideDTOs.add(RouteRideDTOMapper.INSTANCE.toDTO(e));
            }
            //logger.debug(e.toString());
        });
        session.close();
        return routeRideDTOs;
        */

    }

    public LinkedList<BusDTO> getUngroupedBusesForDate(@NotNull LocalDate dateIn) {
        //TODO QUERY for Buses, which !are available! at given date and are !not part! of a group on given day
        /*Session sess = DatabaseConnector.getSession();
        TypedQuery<BusEntity> busEntityTypedQuery = sess.createQuery(
        "SELECT b FROM BusEntity b INNER JOIN b.suspendedsByBusId s INNER JOIN b.routeRidesByBusId rr
        WHERE :dateIn NOT BETWEEN s.dateFrom AND s.dateTo AND rr.operationByOperationId.date != :dateIn",
         BusEntity.class)
                .setParameter("dateIn", dateIn);
        List<BusEntity> busEntities = busEntityTypedQuery.getResultList();
        ODER VIELLEICHT:
        TypedQuery<BusEntity> busQuery = sess.createQuery("SELECT b FROM BusEntity b WHERE b.suspendedsByBusId
        NOT IN (SELECT s from SuspendedEntity s WHERE :dateIn BETWEEN dateFrom AND dateTo) AND b.routeRidesByBusId
        NOT IN (SELECT rr FROM RouteRideEntity rr WHERE operationByOperationId.date = :dateIn)", BusEntity.class)
                .setParameter("dateIn", dateIn);



        List allWhere = pf.getAllWhere(FlatBusEntity.class,
                "WHERE NOT EXISTS (Select o.busId FROM OperationEntity o WHERE o.date = '"
                        + dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                        + "' AND o.busId = busId)");

         */
        List<FlatBusEntity> allWhere = (List<FlatBusEntity>) pf.executeTransaction(session -> {
            Query query = session.createQuery("FROM FlatBusEntity b WHERE NOT EXISTS (Select o.busId FROM OperationEntity o WHERE o.date = '"
                    + dateIn.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                    + "' AND o.busId = b.busId)");
            return query.getResultStream().collect(Collectors.toCollection(LinkedList::new));
        });
        LinkedList<Bus> buses = new LinkedList<>(FlatBusEntityMapper.INSTANCE.toMojos(allWhere));
        return new LinkedList<>(BusDTOMapper.INSTANCE.toDTOs(buses));
    }

    public OperationDTO addRouteRide(@NotNull OperationDTO operationDTO, @NotNull RouteRideDTO routeRideDTO)
            throws GrouperException {
        /*
        Operation operation = operationMapper.toMojo(operationDTO, new CycleAvoidanceMapping());
        //RouteRide routeRideDB = RouteRideEntityMapper.INSTANCE.routeRideEntityToRouteRide(
                //(RouteRideEntity) DatabaseConnector.getSession()
                        //.createQuery("FROM RouteRideEntity r WHERE r.id ='" + routeRideDTO.getId() + "'")
                        //.getResultList().get(0));

        System.out.println("before");
        RouteRide routeRideDB = routeRideMapper.toMojo(routeRideDTO, new CycleAvoidanceMapping());
        System.out.println("routeRideDB = " + routeRideDB.getRouteRideId());
        Session session = DatabaseConnector.getSession();
        List<RouteRide> allRouteRidesOfDB =
                RouteRideEntityMapper.INSTANCE.routeRideEntitiesToRouteRides(pf
                .getAll(RouteRideEntity.class), new CycleAvoidanceMapping());
        RouteRide rideWithId = (new LinkedList<>(allRouteRidesOfDB.stream().filter(e ->
                e.getRouteRideId() == routeRideDB.getRouteRideId()).collect(Collectors.toList()))).getFirst();
        Path p = (Path) PathEntityMapper.INSTANCE.pathEntitesToPaths(session.createQuery
                ("FROM PathEntity WHERE pathId = " + rideWithId.getStartTime().getPathId()).getResultList(),
                new CycleAvoidanceMapping()).get(0);
        rideWithId.getStartTime().setPath(p);
        session.close();
        System.out.println("before action");
        GroupActions.addRideToGroup(operation, routeRideDB);
        System.out.println("after action");
        //update(routeRideDB, operation);
        */
        logger.debug("Add RouteRideDTO{id = " + routeRideDTO.getId() + "} to OperationDTO{id = " +
                operationDTO.getOperationId() + "}");
        Operation operation = OperationDTOMapper.INSTANCE.toMojo(operationDTO);
        RouteRide routeRide = RouteRideDTOMapper.INSTANCE.toMojo(routeRideDTO);
        GroupActions.addRideToGroup(operation, routeRide);
        operationDTO = OperationDTOMapper.INSTANCE.toDTO(operation);
        logger.debug("Added RouteRideDTO");
        return operationDTO;
    }

    @Deprecated
    public LinkedList<BusDTO> getGroupedBusesForDate(LocalDate dateIn) {
        //TODO QUERY for Buses, which !are available! at given date and are !part! of a group on given day
       /* Session sess = DatabaseConnector.getSession();
        TypedQuery<BusEntity> busEntityTypedQuery = sess.createQuery("SELECT b
        FROM BusEntity b INNER JOIN b.suspendedsByBusId s INNER JOIN b.routeRidesByBusId rr
        WHERE :dateIn NOT BETWEEN s.dateFrom AND s.dateTo
        AND rr.operationByOperationId.date = :dateIn", BusEntity.class)
                .setParameter("dateIn", dateIn);
        ODER VIELLEICHT:
        TypedQuery<BusEntity> busQuery = sess.createQuery("SELECT b FROM BusEntity b WHERE b.suspendedsByBusId NOT IN
        (SELECT s from SuspendedEntity s WHERE :dateIn BETWEEN dateFrom AND dateTo) AND b.routeRidesByBusId IN
        (SELECT rr FROM RouteRideEntity rr WHERE operationByOperationId.date = :dateIn)", BusEntity.class)
                .setParameter("dateIn", dateIn);

        List<BusEntity> busEntities = busEntityTypedQuery.getResultList();*/
        return new LinkedList<>();
    }

    public void removeRideFromGroup(@NotNull RouteRideDTO route) {
        QueryService.removeRideFromOperation(route.getId());
    }

    public LinkedList<LocalDate> getDatesWithOperations() {
        LinkedList<LocalDate> dates;
        dates = (LinkedList<LocalDate>) pf.executeTransaction(session -> {
            Query query = session.createQuery("Select o.date FROM OperationEntity o WHERE o.date is not null");
            Stream<Object> resultStream = query.getResultStream();
            return resultStream.collect(Collectors.toCollection(LinkedList::new));
        });
        return (dates != null) ? dates : new LinkedList<>();
    }
}
