package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationRouteRideEntityFast;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class OperationRouteRideEntityFastPersistenceEntityMapper<T> extends PersistenceEntityMapper<T> {
    @Override
    public T read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (T) sess.get(OperationRouteRideEntityFast.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("T", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<T> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from OperationRouteRideEntityFast ");
            return (List<T>) query.getResultList();
        }
    }

    @Override
    public List<T> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from OperationRouteRideEntityFast " + whereQueryFragment);
            return (List<T>) query.getResultList();
        }
    }
}
