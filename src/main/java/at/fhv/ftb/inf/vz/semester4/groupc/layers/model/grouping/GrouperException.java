package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.grouping;

/**
 * GrouperException thrown in Grouper.
 */

public class GrouperException extends RuntimeException {
    private GrouperErrors error;

    public GrouperException(GrouperErrors error) {
        this.error = error;
    }

    public GrouperErrors getError() {
        return error;
    }
}
