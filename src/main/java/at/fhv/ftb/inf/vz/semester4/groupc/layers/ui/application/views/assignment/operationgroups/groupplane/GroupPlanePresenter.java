package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.assignment.operationgroups.groupplane;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.grouping.GrouperException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.grouping.OperationService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.assignment.operationgroups.GroupPlaneButtonPair;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.PathStationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.DragAndDropManager;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.NotificationCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import javax.inject.Inject;

/**
 * GroupPlanePresenter.
 * Presenter to view the group and add rides and a bus
 */
@LoggerableClassDisplayName("[GroupPlanePresenter]")
public class GroupPlanePresenter implements Initializable {

    @Inject
    private static Loggerable logger;


    @FXML
    private Button removeBusButton;
    @FXML
    private TableView<RouteRideDTO> ridesOfGroupTableView;
    @FXML
    private TableColumn<RouteRideDTO, Integer> rideOfGroupRideNumberCol;
    @FXML
    private TableColumn<RouteRideDTO, String> rideOfGroupStartTimeCol;
    @FXML
    private TableColumn<RouteRideDTO, String> rideOfGroupEndTimeCol;
    @FXML
    private TableColumn<RouteRideDTO, String> rideOfGroupStartStationCol;
    @FXML
    private TableColumn<RouteRideDTO, String> rideOfGroupEndStationCol;
    @FXML
    private TableColumn<RouteRideDTO, String> rideOfGroupActionsCol;
    @FXML
    private Label groupName;
    @FXML
    private AnchorPane selfRoot;
    @FXML
    private Label groupPlaneBusLabel;

    private GroupPlaneButtonPair pairOf;

    private void setGroupDisplayName(String displayName) {
        if (displayName == null) {
            displayName = "#";
        }
        groupName.setText(displayName);
        logger.debug("Set Group DisplayName: '" + displayName + "'");
    }

    public void hideToggle() {
        pairOf.hideToggle(false);
    }


    public void setPlaneButtonPair(GroupPlaneButtonPair groupPlaneButtonPair) {
        pairOf = groupPlaneButtonPair;
    }

    @FXML
    private void removeBus() {
        pairOf.removeBus();
    }

    private void setGroupPlaneBusLabelText(String text, boolean isEmpty) {
        groupPlaneBusLabel.setText(text);
        if (!isEmpty) {
            groupPlaneBusLabel.setStyle("-fx-text-fill: #000;");
        } else {
            groupPlaneBusLabel.setStyle("-fx-text-fill: #e62d2d;");
        }
    }


    private void setRidesOfGroupTableData(@NotNull List<RouteRideDTO> rides) {
        logger.debug("Set data of ridesOfGroupTableView[" + rides.size() + "]");
        ridesOfGroupTableView.getItems().setAll(FXCollections.observableArrayList(rides));
    }

    public void update() {
        long start = System.currentTimeMillis();
        logger.debug("UPDATE PLANE START");
        LinkedList<RouteRideDTO> routeRideDTOS = pairOf.getOperation().getRouteRideDTOS();
        setRidesOfGroupTableData(routeRideDTOS);
        setGroupDisplayName(pairOf.getGroupName());
        BusDTO bus = pairOf.getOperation().getBus();
        updateBusLabels((bus != null) ? bus.getLicenceNumber() : null);
        ridesOfGroupTableView.sort();
        logger.debug("UPDATE PLANE END in " + (System.currentTimeMillis() - start) + " ms");
    }

    private void updateBusLabels(String lnumber) {
        if (lnumber != null) {
            setGroupPlaneBusLabelText(lnumber, false);
            //TODO: FIX NPE groupPlaneBusLabel.setTooltip(new Tooltip(pairOf.getOperation().getBus().toString()));
            removeBusButton.setVisible(true);
        } else {
            setGroupPlaneBusLabelText("NONE", true);
            //TODO: FIX NPE groupPlaneBusLabel.setTooltip(new Tooltip("Drag and drop a bus from table view here"));
            removeBusButton.setVisible(false);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        long start = System.currentTimeMillis();
        logger.debug("initialize");
        initTable();
        initPaneDragAndDrop();
        logger.debug("initialized " + (System.currentTimeMillis() - start));
    }

    @SuppressWarnings("Duplicates")
    private void initTable() {
        long start = System.currentTimeMillis();
        logger.debug("Init table");
        rideOfGroupStartTimeCol.setCellValueFactory(data -> {
            LocalTime startTime = data.getValue().getStartTime();
            return new SimpleStringProperty((startTime != null) ?
                    startTime.format(DateTimeFormatter.ofPattern("hh:mm a")) : "-");
        });
        rideOfGroupEndTimeCol.setCellValueFactory(data ->
                new SimpleStringProperty(
                        data.getValue().getEndTime().format(DateTimeFormatter.ofPattern("hh:mm a"))));
        rideOfGroupRideNumberCol.setCellValueFactory(p ->
                new ReadOnlyObjectWrapper<>(ridesOfGroupTableView.getItems().indexOf(p.getValue())));
        rideOfGroupRideNumberCol.setSortable(false);
        rideOfGroupStartStationCol.setCellValueFactory(data ->
                new SimpleStringProperty(getStationPathStationName(data.getValue(), true)));
        rideOfGroupEndStationCol.setCellValueFactory(data ->
                new SimpleStringProperty(getStationPathStationName(data.getValue(), false)));
        //rideOfGroupActionsCol.setCellValueFactory(ignored -> null);
        //rideOfGroupActionsCol.setCellFactory(createCallBack());


        ridesOfGroupTableView.setSortPolicy(table -> {
            Comparator<RouteRideDTO> comparator = (o1, o2) -> {
                if (o1 == null || o1.getStartTime() == null) {
                    return 1;
                }
                if (o2 == null || o2.getStartTime() == null) {
                    return -1;
                }
                return o1.getStartTime().compareTo(o2.getStartTime());
            };
            FXCollections.sort(table.getItems(), comparator); //TODO CHECK PERFORMANCE IMPACT
            return true;
        });
        logger.debug("inited table in " + (System.currentTimeMillis() - start) + "ms");
    }

    private String getStationPathStationName(RouteRideDTO dto, boolean first) {
        try {
            LinkedList<PathStationDTO> pathStations = dto.getPathStations();
            StationDTO stationDTO = (first) ? pathStations.getFirst().getConnection().getStart() :
                    pathStations.getLast().getConnection().getEnd();
            return stationDTO.getShortName();
        } catch (NoSuchElementException e) {
            NotificationCreator.createErrorNotification("Internal Error", "Error", null);
            return "";
        }
    }

    @NotNull
    @Contract(value = " -> new", pure = true)
    private static Callback<TableColumn<RouteRideDTO, String>, TableCell<RouteRideDTO, String>> createCallBack() {
        return new Callback<TableColumn<RouteRideDTO, String>, TableCell<RouteRideDTO, String>>() {
            @Override
            public TableCell<RouteRideDTO, String> call(final TableColumn<RouteRideDTO, String> param) {
                return new TableCell<RouteRideDTO, String>() {

                    final FontAwesomeIconView remove = new FontAwesomeIconView(FontAwesomeIcon.TRASH);
                    final Button removeBtn = new Button("", remove);

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            removeBtn.setOnAction(event -> {
                                RouteRideDTO route = getTableView().getItems().get(getIndex());
                                logger.debug("DELETE: " + route);
                                getTableView().getItems().remove(route);
                                new OperationService().removeRideFromGroup(route);
                            });
                            removeBtn.setStyle("-fx-background-color: none");
                            remove.setStyle("-fx-padding: 0 0 0 -3");
                            setGraphic(new HBox(removeBtn));
                            setText(null);
                        }
                    }
                };
            }
        };
    }

    private void initPaneDragAndDrop() {
        selfRoot.setOnDragEntered(Event::consume);
        selfRoot.setOnDragDropped(e -> {
            logger.debug("Drag Dropped");
            logger.debug(e.getGestureSource().toString());
            String string = e.getDragboard().getString();
            Object o = DragAndDropManager.get(string);
            if (o instanceof RouteRideDTO) {
                try {
                    pairOf.addRouteRide((RouteRideDTO) o);
                } catch (GrouperException handleIgnored) {
                    logger.error(
                            "Error: " + handleIgnored.getError().name()
                            //+ " on Entity: " + handleIgnored.getError().getFailOn()
                    );
                }
            } else {
                if (o instanceof BusDTO) {
                    pairOf.addBus((BusDTO) o);
                }
            }
            //DragAndDropManager.remove(o);
            e.consume();
        });

        selfRoot.setOnDragOver(e -> {
            String string = e.getDragboard().getString();
            Object o = DragAndDropManager.get(string);
            if (o instanceof BusDTO) {
                if (pairOf.getOperation().getBusId() == null) {   //SIMPLE CHECK IF GROUP ALREADY HAS A BUS
                    e.acceptTransferModes(TransferMode.ANY);
                }
            } else {
                e.acceptTransferModes(TransferMode.ANY);
            }
            e.consume();
        });
        selfRoot.setOnDragDone(e -> {
            logger.debug("Drag done");
            e.getDragboard().clear();
            e.setDropCompleted(true);
            e.consume();
        });
    }
}
