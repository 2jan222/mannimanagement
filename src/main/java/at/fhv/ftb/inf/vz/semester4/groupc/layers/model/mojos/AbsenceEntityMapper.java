package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.AbsenceEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AbsenceEntityMapper {

    AbsenceEntityMapper INSTANCE = Mappers.getMapper(AbsenceEntityMapper.class);

    Absence toMojo(AbsenceEntity absenceEntity);

    @Mapping(target = "driver", ignore = true)
    AbsenceEntity toPojo(Absence absence);

    void updateMojo(AbsenceEntity absenceEntity, @MappingTarget Absence absence);

    @Mapping(target = "driver", ignore = true)
    void updatePojo(Absence absence, @MappingTarget AbsenceEntity absenceEntity);

    List<Absence> toMojos(List<AbsenceEntity> absenceEntities);

    List<AbsenceEntity> toPojos(List<Absence> absences);
}
