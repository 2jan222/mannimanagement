package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StartTimePersistenceEntityMapper<StartTimeEntity> extends PersistenceEntityMapper<StartTimeEntity> {
    @Override
    public StartTimeEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (StartTimeEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StartTimeEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("StartTimeEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<StartTimeEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from StartTimeEntity ");
            return (List<StartTimeEntity>) query.getResultList();
        }
    }

    @Override
    public List<StartTimeEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from StartTimeEntity " + whereQueryFragment);
            return (List<StartTimeEntity>) query.getResultList();
        }
    }
}
