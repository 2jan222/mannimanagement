package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos;


import org.jetbrains.annotations.Contract;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

/**
 * RouteRideDTO.
 */
public class RouteRideDTO {
    private long id;
    private Integer busId;
    private LocalTime startTime;
    private LocalTime endTime;
    private LocalDate date;
    private Integer routeId;
    private Integer requiredCapacity;
    private LinkedList<PathStationDTO> pathStations;
    private Integer pathId;
    private Integer operationId;
    private String variation;
    private BusDTO bus;
    private Integer routeNumber;
    private Integer operationShiftId;

    @Contract(pure = true)
    public RouteRideDTO(Integer busId, LocalTime startTime, LocalTime endTime, Integer routeId) {
        this.busId = busId;
        this.startTime = startTime;
        this.routeId = routeId;
        this.endTime = endTime;

    }

    @Contract(pure = true)
    public RouteRideDTO() {

    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }


    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getRequiredCapacity() {
        return requiredCapacity;
    }

    public void setRequiredCapacity(Integer requiredCapacity) {
        this.requiredCapacity = requiredCapacity;
    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public BusDTO getBus() {
        return bus;
    }

    public void setBus(BusDTO bus) {
        this.bus = bus;
    }

    public LinkedList<PathStationDTO> getPathStations() {
        return pathStations;
    }

    public void setPathStations(LinkedList<PathStationDTO> pathStations) {
        this.pathStations = pathStations;
    }

    public Integer getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(Integer routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Override
    public String toString() {
        return "RouteRideDTO{" +
                "id=" + id +
                ", busId=" + busId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", date=" + date +
                ", routeId=" + routeId +
                ", requiredCapacity=" + requiredCapacity +
                ", pathStations=" + pathStations.size() +
                ", pathId=" + pathId +
                ", operationId=" + operationId +
                ", variation='" + variation + '\'' +
                ", bus=" + bus +
                ", routeNumber=" + routeNumber +
                '}';
    }

    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }
}
