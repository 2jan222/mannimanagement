package at.fhv.ftb.inf.vz.semester4.groupc.layers.service.route;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;

import java.time.LocalDate;
import java.util.List;

/**
 * RouteCreationService.
 */
@LoggerableClassDisplayName("[RouteCreationService]")
public class RouteCreationService {

    private Loggerable logger = Loggerable.getInstance();

    public boolean isNumberInUse(int number, LocalDate from, LocalDate to) {
        logger.debug("Check for number:" + number + " From: " + from.toString() + " To: " + to.toString());
        List<RouteEntity> all = PersistenceFacade.getInstance().getAll(RouteEntity.class);
        for (RouteEntity e : all) {
            if (e.getRouteNumber() == number) {
                if (!to.isBefore(e.getValidFrom()) || from.isAfter(e.getValidTo())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void persist(int routeNumber, LocalDate dateFrom, LocalDate dateTo, String variation) {
        RouteEntity routeEntity = new RouteEntity();
        routeEntity.setRouteNumber(routeNumber);
        routeEntity.setValidFrom(dateFrom);
        routeEntity.setValidTo(dateTo);
        routeEntity.setVariation(variation);
        PersistenceFacade.getInstance().create(routeEntity);
    }
}
