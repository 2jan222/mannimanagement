package at.fhv.ftb.inf.vz.semester4.groupd.domain;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;

public class PathStation {
	private PathStationEntity _pathStation;
    private Station station;
    private Path path;
    
    public PathStation(PathStationEntity pathStation) {
    	_pathStation = pathStation;
    }
    
    public PathStationEntity getCapsuledEntity() {
		return _pathStation;
	}
   
   public int getPositionOnPath() {
       return _pathStation.getPositionOnPath();
   }

   public void setPositionOnPath(int positionOnPath) {
       _pathStation.setPositionOnPath(positionOnPath);
   }

   public int getDistanceFromPrevious() {   
       return _pathStation.getStationConnector().getDistance();
   }

   public void setDistanceFromPrevious(int distanceFromPrevious) {
       _pathStation.getStationConnector().setDistance(distanceFromPrevious);
   }

   public int getTimeFromPrevious() {
       return _pathStation.getStationConnector().getDuration();
   }

   public void setTimeFromPrevious(int timeFromPrevious) {
	   _pathStation.getStationConnector().setDuration(timeFromPrevious);
   }

   public Station getStation() {
       return new Station(_pathStation.getStationConnector().getStartStation());
   }

   public void setStation(Station station) {
       this.station = station;
   }

   public Path getPath() {
       return new Path(_pathStation.getPathByPathId());
   }

   public void setPath(Path path) {
       this.path = path;
   }
}
