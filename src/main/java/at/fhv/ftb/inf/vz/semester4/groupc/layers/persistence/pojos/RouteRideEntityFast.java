package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "route_ride", schema = "public", catalog = "kmobil")
public class RouteRideEntityFast implements DatabaseEntityMarker {
    private Integer routeRideId;
   // private Integer operationId;
    private Integer startTimeId;
    private Integer routeId;
    private FlatRouteEntity routeEntity;
    //private LocalDate routeRideDate;
    private StartTimeEntity startTimeByStartTimeId;
   // private Integer operationShiftId;
    private Set<OperationEntity> operations;
    private Set<OperationShiftEntity> shifts;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "route_ride_id")
    public Integer getRouteRideId() {
        return routeRideId;
    }

    public void setRouteRideId(Integer routeRideId) {
        this.routeRideId = routeRideId;
    }

    /*@Basic
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

     */

    @Basic
    @Column(name = "start_time_id")
    public Integer getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(Integer startTimeId) {
        this.startTimeId = startTimeId;
    }

    @ManyToOne
    @JoinColumn(name = "start_time_id", referencedColumnName = "start_time_id", nullable = false, insertable = false, updatable = false)
    public StartTimeEntity getStartTimeByStartTimeId() {
        return startTimeByStartTimeId;
    }

    public void setStartTimeByStartTimeId(StartTimeEntity startTimeByStartTimeId) {
        this.startTimeByStartTimeId = startTimeByStartTimeId;
    }
/*
    @Basic
    @Column(name="operation_shift_id")
    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

 */

    @Override
    public String toString() {
        return "\nRouteRideEntityFast{" +
                "routeRideId=" + routeRideId +
                //", operationId=" + operationId +
                ", startTimeId=" + startTimeId +
                ", startTimeByStartTimeId=" + startTimeByStartTimeId +
                //", operationShiftId=" + operationShiftId +
                '}';
    }

   /* @Basic
    @Column(name = "route_ride_date")
    public LocalDate getRouteRideDate() {
        return routeRideDate;
    }

    public void setRouteRideDate(LocalDate routeRideDate) {
        this.routeRideDate = routeRideDate;
    }


    */
    @Basic
    @Column(name = "route_id")
    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route_id", referencedColumnName = "route_id", insertable = false, updatable = false)
    public FlatRouteEntity getRouteEntity() {
        return routeEntity;
    }

    public void setRouteEntity(FlatRouteEntity routeEntity) {
        this.routeEntity = routeEntity;
    }

    @ManyToMany(mappedBy = "routeRidesByOperationId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public Set<OperationEntity> getOperations() {
        return operations;
    }

    public void setOperations(Set<OperationEntity> operations) {
        this.operations = operations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteRideEntityFast that = (RouteRideEntityFast) o;
        return Objects.equals(routeRideId, that.routeRideId) &&
                Objects.equals(startTimeId, that.startTimeId) &&
                Objects.equals(routeId, that.routeId) &&
                Objects.equals(routeEntity, that.routeEntity) &&
                Objects.equals(startTimeByStartTimeId, that.startTimeByStartTimeId) &&
                Objects.equals(operations, that.operations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(routeRideId, startTimeId, routeId, routeEntity, startTimeByStartTimeId, operations);
    }
    @ManyToMany(mappedBy = "routeRidesByOperationShiftId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public Set<OperationShiftEntity> getShifts() {
        return shifts;
    }

    public void setShifts(Set<OperationShiftEntity> shifts) {
        this.shifts = shifts;
    }
}

