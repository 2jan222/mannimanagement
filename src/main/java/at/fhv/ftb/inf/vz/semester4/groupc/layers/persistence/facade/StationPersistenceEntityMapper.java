package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StationPersistenceEntityMapper<StationEntity> extends PersistenceEntityMapper<StationEntity> {

    @Override
    public StationEntity read(Integer id) {

        try (Session sess = DatabaseConnector.getSession()) {
            return (StationEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("StationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<StationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from StationEntity ");
            return (List<StationEntity>) query.getResultList();
        }
    }

    @Override
    public List<StationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from StationEntity " + whereQueryFragment);
            return (List<StationEntity>) query.getResultList();
        }
    }
}
