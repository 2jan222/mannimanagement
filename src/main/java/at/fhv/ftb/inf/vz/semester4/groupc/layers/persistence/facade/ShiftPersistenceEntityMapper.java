package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ShiftPersistenceEntityMapper<ShiftEntity> extends PersistenceEntityMapper<ShiftEntity> {
    @Override
    public ShiftEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (ShiftEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.ShiftEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("ShiftEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<ShiftEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from ShiftEntity ");
            return (List<ShiftEntity>) query.getResultList();
        }
    }

    @Override
    public List<ShiftEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from ShiftEntity " + whereQueryFragment);
            return (List<ShiftEntity>) query.getResultList();
        }
    }
}
