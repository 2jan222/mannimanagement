package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import java.time.LocalDate;
import java.util.LinkedList;

public class Route {
    private Integer routeId;
    private Integer routeNumber;
    private LocalDate validFrom;
    private LocalDate validTo;
    private String variation;
    private LinkedList<Path> paths;
    private LinkedList<RouteRide> routeRides;

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public LinkedList<Path> getPaths() {
        if (paths == null) {
            paths = new LinkedList<>();
        }
        return paths;
    }

    public void setPaths(LinkedList<Path> paths) {
        this.paths = paths;
    }

    public LinkedList<RouteRide> getRouteRides() {
        return routeRides;
    }

    public void setRouteRides(LinkedList<RouteRide> routeRides) {
        this.routeRides = routeRides;
    }

    public Integer getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(Integer routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeId=" + routeId +
                ", routeNumber=" + routeNumber +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", variation='" + variation + '\'' +
                ", paths=" + paths.size() +
                ", routeRides=" + routeRides.size() +
                '}';
    }
}
