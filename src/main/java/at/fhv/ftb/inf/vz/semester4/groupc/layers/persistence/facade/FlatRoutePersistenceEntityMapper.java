package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatRoutePersistenceEntityMapper<FlatRouteEntity> extends PersistenceEntityMapper<FlatRouteEntity>{
    @Override
    public FlatRouteEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (FlatRouteEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatRouteEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("FlatRouteEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatRouteEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatRouteEntity ");
            return (List<FlatRouteEntity>) query.getResultList();
        }
    }

    @Override
    public List<FlatRouteEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatRouteEntity " + whereQueryFragment);
            return (List<FlatRouteEntity>) query.getResultList();
        }
    }
}
