package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftEntryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = ShiftRideDTOMapper.class)
public interface ShiftEntryDTOMapper {

    ShiftEntryDTOMapper INSTANCE = Mappers.getMapper(ShiftEntryDTOMapper.class);

    @Mapping(target = "startTime", source = "start")
    @Mapping(target = "endTime", source = "end")
    @Mapping(target = "busId", source = "busID")
    ShiftEntryDTO toDTO(ShiftEntry shiftEntry);

    @Mapping(target = "driver", ignore = true)
    @Mapping(target = "start", source = "startTime")
    @Mapping(target = "end", source = "endTime")
    @Mapping(target = "busID", source = "busId")
    ShiftEntry toMojo(ShiftEntryDTO shiftEntryDTO);

    @Mapping(target = "startTime", source = "start")
    @Mapping(target = "endTime", source = "end")
    @Mapping(target = "busId", source = "busID")
    void updateDTO(ShiftEntry shiftEntry, @MappingTarget ShiftEntryDTO shiftEntryDTO);

    @Mapping(target = "driver", ignore = true)
    @Mapping(target = "start", source = "startTime")
    @Mapping(target = "end", source = "endTime")
    @Mapping(target = "busID", source = "busId")
    ShiftEntry updateMojo(ShiftEntryDTO shiftEntryDTO, @MappingTarget ShiftEntry shiftEntry);

    List<ShiftEntryDTO> toDTOs(List<ShiftEntry> shiftEntries);

    List<ShiftEntry> toMojos(List<ShiftEntryDTO> shiftEntryDTOS);

}
