package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Initializes Database Connection.
 */
public class DatabaseConnector {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            ourSessionFactory = new Configuration().
                    configure("hibernate.cfg.xml").
                    buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Returns Hibernate Session.
     *
     * @return Session
     * @throws HibernateException if Session cannot be opened.
     */
    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static boolean hasConnection() {
        Session session = getSession();
        boolean sessionConnected = session.isConnected();
        session.close();
        return sessionConnected;
    }

    public static void shutdown() {
        ourSessionFactory.close();
    }

    public static void init() {

    }
}
