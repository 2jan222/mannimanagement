package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StartTimeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {RouteRideEntityMapper.class, PathEntityMapper.class})
public interface StartTimeEntityMapper {
    StartTimeEntityMapper INSTANCE = Mappers.getMapper(StartTimeEntityMapper.class);

    @Mapping(target = "routeRides", source = "routeRidesByStartTimeId")
    @Mapping(target = "path", source = "pathByPathId")
    StartTime toMojo(StartTimeEntity startTimeEntity);

    @Mapping(target = "routeRidesByStartTimeId", source = "routeRides")
    @Mapping(target = "pathByPathId", source = "path")
    StartTimeEntity toPojo(StartTime startTime);

    @Mapping(target = "routeRides", source = "routeRidesByStartTimeId")
    @Mapping(target = "path", source = "pathByPathId")
    void updateMojo(StartTimeEntity startTimeEntity, @MappingTarget StartTime startTime);

    @Mapping(target = "routeRidesByStartTimeId", source = "routeRides")
    @Mapping(target = "pathByPathId", source = "path")
    void updatePojo(StartTime startTime, @MappingTarget StartTimeEntity startTimeEntity);

    List<StartTime> toMojos(List<StartTimeEntity> startTimeEntities);

    List<StartTimeEntity> toPojos(List<StartTime> startTimes);

}
