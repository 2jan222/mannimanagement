package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.FlatStationConnectorEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.PathStation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Route;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationConnectorEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.StationDTOMapper;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.collections.ObservableList;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.TestOnly;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * PathCreator.
 */
@SuppressWarnings("unchecked")
@LoggerableClassDisplayName("[PathCreator]")
public class PathCreator {
    private Station start;
    private Station end;
    private LinkedList<Station> intermediateStations = new LinkedList<>();
    private HashMap<Station, Pair<Station, LinkedList<StationConnector>>> connections = new HashMap<>();
    private static StationPlan stationPlan = StationPlan.getInstance();
    private static Loggerable logger = Loggerable.getInstance();

    @Contract(pure = true)
    public PathCreator(@NotNull Station start, @NotNull Station end) {
        this.start = start;
        this.end = end;
        connections.put(start, new Pair<>(end, new LinkedList<>(Collections.singletonList(
                new StationConnector(start, end, -1, -1)))));
    }


    public static void initStationPlanFromDB() {
        long startMilli = System.currentTimeMillis();
        logger.debug("Load Graph");

        List<StationConnector> stationConnectors =
                FlatStationConnectorEntityMapper.INSTANCE.toMojos(
                        PersistenceFacade.getInstance().getAll(FlatStationConnectorEntity.class)
                );

        logger.debug("Fetched Data in " + (System.currentTimeMillis() - startMilli));
        logger.debug("Size" + stationConnectors.size());
        for (StationConnector stationConnector : stationConnectors) {
            //logger.debug("Start: " + stationConnector.getStart().toString());
            //logger.debug("Ende: " + stationConnector.getEnd().toString());
            boolean b = stationPlan.addStationPath(stationConnector, true);
            //logger.debug("Success: " + b);
        }
        logger.debug("Loaded Graph in " + (System.currentTimeMillis() - startMilli));
    }

    private PathCreator(@NotNull LinkedList<PathStation> pathStation) {
        logger.debug("Construction from pathStations[" + pathStation.size() + "]");

        pathStation.sort(Comparator.comparingInt(PathStation::getPositionOnPath));
        start = pathStation.getFirst().getConnection().getStart();
        end = pathStation.getLast().getConnection().getEnd();
        for (int i = 0; i < pathStation.size() - 1; i++) {
            StationConnector c = pathStation.get(i).getConnection();
            stationPlan.addStationPath(c, true);
            boolean b = addStation(c.getStart(), c.getEnd());
            if (!b) {
                logger.error("Could not add: " + c.getStart() + " : " + c.getEnd());
            }
        }
        StationConnector lastConnection = pathStation.getLast().getConnection();
        stationPlan.addStationPath(lastConnection, true);
        addConnectionToEnd(lastConnection.getStart(), lastConnection);
        updateConnectionLists();
    }

    @Nullable
    public static PathCreator of(@NotNull Path path) {
        logger.debug(path.toString());
        LinkedList<PathStation> pathStations = path.getPathStations();
        return (pathStations != null) ? new PathCreator(pathStations) : null;
    }


    @Nullable
    @Contract("_ -> new")
    public static PathCreator of(@NotNull ObservableList<StationDTO> items) {
        if (items.size() < 2) {
            return null;
        }
        return new PathCreator(StationDTOMapper.INSTANCE.toMojo(items.get(0)),
                StationDTOMapper.INSTANCE.toMojo(items.get(items.size() - 1)));
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH")
    public boolean addStation(@Nullable Station before, @NotNull Station station) {
        if (!(station.equals(start) || station.equals(end))) {
            int index = intermediateStations.indexOf(before) + 1;
            if (index > intermediateStations.size()) {
                intermediateStations.addLast(station);
            } else {
                intermediateStations.add(index, station);
            }
            updateConnections(index);
            return true;
        } else {
            return false;
        }
    }

    private void addConnectionToEnd(Station before, StationConnector connector) {
        LinkedList<StationConnector> l = new LinkedList<>(Collections.singleton(connector));
        if (connections.containsKey(before)) {
            l = connections.get(before).getValue();
        }
        connections.put(before, new Pair<>(connector.getEnd(), l));
    }

    private void updateConnections(int index) {
        LinkedList<StationConnector> connectionsBetween;
        Station previous;
        Station target;
        Station after;
        if (index == 0) {
            previous = start;
            target = intermediateStations.getFirst();
        } else {
            previous = intermediateStations.get(index - 1);
            target = intermediateStations.get(index);
        }
        if (index + 1 < intermediateStations.size()) {
            after = intermediateStations.get(index + 1);
        } else {
            after = end;
        }
        connections.remove(previous);
        connectionsBetween = StationPlan.getConnectionsBetween(previous, target);
        connections.put(previous, new Pair<>(target, connectionsBetween));
        connections.remove(target);
        connectionsBetween = StationPlan.getConnectionsBetween(target, after);
        connections.put(target, new Pair<>(after, connectionsBetween));
    }


    public boolean isPathComplete() {
        logger.debug("IsPathComplete Check");
        Station curr = this.start;
        while (curr != end) {
            logger.debug("Current:" + curr);
            LinkedList<StationConnector> stationConnectors = connections.get(curr).getValue();
            if (stationConnectors.isEmpty()) {
                logger.debug("Path - isComplete: false");
                return false;
            } else {
                logger.debug(stationConnectors.getFirst().toString());
                curr = stationConnectors.getFirst().getEnd();
            }
        }
        logger.debug("Path - isComplete: true");
        return true;
    }

    public HashMap<Station, Pair<Station, LinkedList<StationConnector>>> getConnections() {
        return connections;
    }


    @SuppressWarnings("WeakerAccess")
    public LinkedList<Station> getIntermediateStations() {
        return intermediateStations;
    }

    public LinkedList<Station> getAllStations() {
        LinkedList<Station> stations = new LinkedList<>(intermediateStations);
        stations.addFirst(start);
        stations.addLast(end);
        return stations;
    }


    @TestOnly
    static void setStationPlan(StationPlan plan) {
        stationPlan = plan;
    }


    @SuppressWarnings("WeakerAccess")
    public void updateConnectionLists() {
        connections.keySet().forEach(e -> {
            Pair<Station, LinkedList<StationConnector>> pair = connections.get(e);
            LinkedList<StationConnector> connectionsBetween = StationPlan.getConnectionsBetween(e, pair.getKey());
            connections.put(e, new Pair<>(pair.getKey(), connectionsBetween));
        });
    }

    public Path toPath(@NotNull Route route, boolean isToPath) {
        Path path;
        try {
            path = route.getPaths().get((isToPath) ? 0 : 1);
            if (path == null) {
                path = new Path();
            }
        } catch (IndexOutOfBoundsException e) {
            path = new Path();
        }
        LinkedList<PathStation> pathStations = new LinkedList<>();
        int i = 1;
        for (Map.Entry<Station, Pair<Station, LinkedList<StationConnector>>> e : connections.entrySet()) {
            logger.debug(e.toString(), LoggerableColor.ANSI_YELLOW);
            StationConnector first;
            StationPlan.addStationPath(new StationConnector(e.getKey(), e.getValue().getKey(), -1, -1), true);
            if (e.getValue().getValue() != null && !e.getValue().getValue().isEmpty()) {
                first = e.getValue().getValue().getFirst();
                Integer pathId = path.getPathId();
                LinkedList<StationConnector> connectionsBetween = StationPlan.getConnectionsBetween(e.getKey(),
                        e.getValue().getKey());

                System.out.println("connectionsBetween.size() = " + connectionsBetween.size());
                connectionsBetween.forEach(d -> logger.debug(d.toString()));
                System.out.println("pathId = " + pathId);
                System.out.println("first = " + first);
                System.out.println(i);
                PathStation pathStation = new PathStation(pathId, i, first);
                pathStations.add(pathStation);
            }
            i++;
        }
        path.setPathStations(pathStations);
        path.setRouteId(route.getRouteId());
        if (isToPath) {
            LinkedList<Path> paths = route.getPaths();
            if (paths == null) {
                route.setPaths(new LinkedList<>());
            }
            if (!route.getPaths().isEmpty()) {
                route.getPaths().removeFirst();
            }
            route.getPaths().addFirst(path);
        } else {
            route.getPaths().remove(1);
            if (route.getPaths().size() > 2) {
                route.getPaths().add(1, path);
            } else {
                route.getPaths().addLast(path);
            }
        }
        return path;
    }
}
