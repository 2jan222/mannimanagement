package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatStationPersistenceEntityMapper<FlatStationEntity> extends PersistenceEntityMapper<FlatStationEntity>{
    @Override
    public FlatStationEntity read(Integer id) {

        try (Session sess = DatabaseConnector.getSession()) {
            return (FlatStationEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("FlatStationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatStationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatStationEntity ");
            return (List<FlatStationEntity>) query.getResultList();
        }
    }

    @Override
    public List<FlatStationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatStationEntity " + whereQueryFragment);
            return (List<FlatStationEntity>) query.getResultList();
        }
    }
}
