package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import java.util.LinkedList;

public class Path {

    private Integer pathId;
    private Integer routeId;
    private LinkedList<PathStation> pathStations;

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }


    public LinkedList<PathStation> getPathStations() {
        if (pathStations == null) {
            pathStations = new LinkedList<>();
        }
        return pathStations;
    }

    public void setPathStations(LinkedList<PathStation> pathStations) {
        this.pathStations = pathStations;
    }

    @Override
    public String toString() {
        return "Path{" +
                "pathId=" + pathId +
                ", routeId=" + routeId +
                ", pathStations=" + pathStations +
                '}';
    }
}
