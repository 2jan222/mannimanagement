package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.station.create;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.stationmanagement.StationCRUDService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.inject.Inject;

/**
 * Presenter for operation add.
 */
public class StationCreatePresenter {
    @Inject private StationCRUDService crudService;
    @FXML private Button saveBtn;
    @FXML private TextArea descriptionTextArea;
    @FXML private TextField abbreviationTextField;
    @FXML private TextField numberTextField;
    @FXML private TextField nameTextField;
    @FXML private Label reason;

    public void saveAction(ActionEvent actionEvent) {
        String sb = validateName() +
                validateNumber() +
                validateAbbreviation() +
                validateDescription();
        reason.setText(sb);
        if ("".equals(sb)) {
            crudService.createStation(
                    nameTextField.getText(),
                    Integer.parseInt(numberTextField.getText()),
                    abbreviationTextField.getText(),
                    descriptionTextArea.getText()
            );
            ((Stage) descriptionTextArea.getScene().getWindow()).close();
        }
    }

    private String validateName() {
        String text = nameTextField.getText();
        if ("".equals(text)) {
            return "Name must not be empty";
        } else {
            return "";
        }
    }

    private String validateNumber() {
        String text = numberTextField.getText();
        if ("".equals(text)) {
            return "Number must not be empty";
        } else {
            if (text.matches("^[0-9]+$")) {
                if (crudService.numberUsed(Integer.parseInt(text))) {
                    return "Number already in use";
                } else {
                    return "";
                }
            } else {
                return "Number may only contain digits";
            }
        }
    }

    private String validateAbbreviation() {
        return "";
    }

    private String validateDescription() {
        return "";
    }
}
