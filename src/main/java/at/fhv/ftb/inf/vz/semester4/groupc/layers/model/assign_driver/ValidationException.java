package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class ValidationException extends Exception {
    public ValidationException() {
        super();
    }

    public ValidationException(ValidationType... types) {
        this(Arrays.stream(types).map(Enum::name).collect(Collectors.joining(" , ")));
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    protected ValidationException(String message, Throwable cause, boolean enableSuppression,
                                  boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ValidationException(@NotNull ValidationType type, Throwable cause) {
        this(type.name(), cause);
    }
}
