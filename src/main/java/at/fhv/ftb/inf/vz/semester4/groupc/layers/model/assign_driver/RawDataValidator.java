package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.util.LinkedList;

@SuppressWarnings("RedundantThrows")
class RawDataValidator {
    static void hasOperationBus(@NotNull OperationFlat operation) throws ValidationException {
        LinkedList<ValidationType> errors = new LinkedList<>();
        if (operation.getBusId() == null) {
            errors.add(ValidationType.BUS_ID_NULL);
        }
        if (!errors.isEmpty()) {
            throw new ValidationException(ValidationType.OPERATION_HAS_NO_BUS,
                    new ValidationException(errors.toArray(new ValidationType[0])));
        }
    }

    @Contract("null -> fail")
    static void hasOperationId(@Nullable OperationFlat operation) throws ValidationException {
        if (operation == null) {
            throw new ValidationException(ValidationType.OPERATION_ID_NULL,
                    new ValidationException(ValidationType.OPERATION_NULL));
        } else {
            if (operation.getOperationId() == null) {
                throw new ValidationException(ValidationType.OPERATION_ID_NULL);
            }
        }
    }

    static void validateOperation(@Nullable OperationFlat operation) throws ValidationException {
        hasOperationId(operation);
        hasOperationBus(operation);
    }

    static void validateDriver(Driver driver, LocalDate date) throws ValidationException {
        //To be implemented.
    }

    static void validateShift(ShiftEntry shiftEntry) throws ValidationException {
        //To be implemented.
    }
}
