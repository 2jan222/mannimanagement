package at.fhv.ftb.inf.vz.semester4.groupc.layers.service.stationmanagement;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.StationDTOMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

/**
 * UI Service for Stations.
 * Aim: Support of CRUD operations.
 */
@SuppressWarnings("unchecked")
public class StationCRUDService {
    public boolean numberUsed(int number) {
        return false;
    }

    @SuppressWarnings("UnusedReturnValue")
    public @Nullable List<StationCRUDError> createStation(
            @NotNull String name,
            int stationId,
            @NotNull String abbreviation,
            @NotNull String description) {
        LinkedList<StationCRUDError> errors = new LinkedList<>();
        //TODO Validate Data
        StationEntity station = new StationEntity();
        station.setStationName(name);
        station.setStationId(stationId);
        station.setShortName(abbreviation);
        PersistenceFacade.getInstance().create(station);
        return errors;
    }


    public ObservableList<StationDTO> getStationObservables() {
        List<StationEntity> list = PersistenceFacade.getInstance().getAll(StationEntity.class);
        return FXCollections.observableArrayList(
                StationDTOMapper.INSTANCE.toDTOs(StationEntityMapper.INSTANCE.toMojos(list))
        );
    }

    /**
     * ErrorTypes of StationCRUD.
     */
    @SuppressWarnings("unused")
    public enum StationCRUDError {
        FIELD_EMPTY, NUMBER_USED, NAME_USED, ABBREVIATION_USED
    }
}
