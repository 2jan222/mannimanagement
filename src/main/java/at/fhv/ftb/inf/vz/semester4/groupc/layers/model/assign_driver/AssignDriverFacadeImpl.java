package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.DriverDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftEntryDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.BusDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.DriverDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.ShiftEntryDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.ShiftRideDTOMapper;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

@LoggerableClassDisplayName("[AssignDriverFacadeImpl]")
public class AssignDriverFacadeImpl implements AssignDriverFacade {
    private static Loggerable logger = Loggerable.getInstance();
    private static ContextMediator contextMediator = new ContextMediator();
    private static AssignDriverDataTransfer dataTransfer = new DataSupplier();

    private static DataContext loadDataForDay(LocalDate date) {
        DataContextBuilder builder = DataContext.builder(date);
        long start = System.currentTimeMillis();
        logger.debug("Loading Drivers");
        for (Driver driver : dataTransfer.getDriverForDay(date)) {
            try {
                builder.addDriverOnDuty(driver);
            } catch (ValidationException e) {
                logger.error(e.getMessage());
            }
        }
        logger.debug("Loading Drivers took " + (System.currentTimeMillis() - start) + " ms");
        start = System.currentTimeMillis();
        logger.debug("Loading Operations");
        LinkedList<OperationFlat> operationsForDay = dataTransfer.getOperationsForDay(date);
        for (OperationFlat opt : operationsForDay) {
            try {
                builder.addOperation(opt);
            } catch (ValidationException e) {
                logger.error(e.getMessage());
            }
        }
        logger.debug("Loading " + operationsForDay.size() + " Operations took " + (System.currentTimeMillis() - start) + " ms");
        start = System.currentTimeMillis();
        logger.debug("Loading Shift Entries");
        LinkedList<ShiftEntry> shiftEntriesForDay = dataTransfer.getShiftEntriesForDay(date);
        for (ShiftEntry shiftE : shiftEntriesForDay) {
            try {
                builder.addShift(shiftE);
            } catch (ValidationException e) {
                logger.error(e.getMessage());
            }
        }
        logger.debug("Loading " + shiftEntriesForDay.size() + " shifts took " + (System.currentTimeMillis() - start) + " ms");
        return builder.buildContext();
    }

    @SuppressFBWarnings("ST_WRITE_TO_STATIC_FROM_INSTANCE_METHOD")
    public void init(AssignDriverDataTransfer dataTransfer) {
        AssignDriverFacadeImpl.dataTransfer = dataTransfer;
        AssignDriverFacadeImpl.contextMediator = new ContextMediator();
    }

    @Override
    public LinkedList<ShiftRide> getRidesByOperationId(Integer id, LocalDate date) throws AssignDriverContextCreationException {
        DataContext contextOfDate = getContextOfDate(date);
        OperationFlat op = contextOfDate.getOperationByOperationId(id);
        if (op != null) {
            System.out.println("HERE");
            return op.getShiftRides();
        } else {
            return new LinkedList<>();
        }
    }

    @Override
    public LinkedList<Integer> getOperationsByIDOfDay(LocalDate date) {
        logger.debug("GET operation ids of date: " + date);
        DataContext dataContext;
        try {
            dataContext = getContextOfDate(date);
            return dataContext.getOperationIds();
        } catch (AssignDriverContextCreationException e) {
            logger.error(e.getMessage());
        }
        return new LinkedList<>();
    }

    public LinkedList<ShiftEntryDTO> getShiftsByOperationId(Integer operationId, LocalDate date)
            throws AssignDriverContextCreationException {
        logger.debug("GET shifts of operationId: " + operationId);
        DataContext contextOfDate = getContextOfDate(date);
        return new LinkedList<>(ShiftEntryDTOMapper.INSTANCE.toDTOs(contextOfDate.getShiftEntriesForOperation(operationId)));
    }

    @Override
    public LinkedList<DriverDTO> getFreeDriverFormShift(ShiftEntryDTO shiftEntryDTO)
            throws AssignDriverContextCreationException {
        ShiftEntry shiftEntry = ShiftEntryDTOMapper.INSTANCE.toMojo(shiftEntryDTO);
        System.out.println("shiftEntry = " + shiftEntry);
        DataContext context = getContextOfDate(shiftEntry.getDate());
        LinkedList<Driver> drivers = AssignDriverActions.calculateDriversApplicableForShift(shiftEntry, context);
        return new LinkedList<>(DriverDTOMapper.INSTANCE.toDTOs(drivers));
    }

    @Override
    public OperationDTO getOperationFromId(Integer operationId, LocalDate date) {
        return null;
    }

    @Override
    public ShiftEntryDTO setShift(Integer operationId, LinkedList<ShiftRide> rides, LocalDate date)
            throws AssignDriverContextCreationException {
        DataContext context = getContextOfDate(date);
        ShiftEntry newShift = AssignDriverActions.createNewShift(operationId, rides, context);
        //updateMediator(context);
        return (newShift != null) ? ShiftEntryDTOMapper.INSTANCE.toDTO(newShift) : null;
    }

    @Override
    public ShiftEntry setShiftForTimeFrame(Integer operationId, LocalTime start, LocalTime end, LocalDate date)
            throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException {
        DataContext context = getContextOfDate(date);
        OperationFlat operation = context.getOperationByOperationId(operationId);
        ShiftEntry newShift = null;
        if (operation != null) {
            newShift = AssignDriverActions.createNewShiftForTimeframe(operation, start, end, context);
            System.out.println("newShift = " + newShift);
            if (newShift != null) {
                dataTransfer.updateOrCreateShiftEntry(newShift);
                context.addShift(newShift);
            }
        }
        return newShift;
    }

    @Override
    public boolean addRideToShift(ShiftEntryDTO shiftDTO, RouteRideDTO toAdd) {
        return false;
    }

    @Override
    public boolean removeRideFromShift(ShiftEntryDTO shiftDTO, RouteRideDTO toRemove) {
        return false;
    }

    @Override
    public boolean removeAllRidesAndDriverFromShift(ShiftEntryDTO shiftDTO) {
        return false;
    }


    @Override
    public boolean assignDriverToShift(@NotNull ShiftEntryDTO shiftDTO, @NotNull DriverDTO driverDTO)
            throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException {
        DataContext context = getContextOfDate(shiftDTO.getDate());
        Driver driver = context.getDriverById(driverDTO.getDriverId());
        System.out.println("adfsdghjhkljl-" + shiftDTO.getShiftEntryId());
        ShiftEntry shiftEntry = context.getShiftEntryById(shiftDTO.getShiftEntryId());
        System.out.println("IS IT NULL ?! " + shiftEntry);
        boolean b = AssignDriverActions.assignDriverToShift(driver, shiftEntry, context);
        logger.debug("AssignDriver" + b);
        if (b) {
            ShiftEntryDTOMapper.INSTANCE.updateDTO(shiftEntry, shiftDTO);
            dataTransfer.updateOrCreateShiftEntry(shiftEntry);
            //updateMediator(context);
        }
        return b;
    }

    @Override
    public boolean removeDriverFromShift(@NotNull ShiftEntryDTO shiftDTO)
            throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException {
        DataContext context = getContextOfDate(shiftDTO.getDate());
        ShiftEntry shiftEntry = context.getShiftEntryById(shiftDTO.getShiftEntryId());
        boolean b = AssignDriverActions.removeDriverFromShift(shiftEntry, context);
        if (b) {
            ShiftEntryDTOMapper.INSTANCE.updateDTO(shiftEntry, shiftDTO);
            dataTransfer.updateOrCreateShiftEntry(shiftEntry);
            //updateMediator(context);
        }
        return b;
    }

    @Override
    public boolean removeShift(ShiftEntryDTO shiftDTO) {
        return false;
    }

    @Override
    public boolean isSaved(LocalDate date) {
        return false;
    }

    @Override
    public boolean save(LocalDate date) {
        return false;
    }

    @Override
    public void clearContextForAllDates() {
        contextMediator.clear();
    }

    @Override
    public BusDTO getBusByOperationId(Integer operationId, LocalDate date) throws AssignDriverContextCreationException {
        Bus busByOperationId = getContextOfDate(date).getBusByOperationId(operationId);
        return (busByOperationId != null) ? BusDTOMapper.INSTANCE.toDTO(busByOperationId) : null;
    }

    @Override
    public LinkedList<LocalDate> getDatesWithOperationsWhichHaveBus() {
        LinkedList<LocalDate> dates = dataTransfer.getDatesWithOperationWhichHaveBus();
        dates.sort(LocalDate::compareTo);
        return dates;
    }

    @Override
    public Driver getDriverById(Integer driverId, LocalDate date) throws AssignDriverContextCreationException {
        return getContextOfDate(date).getDriverById(driverId);
    }

    @NotNull
    private DataContext getContextOfDate(LocalDate date) throws AssignDriverContextCreationException {
        DataContext value = contextMediator.getValue(date);
        if (value == null) {
            DataContext context = loadDataForDay(date);
            contextMediator.setValue(date, context);
            value = contextMediator.getValue(date);
            if (value == null) {
                throw new AssignDriverContextCreationException("Creation of context failed");
            } else {
                return value;
            }
        } else {
            return value;
        }
    }

    @Deprecated
    private void updateMediator(@NotNull DataContext context) {
        contextMediator.setValue(context.getDate(), context);
    }
}
