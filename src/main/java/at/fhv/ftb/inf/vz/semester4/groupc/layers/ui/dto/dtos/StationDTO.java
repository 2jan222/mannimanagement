package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos;

/**
 * StationDTO.
 */
public class StationDTO {
    private Integer stationId;
    private String stationName;
    private String shortName;

    public StationDTO() {}

    public StationDTO(Integer stationId, String stationName, String shortName) {
        this.stationId = stationId;
        this.stationName = stationName;
        this.shortName = shortName;
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return "StationDTO{" +
                "stationId=" + stationId +
                ", stationName='" + stationName + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
