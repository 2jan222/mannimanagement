package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.TestOnly;

public class Bus {
    private Integer busId;
    private String licenceNumber;
    private String make;
    private String model;
    private Integer seatPlaces;
    private Integer standPlaces;
    private String note;

    @TestOnly
    public Bus(Integer busId) {
        this.busId = busId;
    }

    public Bus() {
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(int busId) {
        this.busId = busId;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSeatPlaces() {
        return seatPlaces;
    }

    public void setSeatPlaces(int seatPlaces) {
        this.seatPlaces = seatPlaces;
    }

    public int getStandPlaces() {
        return standPlaces;
    }

    public void setStandPlaces(int standPlaces) {
        this.standPlaces = standPlaces;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "busId=" + busId +
                ", licenceNumber='" + licenceNumber + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", seatPlaces=" + seatPlaces +
                ", standPlaces=" + standPlaces +
                ", note='" + note + '\'' +
                '}';
    }
}
