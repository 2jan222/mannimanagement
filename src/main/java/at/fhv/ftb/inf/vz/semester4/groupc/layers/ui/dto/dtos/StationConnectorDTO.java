package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos;

/**
 * StationConnectorDTO.
 */
public class StationConnectorDTO {
    private Integer connectionId;
    private StationDTO start;
    private StationDTO end;
    private Integer distanceFromPrevious;
    private Integer timeFromPrevious;
    private Integer startId;
    private Integer pathStationId;

    public StationConnectorDTO() {
    }

    public StationConnectorDTO(Integer connectionId, StationDTO start, StationDTO end) {
        this.connectionId = connectionId;
        this.start = start;
        this.end = end;
    }

    private Integer endId;

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public StationDTO getStart() {
        return start;
    }

    public void setStart(StationDTO start) {
        this.start = start;
    }

    public StationDTO getEnd() {
        return end;
    }

    public void setEnd(StationDTO end) {
        this.end = end;
    }

    public Integer getDistanceFromPrevious() {
        return distanceFromPrevious;
    }

    public void setDistanceFromPrevious(Integer distanceFromPrevious) {
        this.distanceFromPrevious = distanceFromPrevious;
    }

    public Integer getTimeFromPrevious() {
        return timeFromPrevious;
    }

    public void setTimeFromPrevious(Integer timeFromPrevious) {
        this.timeFromPrevious = timeFromPrevious;
    }

    public Integer getStartId() {
        return startId;
    }

    public void setStartId(Integer startId) {
        this.startId = startId;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    @Override
    public String toString() {
        return "StationConnectorDTO{" +
                "connectionId=" + connectionId +
                ", start=" + start +
                ", end=" + end +
                ", distanceFromPrevious=" + distanceFromPrevious +
                ", timeFromPrevious=" + timeFromPrevious +
                ", startId=" + startId +
                ", endId=" + endId +
                '}';
    }

    public Integer getPathStationId() {
        return pathStationId;
    }

    public void setPathStationId(Integer pathStationId) {
        this.pathStationId = pathStationId;
    }
}
