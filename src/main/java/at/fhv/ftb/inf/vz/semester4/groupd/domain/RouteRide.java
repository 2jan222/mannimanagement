package at.fhv.ftb.inf.vz.semester4.groupd.domain;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatRouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IRoute;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IRouteRide;

import org.hibernate.cfg.NotYetImplementedException;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class RouteRide implements IRouteRide {
    private RouteRideEntityFast _routeRideEntity;
    
    private StartTime _startTime;

    public void setRouteRideEntity(RouteRideEntityFast _routeRideEntity) {
        this._routeRideEntity = _routeRideEntity;
    }

    public void setStartTime(StartTime _startTime) {
        this._startTime = _startTime;
    }

    public void setStartStation(Station _startStation) {
        this._startStation = _startStation;
    }

    public void setEndStation(Station _endStation) {
        this._endStation = _endStation;
    }

    public void setStartingTime(LocalTime _startingTime) {
        this._startingTime = _startingTime;
    }

    public void setEndingTime(LocalTime _endingTime) {
        this._endingTime = _endingTime;
    }

    private Station _startStation;
    private Station _endStation;
    private LocalTime _startingTime;
    private LocalTime _endingTime;

    public RouteRide(RouteRideEntityFast routeRideEntity) {
        _routeRideEntity = routeRideEntity;
    }

    public RouteRide(RouteRideEntity routeRideEntity) {
        _routeRideEntity = new RouteRideEntityFast();
       // _routeRideEntity.setOperationId(routeRideEntity.getOperationId());
        //_routeRideEntity.setOperationShiftId(routeRideEntity.getOperationShiftId());
        _routeRideEntity.setStartTimeByStartTimeId(routeRideEntity.getStartTimeByStartTimeId());
        _routeRideEntity.setRouteId(routeRideEntity.getRouteId());
        //_routeRideEntity.setRouteRideDate(routeRideEntity.getRouteRideDate());
        _routeRideEntity.setRouteRideId(routeRideEntity.getRouteRideId());
        _routeRideEntity.setStartTimeId(routeRideEntity.getStartTimeId());
        FlatRouteEntity flatRouteEntity = new FlatRouteEntity();
        flatRouteEntity.setRouteId(routeRideEntity.getRouteByRouteId().getRouteId());
        flatRouteEntity.setRouteNumber(routeRideEntity.getRouteByRouteId().getRouteNumber());
        _routeRideEntity.setRouteEntity(flatRouteEntity);
    }

    public RouteRide() {
        this(new RouteRideEntityFast());
    }

    public RouteRideEntityFast getCapsuledEntity() {
    	return _routeRideEntity;
    }

    @Override
    public int getRouteRideId() {
        return _routeRideEntity.getRouteRideId();
    }

    @Override
    @Deprecated
    public List<Operation> getOperations() {
        if (_routeRideEntity.getOperations() == null) {
            return null;
        }
        //return _routeRideEntity.getOperations().stream().map(Operation::new).collect(Collectors.toList());
        throw new NotYetImplementedException();
    }

    @Override
    public IRoute getRoute() {
        return new Route(new RouteEntity(_routeRideEntity.getRouteId(),_routeRideEntity.getRouteEntity().getRouteNumber(), null, null, null, null, null));
    }

    @Override
    public StartTime getStartTime() {
    	if(_startTime == null) {
    		_startTime = new StartTime(_routeRideEntity.getStartTimeByStartTimeId());
    	}
        return _startTime;
    }
    
    public LocalTime getStartingTime() {
    	if (_startingTime == null) {
    		setNeededAttributes();
    	}
    	
    	return _startingTime;
    }

    public LocalTime getEndingTime() {
    	if (_endingTime == null) {
    		setNeededAttributes();
    	}
    	
    	return _endingTime;
    }
    
    public String getStartStationName() {
    	if(_startStation == null) {
    		setNeededAttributes();
    	}
    	
    	return _startStation == null ? "" : _startStation.getStationName();
    }
    
    public String getEndStationName() {
    	if(_endStation == null) {
    		setNeededAttributes();
    	}
    	
    	return _endStation == null ? "" : _endStation.getStationName();
    }
    
    private void setNeededAttributes() {
    	for(PathStation station : getStartTime().getPath().getPathStations()) {
			if(station.getPositionOnPath() == 1) {
				_startStation = station.getStation();
			}
			if(station.getPositionOnPath() == getStartTime().getPath().getPathStations().size()) {
				_endStation = station.getStation();
			}
		}
    	
    	// wenn eine Fahrt hin und zur�ck geht, w�rde das unten eventuell funktionieren
//    	for (Path path : getRoute().getPaths()) {
//    	    if(!path.isRetour()){
//                for(PathStation station : getStartTime().getPath().getPathStations()) {
//                    if(station.getPositionOnPath() == 1) {
//                        _startStation = station.getStation();
//                    }
//                    if (station.getPositionOnPath() == path.getPathStations().size()) {
//                        _endStation = station.getStation();
//                    }
//                    duration += station.getTimeFromPrevious();
//                }
//            } else {
//                List<PathStation> pathStations = path.getPathStations();
//                for (PathStation station : pathStations) {
//                    if (station.getPositionOnPath() == pathStations.size()) {
//                        _endStation = station.getStation();
//                    }
//                    duration += station.getTimeFromPrevious();
//                }
//            }
//        }
    	
    	_startingTime = getStartTime().getStartTime();
    	_endingTime = _startingTime.plusMinutes(getStartTime().getPath().getDuration());
    }

    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof RouteRide) {
    		RouteRide ride = (RouteRide)obj;
    		return _routeRideEntity.equals(ride.getCapsuledEntity());
    	}
    	return false;
    }
    
    @Override
    public int hashCode() {
    	return _routeRideEntity.hashCode();
    }
}
