package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.Contract;

import java.time.LocalDate;
import java.util.LinkedList;

public class Operation {
    private Integer operationId;
    private LocalDate date;
    private Integer busId;
    private LinkedList<RouteRide> routeRides;
    private Bus bus;

    @Contract(pure = true)
    public Operation() {
    }

    @Contract(pure = true)
    public Operation(Integer operationId, LocalDate date, Integer busId, LinkedList<RouteRide> routeRides, Bus bus) {
        this.operationId = operationId;
        this.date = date;
        this.busId = busId;
        this.routeRides = routeRides;
        this.bus = bus;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public LinkedList<RouteRide> getRouteRides() {
        if (routeRides == null) {
            routeRides = new LinkedList<>();
        }
        return routeRides;
    }

    public void setRouteRides(LinkedList<RouteRide> routeRides) {
        this.routeRides = routeRides;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "operationId=" + operationId +
                ", date=" + date +
                ", busId=" + busId +
                //", routeRides=" + routeRides +
                ", bus=" + bus +
                '}';
    }
}
