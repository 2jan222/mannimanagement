package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationConnectorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = FlatStationEntityMapper.class)
public interface FlatStationConnectorEntityMapper {

    FlatStationConnectorEntityMapper INSTANCE = Mappers.getMapper(FlatStationConnectorEntityMapper.class);

    @Mapping(target = "pathStationId", ignore = true)
    @Mapping(target = "start", source = "startStation")
    @Mapping(target = "timeFromPrevious", source = "duration")
    @Mapping(target = "startId", source = "startStationId")
    @Mapping(target = "endId", source = "endStationId")
    @Mapping(target = "end", source = "endStation")
    @Mapping(target = "distanceFromPrevious", source = "distance")
    @Mapping(target = "connectionId", source = "stationConnectorId")
    StationConnector toMojo(FlatStationConnectorEntity stationConnectorEntity);

    @Mapping(target = "stationConnectorId", source = "connectionId")
    @Mapping(target = "startStationId", source = "startId")
    @Mapping(target = "startStation", source = "start")
    @Mapping(target = "endStationId", source = "endId")
    @Mapping(target = "duration", source = "timeFromPrevious")
    @Mapping(target = "endStation", source = "end")
    @Mapping(target = "distance", source = "distanceFromPrevious")
    FlatStationConnectorEntity toPojo(StationConnector stationConnector);


    @Mapping(target = "pathStationId", ignore = true)
    @Mapping(target = "start", source = "startStation")
    @Mapping(target = "timeFromPrevious", source = "duration")
    @Mapping(target = "startId", source = "startStationId")
    @Mapping(target = "endId", source = "endStationId")
    @Mapping(target = "end", source = "endStation")
    @Mapping(target = "distanceFromPrevious", source = "distance")
    @Mapping(target = "connectionId", source = "stationConnectorId")
    void updateMojo(FlatStationConnectorEntity stationConnectorEntity, @MappingTarget StationConnector stationConnector);

    @Mapping(target = "stationConnectorId", source = "connectionId")
    @Mapping(target = "startStationId", source = "startId")
    @Mapping(target = "startStation", source = "start")
    @Mapping(target = "endStationId", source = "endId")
    @Mapping(target = "duration", source = "timeFromPrevious")
    @Mapping(target = "endStation", source = "end")
    @Mapping(target = "distance", source = "distanceFromPrevious")
    void updatePojo(StationConnector stationConnector, @MappingTarget FlatStationConnectorEntity stationConnectorEntity);

    List<StationConnector> toMojos(List<FlatStationConnectorEntity> stationConnectorEntities);

    List<FlatStationConnectorEntity> toPojos(List<StationConnector> stationConnectors);
}
