package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRideEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.tour.CreateOperationFacadeImpl;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationRouteRideEntityFast;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.Operation;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.Route;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.Date;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 * Default main. Auto generated.
 */
@Deprecated
public class Main {

    private static final SessionFactory ourSessionFactory;

    static {
        try {
            ourSessionFactory = new Configuration().
                    configure("hibernate.cfg.xml").
                    buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    @SuppressFBWarnings("WMI_WRONG_MAP_ITERATOR")
    public static void main2(final String[] args) {
        CreateOperationFacadeImpl impl = new CreateOperationFacadeImpl();
        List<Operation> operationList = impl.getOperationsByDate(Date.valueOf(LocalDate.of(2019, 7, 22)));
        for(Operation o: operationList) {
            System.out.println("o = " + o);
            System.out.println("oe = " + o.getCapsuledEntity());
        }
        List<Operation> operationList1 = impl.getOperationsByMonth(7);
        for(Operation o: operationList1) {
            System.out.println("o = " + o);
            System.out.println("oe = " + o.getCapsuledEntity());
        }
        List<Route> routes = (List<Route>) impl.getValidRoutesByDay(LocalDate.of(2019, 7, 22));
        for(Route r: routes) {
            System.out.println("r = " + r);
        }
        List<Route> routes1 = (List<Route>) impl.getValidRoutesByMonth(7);
        for(Route r: routes) {
            System.out.println("r = " + r);
        }
        Operation o = new Operation();
        impl.saveOperation(o);

        Operation o1 = new Operation();
        Operation o2 = new Operation();
        Operation o3 = new Operation();
        Operation o4 = new Operation();
        List<Operation> ops = new LinkedList<>();
        ops.add(o1);
        ops.add(o2);
        ops.add(o3);
        ops.add(o4);
        impl.saveOperations(ops);
        impl.deleteOperation(operationList.get(0));
        operationList.remove(0);

    }

    public static void main(String[] args) {
       /* List<RouteRideEntity> all = PersistenceFacade.getInstance().getAll(RouteRideEntity.class);
        System.out.println(all.size());
        all.forEach(e -> System.out.println(ShiftRideEntityMapper.INSTANCE.toMojo((RouteRideEntityFast) e)));

        */
    }

}
