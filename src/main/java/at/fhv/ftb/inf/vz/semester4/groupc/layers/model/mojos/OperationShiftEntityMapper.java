package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationShiftEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import org.jetbrains.annotations.NotNull;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Deprecated
@Mapper(uses = DriverEntityMapper.class)
public interface OperationShiftEntityMapper {
/*
    OperationShiftEntityMapper INSTANCE = Mappers.getMapper(OperationShiftEntityMapper.class);

    @Mapping(target = "shiftRides", source = "operationShiftEntity", qualifiedByName = "routeRideEntitiesToShiftRides")
    @Mapping(target = "driver", source = "driverByDriverId")
    OperationShift toMojo(OperationShiftEntity operationShiftEntity);

    @Mapping(target = "routeRidesByOperationShiftId", source = "operationShift", qualifiedByName = "shiftRidesToRouteRideEntities")
    @Mapping(target = "driverByDriverId", source = "driver")
    OperationShiftEntity toPojo(OperationShift operationShift);

    @Mapping(target = "shiftRides", source = "operationShiftEntity", qualifiedByName = "routeRideEntitiesToShiftRides")
    @Mapping(target = "driver", source = "driverByDriverId")
    void updateMojo(OperationShiftEntity operationShiftEntity, @MappingTarget OperationShift operationShift);

    @Mapping(target = "routeRidesByOperationShiftId", source = "operationShift", qualifiedByName = "shiftRidesToRouteRideEntities")
    @Mapping(target = "driverByDriverId", source = "driver")
    void updatePojo(OperationShift operationShift, @MappingTarget OperationShiftEntity operationShiftEntity);

    List<OperationShift> toMojos(List<OperationShiftEntity> operationShiftEntities);

    List<OperationShiftEntity> toPojos(List<OperationShift> operationShifts);

    @Named("routeRideEntitiesToShiftRides")
    default LinkedList<ShiftRide> routeRideEntitiesToShiftRides(@NotNull OperationShiftEntity pojo) {
        List<RouteRideEntity> routeRides = new ArrayList<>(pojo.getRouteRidesByOperationShiftId());
        return new LinkedList<>(ShiftRideEntityMapper.INSTANCE.toMojos(routeRides));
    }

    @Named("shiftRidesToRouteRideEntities")
    default Set<RouteRideEntity> shiftRidesToRouteRideEntities(@NotNull OperationShift mojo) {
        List<ShiftRide> shiftRides = mojo.getShiftRides();
        return new HashSet<>(ShiftRideEntityMapper.INSTANCE.toPojos(shiftRides));
    }*/
}
