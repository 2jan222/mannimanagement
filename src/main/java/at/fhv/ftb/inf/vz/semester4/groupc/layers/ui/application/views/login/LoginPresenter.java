package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.login;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.login.LoginService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuIntegratedPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuIntegratedView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.NotificationCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;


/**
 * LoginPresenter
 * Handles behavior and interaction of the login screen.
 */
@LoggerableClassDisplayName("[LoginPresenter]")
public class LoginPresenter {
    @Inject
    private Loggerable logger;
    @Inject
    private LoginService loginService;

    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordField;

    @FXML
    private void sendOnEnter(@NotNull KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            eval();
        }
    }

    @FXML
    private void sendOnClick() {
        eval();
    }

    private void eval() {
        String username = usernameTextField.getText();
        String password = passwordField.getText();
        launchMenuOnValidation(username, password);
    }

    private void launchMenuOnValidation(String username, String password) {
        if (loginService.validate(username, password)) {
            Stage s = (Stage) usernameTextField.getScene().getWindow();
            logger.debug(s, LoggerableColor.ANSI_CYAN);
            MenuIntegratedView menuIntegratedView = new MenuIntegratedView();
            MenuIntegratedPresenter presenter = (MenuIntegratedPresenter) menuIntegratedView.getPresenter();
            logger.debug(s, LoggerableColor.ANSI_CYAN);
            presenter.setStage(s);
            Scene menu = new Scene(menuIntegratedView.getView());
            s.setResizable(true);
            s.setScene(menu);
            s.setMaximized(true);
        } else {
            logger.info("Wrong password");
            NotificationCreator.createNotification("Credentials incorrect", "Password or Username are wrong", null);
        }
    }


}
