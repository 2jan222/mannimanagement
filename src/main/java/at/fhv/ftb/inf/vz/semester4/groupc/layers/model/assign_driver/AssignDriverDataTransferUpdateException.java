package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

@SuppressWarnings("WeakerAccess")
public class AssignDriverDataTransferUpdateException extends Exception {
    public AssignDriverDataTransferUpdateException() {
        super();
    }

    public AssignDriverDataTransferUpdateException(String message) {
        super(message);
    }

    public AssignDriverDataTransferUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public AssignDriverDataTransferUpdateException(Throwable cause) {
        super(cause);
    }

    protected AssignDriverDataTransferUpdateException(String message, Throwable cause, boolean enableSuppression,
                                                      boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
