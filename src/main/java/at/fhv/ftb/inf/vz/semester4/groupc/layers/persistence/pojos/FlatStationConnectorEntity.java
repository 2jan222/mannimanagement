package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;

@Entity
@Table(name = "station_connection")
public class FlatStationConnectorEntity implements DatabaseEntityMarker {

    private int stationConnectorId;
    private int startStationId;
    private int endStationId;
    private Integer duration;
    private Integer distance;
    private FlatStationEntity startStation;
    private FlatStationEntity endStation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "station_connection_id")
    public int getStationConnectorId() {
        return stationConnectorId;
    }

    public void setStationConnectorId(int stationConnectorId) {
        this.stationConnectorId = stationConnectorId;
    }


    @Column(name = "station_start")
    public Integer getStartStationId() {
        return startStationId;
    }

    public void setStartStationId(Integer startStationId) {
        this.startStationId = startStationId;
    }

    @Column(name = "station_end")
    public Integer getEndStationId() {
        return endStationId;
    }

    public void setEndStationId(Integer endStationId) {
        this.endStationId = endStationId;
    }

    @Column(name = "duration")
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Column(name = "distance")
    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
    @ManyToOne
    @JoinColumn(name = "station_start", referencedColumnName = "station_id", nullable = false, insertable = false, updatable = false)
    public FlatStationEntity getStartStation() {
        return startStation;
    }

    public void setStartStation(FlatStationEntity startStation) {
        this.startStation = startStation;
    }

    @ManyToOne
    @JoinColumn(name = "station_end", referencedColumnName = "station_id", nullable = false, insertable = false, updatable = false)
    public FlatStationEntity getEndStation() {
        return endStation;
    }


    public void setEndStation(FlatStationEntity endStation) {
        this.endStation = endStation;
    }

    @Override
    public String toString() {
        return "FlatStationConnectorEntity{" +
                "stationConnectorId=" + stationConnectorId +
                ", startStationId=" + startStationId +
                ", endStationId=" + endStationId +
                ", duration=" + duration +
                ", distance=" + distance +
                ", startStation=" + startStation +
                ", endStation=" + endStation +
                '}';
    }


}
