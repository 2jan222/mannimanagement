package at.fhv.ftb.inf.vz.semester4.groupd.domain.security;

import at.fhv.ftb.inf.vz.semester4.groupd.persistance.entities.SuspendedEntity;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.Operation;

import java.time.LocalDate;
import java.util.Set;

public interface IBus {
	public int getBusId();
	
    public int getMaintenanceKm();

    public String getLicenceNumber();

    public String getMake();

    public String getModel();
    
    public String getNote();

    public LocalDate getRegistrationDate();

    public int getSeatPlaces();
    
    public int getStandPlaces();

    public Set<SuspendedEntity> getSuspendeds();
    
    public Set<Operation> getOperations();
}
