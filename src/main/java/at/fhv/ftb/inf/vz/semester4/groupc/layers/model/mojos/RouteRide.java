package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.TestOnly;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

import static java.time.temporal.ChronoUnit.SECONDS;

public class RouteRide {
    private Integer routeRideId;
    private Integer busId;
    private Integer operationId;
    private Integer routeId;
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer requiredCapacity;
    private Integer pathId;
    private Integer startTimeId;
    private LinkedList<PathStation> pathStations;
    private String variation;
    private Bus bus;
    private Integer routeNumber;
    private Integer operationShiftId;

    @Contract(pure = true)
    public RouteRide() {
    }

    @TestOnly
    public RouteRide(Integer routeRideId, LocalTime startTime, LocalTime endTime, LinkedList<PathStation> pathStations) {
        this.routeRideId = routeRideId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.pathStations = pathStations;
    }




    public Integer getRouteRideId() {
        return routeRideId;
    }

    public void setRouteRideId(Integer routeRideId) {
        this.routeRideId = routeRideId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public void setRequiredCapacity(Integer requiredCapacity) {
        this.requiredCapacity = requiredCapacity;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(Integer startTimeId) {
        this.startTimeId = startTimeId;
    }

    public Integer getRequiredCapacity() {
        return requiredCapacity;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public LinkedList<PathStation> getPathStations() {
        return pathStations;
    }

    public void setPathStations(LinkedList<PathStation> pathStations) {
        this.pathStations = pathStations;
    }

    public Integer getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(Integer routeNumber) {
        this.routeNumber = routeNumber;
    }

    @Override
    public String toString() {
        return "RouteRide{" +
                "routeRideId=" + routeRideId +
                ", endTime=" + endTime +
                ", busId=" + busId +
                ", operationId=" + operationId +
                ", routeId=" + routeId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", requiredCapacity=" + requiredCapacity +
                ", pathId=" + pathId +
                ", startTimeId=" + startTimeId +
                ", pathStations=" + pathStations +
                ", variation='" + variation + '\'' +
                ", bus=" + bus +
                ", routeNumber=" + routeNumber +
                '}';
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public long getRideDuration() {
        return SECONDS.between(startTime, endTime);
    }

    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }
}
