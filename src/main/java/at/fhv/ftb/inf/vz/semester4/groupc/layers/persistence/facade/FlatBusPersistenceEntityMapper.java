package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatBusPersistenceEntityMapper<FlatBusEntity> extends PersistenceEntityMapper<FlatBusEntity>{
    @Override
    public FlatBusEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (FlatBusEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatBusEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("FlatBusEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatBusEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatBusEntity ");
            return (List<FlatBusEntity>) query.getResultList();
        }
    }

    @Override
    public List<FlatBusEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatBusEntity " + whereQueryFragment);
            return (List<FlatBusEntity>) query.getResultList();
        }
    }
}
