package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationConnectorDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * StationConnectorDTO mapper.
 */
@Mapper(uses = StationDTOMapper.class)
public interface StationConnectorDTOMapper {

    StationConnectorDTOMapper INSTANCE = Mappers.getMapper(StationConnectorDTOMapper.class);

    StationConnectorDTO toDTO(StationConnector stationConnector);

    StationConnector toMojo(StationConnectorDTO stationConnectorDTO);

    void updateDTO(StationConnector stationConnector, @MappingTarget StationConnectorDTO stationConnectorDTO);

    void updateMojo(StationConnectorDTO stationConnectorDTO, @MappingTarget StationConnector stationConnector);

    List<StationConnectorDTO> toDTOs(List<StationConnector> stationConnectors);

    List<StationConnector> toMojos(List<StationConnectorDTO> stationConnectorDTOS);
}
