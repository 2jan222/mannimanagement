package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.PathDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * PathDTO mapper.
 */
@Mapper(uses = PathStationDTOMapper.class)
public interface PathDTOMapper {
    PathDTOMapper INSTANCE = Mappers.getMapper(PathDTOMapper.class);


    PathDTO toDTO(Path path);

    Path toMojo(PathDTO pathDTO);

    void updateDTO(Path path, @MappingTarget PathDTO pathDTO);

    void updateMojo(PathDTO pathDTO, @MappingTarget Path path);

    List<PathDTO> toDTOs(List<Path> paths);

    List<Path> toMojos(List<PathDTO> pathDTOS);
}
