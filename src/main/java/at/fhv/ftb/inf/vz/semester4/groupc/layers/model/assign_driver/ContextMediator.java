package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.util.Mediator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;

import java.time.LocalDate;
import java.util.Observable;
import java.util.Observer;

@LoggerableClassDisplayName("[ContextMediator]")
public class ContextMediator extends Mediator<LocalDate, DataContext> implements Observer {
    private static Loggerable logger = Loggerable.getInstance();

    @Override
    public void setValue(LocalDate storageName, DataContext value) {
        super.setValue(storageName, value);
        logger.debug("Set data context for date: " + storageName);
        value.addObserver(this);
    }

    @Override
    public DataContext getValue(LocalDate storageName) {
        DataContext value = super.getValue(storageName);
        logger.debug("GET data context for date " + storageName);
        return value;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof DataContext) {
            super.notifyObservers(((DataContext) o).getDate());
        }
    }
}
