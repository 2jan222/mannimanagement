package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.stationpath.shapes;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;


/**
 * Arrow shape.
 */
public class Arrow extends Path {
    private static final double defaultArrowHeadSize = 10.0;
    private Circle start;
    private Circle end;

    public Arrow(Circle start, Circle end) {
        this.start = start;
        this.end = end;
    }

    public void redraw() {
        double arrowHeadSize = defaultArrowHeadSize;
        strokeProperty().bind(fillProperty());
        setFill(Color.BLACK);


        Vector2d resultStart = closer(start, end, true);
        Vector2d resultEnd = closer(start, end, false);

        //Line
        double startX = resultEnd.getX();
        double startY = resultEnd.getY();
        getElements().add(new MoveTo(startX, startY));
        double endX = resultStart.getX();
        double endY = resultStart.getY();
        getElements().add(new LineTo(endX, endY));

        //ArrowHead
        double angle = Math.atan2((endY - startY), (endX - startX)) - Math.PI / 2.0;
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        //point1
        double x1 = (-1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * arrowHeadSize + endX;
        double y1 = (-1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * arrowHeadSize + endY;
        //point2
        double x2 = (1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * arrowHeadSize + endX;
        double y2 = (1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * arrowHeadSize + endY;

        getElements().add(new LineTo(x1, y1));
        getElements().add(new LineTo(x2, y2));
        getElements().add(new LineTo(endX, endY));
    }

    private static Vector2d closer(Circle start, Circle end, boolean forStart) {
        if (!forStart) {
            Circle temp = start;
            start = end;
            end = temp;
        }
        Vector2d[] vectorsStart =
                circleLineIntersection(
                        new Vector2d(start.getCenterX(), start.getCenterY()),
                        new Vector2d(end.getCenterX(), end.getCenterY()),
                        new Vector2d(end.getCenterX(), end.getCenterY()),
                        end.getRadius());
        Vector2d distStartVec1 = (Vector2d) vectorsStart[0].clone();
        Point2d startAsPoint2D = new Point2d(start.getCenterX(), start.getCenterY());
        distStartVec1.sub(startAsPoint2D);
        Vector2d distStartVec2 = (Vector2d) vectorsStart[1].clone();
        distStartVec2.sub(startAsPoint2D);
        return (distStartVec1.length() <= distStartVec2.length()) ? vectorsStart[0] : vectorsStart[1];
    }

    @SuppressWarnings("Duplicates")
    private static Vector2d[] circleLineIntersection(Vector2d a, Vector2d b, Vector2d o, double radius) {
        Vector2d p1 = new Vector2d(a);
        Vector2d p2 = new Vector2d(b);
        p1.sub(o);
        p2.sub(o);

        Vector2d d = new Vector2d();
        d.sub(p2, p1);

        double det = p1.x * p2.y - p2.x * p1.y;

        double dSq = d.lengthSquared();

        double discrimant = radius * radius * dSq - det * det;

        if (discrimant < 0) {
            return new Vector2d[0];
        }

        if (discrimant == 0) {
            Vector2d[] t = new Vector2d[1];
            t[0] = new Vector2d(det * d.y / dSq + o.x, -det * d.x / dSq + o.y);

            return t;
        }

        double discSqrt = Math.sqrt(discrimant);

        double sgn = 1;
        if (d.y < 0) {
            sgn = -1;
        }

        Vector2d[] t = new Vector2d[2];
        t[0] = new Vector2d(
                (det * d.y + sgn * d.x * discSqrt) / dSq + o.x,
                (-det * d.x + Math.abs(d.y) * discSqrt) / dSq + o.y
        );
        t[1] = new Vector2d(
                (det * d.y - sgn * d.x * discSqrt) / dSq + o.x,
                (-det * d.x - Math.abs(d.y) * discSqrt) / dSq + o.y
        );
        return t;
    }
}
