package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "bus", schema = "public", catalog = "kmobil")
public class FlatBusEntity implements DatabaseEntityMarker {
    private int busId;
    private int maintenanceKm;
    private String licenceNumber;
    private String make;
    private String model;
    private String note;
    private LocalDate registrationDate;
    private int seatPlaces;
    private int standPlaces;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bus_id")
    public int getBusId() {
        return busId;
    }

    public void setBusId(int busId) {
        this.busId = busId;
    }

    @Basic
    @Column(name = "maintenance_km")
    public int getMaintenanceKm() {
        return maintenanceKm;
    }

    public void setMaintenanceKm(int maintenanceKm) {
        this.maintenanceKm = maintenanceKm;
    }

    @Basic
    @Column(name = "licence_number")
    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    @Basic
    @Column(name = "make")
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Basic
    @Column(name = "model")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Basic
    @Column(name = "registration_date")
    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Basic
    @Column(name = "seat_places")
    public int getSeatPlaces() {
        return seatPlaces;
    }

    public void setSeatPlaces(int seatPlaces) {
        this.seatPlaces = seatPlaces;
    }

    @Basic
    @Column(name = "stand_places")
    public int getStandPlaces() {
        return standPlaces;
    }

    public void setStandPlaces(int standPlaces) {
        this.standPlaces = standPlaces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlatBusEntity flatBusEntity = (FlatBusEntity) o;
        return busId == flatBusEntity.busId &&
                maintenanceKm == flatBusEntity.maintenanceKm &&
                seatPlaces == flatBusEntity.seatPlaces &&
                standPlaces == flatBusEntity.standPlaces &&
                Objects.equals(licenceNumber, flatBusEntity.licenceNumber) &&
                Objects.equals(make, flatBusEntity.make) &&
                Objects.equals(model,flatBusEntity.model) &&
                Objects.equals(note, flatBusEntity.note) &&
                Objects.equals(registrationDate, flatBusEntity.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(busId, maintenanceKm, licenceNumber, make, model, note, registrationDate, seatPlaces, standPlaces);
    }

    @Override
    public String toString() {
        return "FlatBusEntity{" +
                "busId=" + busId +
                ", maintenanceKm=" + maintenanceKm +
                ", licenceNumber='" + licenceNumber + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", note='" + note + '\'' +
                ", registrationDate=" + registrationDate +
                ", seatPlaces=" + seatPlaces +
                ", standPlaces=" + standPlaces +
                '}';
    }
}
