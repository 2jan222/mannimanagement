package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatOperationPersistenceEntityMapper<FlatOperationEntity> extends PersistenceEntityMapper<FlatOperationEntity> {


    @Override
    public FlatOperationEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (FlatOperationEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatOperationEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("FlatOperationEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatOperationEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatOperationEntity ");
            return (List<FlatOperationEntity>) query.getResultList();
        }
    }

    @Override
    public List<FlatOperationEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatOperationEntity " + whereQueryFragment);
            return (List<FlatOperationEntity>) query.getResultList();
        }
    }
}
