package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class OperationShiftPersistenceEntityMapper<OperationShiftEntity> extends PersistenceEntityMapper<OperationShiftEntity>{
    @Override
    public OperationShiftEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (OperationShiftEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationShiftEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("OperationShiftEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<OperationShiftEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from OperationShiftEntity ");
            return (List<OperationShiftEntity>) query.getResultList();
        }
    }

    @Override
    public List<OperationShiftEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from OperationShiftEntity " + whereQueryFragment);
            return (List<OperationShiftEntity>) query.getResultList();
        }
    }
}
