package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.demo.bluebg;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;

/**
 * Demo Presenter.
 */
public class BlueBGPresenter implements MenuViewItem {
    @Override
    public String getMenuDisplayName() {
        return "BlueBG";
    }
}
