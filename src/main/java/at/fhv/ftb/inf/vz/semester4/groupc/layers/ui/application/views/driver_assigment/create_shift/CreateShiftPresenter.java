package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.driver_assigment.create_shift;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver.AssignDriverContextCreationException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver.AssignDriverDataTransferUpdateException;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.driverassignment.DriverAssignmentService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.NotificationCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import com.jfoenix.controls.JFXTimePicker;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javax.inject.Inject;

@LoggerableClassDisplayName("[CreateShiftPresenter]")
public class CreateShiftPresenter implements Initializable {

    @Inject
    DriverAssignmentService service;
    @FXML
    private JFXTimePicker timeFrom;
    @FXML
    private JFXTimePicker timeUntil;
    @FXML
    private TableView<ShiftRide> table;
    @FXML
    private TableColumn<ShiftRide, Integer> numberCol;
    @FXML
    private TableColumn<ShiftRide, String> startTimeCol;
    @FXML
    private TableColumn<ShiftRide, String> endTimeCol;
    @FXML
    private TableColumn<ShiftRide, String> startStationCol;
    @FXML
    private TableColumn<ShiftRide, String> endStationCol;

    private static final Loggerable logger = Loggerable.getInstance();
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("hh:mm a");
    private Integer operationId;
    private ObservableList<ShiftRide> rides;
    private LocalDate date;
    private Runnable runOnClose;

    public void display(Runnable runOnClose, Integer id, @NotNull LinkedList<ShiftRide> shiftRides, LocalDate date) {
        this.operationId = id;
        this.rides = FXCollections.observableArrayList(shiftRides);
        this.date = date;
        this.runOnClose = runOnClose;
        shiftRides.forEach(System.out::println);
        this.rides.addListener((ListChangeListener<ShiftRide>) c -> {
            if (rides.size() < 1) {
                closeWindow();
            }
        });
        ((Stage) timeUntil.getScene().getWindow()).setOnCloseRequest(e -> {
            System.out.println("RUN CLOSE REQUEST");
            runOnClose.run();
        });
        table.setItems(this.rides);
        table.sort();
    }

    @FXML
    private void createShiftAction(ActionEvent event) {
        logger.debug("Create Shift Between Specified Timestamps");
        final String title = "Info";
        LocalTime fromValue = timeFrom.getValue().minus(1, ChronoUnit.MINUTES);
        LocalTime untilValue = timeUntil.getValue().plus(1, ChronoUnit.MINUTES);
        if (fromValue != null && untilValue != null) {
            logger.debug("Timerange[" + fromValue + "," + untilValue + "]"); //TODO Not ideal, maybe fix
            try {
                ShiftEntry b = service.setShift(operationId, fromValue, untilValue, date);
                if (b != null) {
                    logger.debug("Set Shift Successful");
                    runOnClose.run();
                    removeRidesWithinTime(b);
                    NotificationCreator.createNotification(title, "Added new Shift Entry", null);
                } else {
                    NotificationCreator.createWarningNotification("Warning", "Specified timeframe could not be translated into shift");
                }
            } catch (AssignDriverContextCreationException | AssignDriverDataTransferUpdateException e) {
                e.printStackTrace();
            }
        } else {
            final String msgStart = "Please select a ";
            if (fromValue == null) {
                NotificationCreator.createNotification(title, msgStart + "start time", null);
            }
            if (untilValue == null) {
                NotificationCreator.createNotification(title, msgStart + "end time", null);
            }
        }
    }

    private void removeRidesWithinTime(@NotNull ShiftEntry entry) {
        entry.getShiftRides().forEach(e -> rides.remove(e));
    }

    @FXML
    private void closeWindow() {
        System.out.println("CLOSE WINDOW");
        runOnClose.run();
        ((Stage) timeUntil.getScene().getWindow()).close();
    }

    private void initTable() {
        long start = System.currentTimeMillis();
        logger.debug("Init table");
        startTimeCol.setCellValueFactory(data -> toProp(data.getValue().getStartTime().format(FORMATTER)));
        endTimeCol.setCellValueFactory(data -> toProp(data.getValue().getEndTime().format(FORMATTER)));
        numberCol.setCellValueFactory(data -> new ReadOnlyObjectWrapper<>(table.getItems().indexOf(data.getValue())));
        startStationCol.setCellValueFactory(data -> toProp(data.getValue().getStart().getStationName()));
        endStationCol.setCellValueFactory(data -> toProp(data.getValue().getEnd().getStationName()));

        numberCol.setSortable(false);


        table.setSortPolicy(table -> {
            Comparator<ShiftRide> comparator = Comparator.comparing(ShiftRide::getStartTime); //IF NOT Functional revert to GroupPlanePresenter impl
            FXCollections.sort(table.getItems(), comparator);
            return true;
        });

        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.getSelectionModel().getSelectedItems().addListener((ListChangeListener<ShiftRide>) c -> {
            ObservableList<ShiftRide> selectedItems = table.getSelectionModel().getSelectedItems();
            if (selectedItems.size() > 0) {
                ShiftRide startRide = selectedItems.get(0);
                timeFrom.setValue(startRide.getStartTime());
                ShiftRide endRide = selectedItems.get(selectedItems.size() - 1);
                timeUntil.setValue(endRide.getEndTime());
            }
        });

        logger.debug("inited table in " + (System.currentTimeMillis() - start) + "ms");
    }

    @NotNull
    @Contract(value = "_ -> new", pure = true)
    private static SimpleStringProperty toProp(@NotNull String s) {
        return new SimpleStringProperty(s);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("Initialize");
        initTable();
    }
}





