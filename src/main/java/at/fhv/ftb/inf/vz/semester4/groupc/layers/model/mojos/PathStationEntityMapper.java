package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {StationConnectorEntityMapper.class})
public interface PathStationEntityMapper {

    PathStationEntityMapper INSTANCE = Mappers.getMapper(PathStationEntityMapper.class);


    @Mapping(target = "pathStationId", source = "pathStationId")
    @Mapping(target = "positionOnPath", source = "positionOnPath")
    @Mapping(target = "pathId", source = "pathId")
    @Mapping(target = "connectionId", source = "stationConnectorId")
    @Mapping(target = "connection", source = "stationConnector")
    PathStation toMojo(PathStationEntity pathStationEntity);


    @Mapping(target = "pathId", source = "pathId")
    @Mapping(target = "pathStationId", source = "pathStationId")
    @Mapping(target = "positionOnPath", source = "positionOnPath")
    @Mapping(target = "stationConnectorId", source = "connectionId")
    @Mapping(target = "stationConnector", source = "connection")
    @Mapping(target = "pathByPathId", ignore = true)
    PathStationEntity toPojo(PathStation pathStation);


    @Mapping(target = "pathStationId", source = "pathStationId")
    @Mapping(target = "positionOnPath", source = "positionOnPath")
    @Mapping(target = "pathId", source = "pathId")
    @Mapping(target = "connectionId", source = "stationConnectorId")
    @Mapping(target = "connection", source = "stationConnector")
    void updateMojo(PathStationEntity pathStationEntity, @MappingTarget PathStation pathStation);


    @Mapping(target = "pathId", source = "pathId")
    @Mapping(target = "pathStationId", source = "pathStationId")
    @Mapping(target = "positionOnPath", source = "positionOnPath")
    @Mapping(target = "stationConnectorId", source = "connectionId")
    @Mapping(target = "stationConnector", source = "connection")
    @Mapping(target = "pathByPathId", ignore = true)
    void updatePojo(PathStation pathStation, @MappingTarget PathStationEntity pathStationEntity);

    List<PathStation> toMojos(List<PathStationEntity> pathStationEntities);

    List<PathStationEntity> toPojos(List<PathStation> pathStations);
}
