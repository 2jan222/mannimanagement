package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class SuspendedPersistenceEntityMapper<SuspendedEntity> extends PersistenceEntityMapper<SuspendedEntity> {
    @Override
    public SuspendedEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (SuspendedEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.SuspendedEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("SuspendedEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<SuspendedEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from SuspendedEntity ");
            return (List<SuspendedEntity>) query.getResultList();
        }
    }

    @Override
    public List<SuspendedEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from SuspendedEntity " + whereQueryFragment);
            return (List<SuspendedEntity>) query.getResultList();
        }
    }


}
