package at.fhv.ftb.inf.vz.semester4.groupd.domain;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IOperation;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IRoute;
import at.fhv.ftb.inf.vz.semester4.groupd.domain.security.IRouteRide;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Route implements IRoute {
	private RouteEntity _routeEntity;
	private int _openRides;

	public Route(RouteEntity routeEntity) {
		_routeEntity = routeEntity;
	}

	@Override
	public int getRouteId() {
		return _routeEntity.getRouteId();
	}

	@Override
	public int getRouteNumber() {
		return _routeEntity.getRouteNumber();
	}

	@Override
	public LocalDate getValidFrom() {
		return _routeEntity.getValidFrom();
	}

	@Override
	public LocalDate getValidTo() {
		return _routeEntity.getValidTo();
	}

	@Override
	public String getVariation() {
		return _routeEntity.getVariation();
	}

	@Override
	public List<IRouteRide> getRouteRides() {
		return _routeEntity.getRouteRidesByRouteId().stream().map(RouteRide::new).collect(Collectors.toList());
	}

    @Override
    public List<Path> getPaths() {
        return _routeEntity.getPathsByRouteId().stream().map(Path::new).collect(Collectors.toList());
    }

	@Override
	public List<? extends IRouteRide> getOpenRouteRides(List<? extends IOperation> operations, DayType daytype) {
		List<IRouteRide> rides = new LinkedList<>(getRouteRides());
		System.out.println("RIDES " + rides.size());
		rides.forEach(e -> System.out.println(e));
        for (IRouteRide ride : getRouteRides()) {
			if (ride.getStartTime().getDaytype() == daytype) {
				for (IOperation operation : operations) {
					if (operation.getRouteRides().contains(ride)) {
                        rides.remove(ride);
					}
				}
			}
			else {
                rides.remove(ride);
			}
		}
        _openRides = rides.size();
        return rides;
	}

	@Override
	public int getNumberOfRidesPerDayType(DayType dayType) {
		int rideCount = 0;
		for (IRouteRide ride : this.getRouteRides()) {
			if (ride.getStartTime().getDaytype() == dayType) {
				rideCount++;
			}
		}
		return rideCount;
	}

	@Override
	public String toString() {
		return "Route{" +
				"_routeEntity=" + _routeEntity +
				", _openRides=" + _openRides +
				'}';
	}
}
