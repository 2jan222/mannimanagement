package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class PathStation implements Comparable {
    private Integer pathId;
    private Integer positionOnPath;
    private StationConnector connection;
    private Integer connectionId;
    private Integer pathStationId;

    @Contract(pure = true)
    public PathStation(Integer pathId, Integer positionOnPath, @NotNull StationConnector connection) {
        this.pathId = pathId;
        this.positionOnPath = positionOnPath;
        this.connection = connection;
        this.connectionId = connection.getConnectionId();
    }

    @Contract(pure = true)
    public PathStation() {

    }

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getPositionOnPath() {
        return positionOnPath;
    }

    public void setPositionOnPath(Integer positionOnPath) {
        this.positionOnPath = positionOnPath;
    }

    public StationConnector getConnection() {
        return connection;
    }

    public void setConnection(StationConnector connection) {
        this.connection = connection;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public Integer getPathStationId() {
        return pathStationId;
    }

    public void setPathStationId(Integer pathStationId) {
        this.pathStationId = pathStationId;
    }

    @Override
    public String toString() {
        return "PathStation{" +
                "pathId=" + pathId +
                ", positionOnPath=" + positionOnPath +
                ", connection=" + connection +
                ", connectionId=" + connectionId +
                ", pathStationId=" + pathStationId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PathStation)) return false;
        PathStation that = (PathStation) o;
        return pathId.equals(that.pathId) &&
                positionOnPath.equals(that.positionOnPath) &&
                Objects.equals(connection, that.connection) &&
                connectionId.equals(that.connectionId) &&
                pathStationId.equals(that.pathStationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pathId, positionOnPath, connection, connectionId, pathStationId);
    }

    @Override
    public int compareTo(@NotNull Object o) {
        return ((PathStation) o).getPositionOnPath().compareTo(positionOnPath) * -1;
    }
}
