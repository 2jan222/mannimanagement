package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.DatabaseEntityMarker;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "operation", schema = "public", catalog = "kmobil")
public class FlatOperationEntity implements DatabaseEntityMarker {

    private Integer operationId;
    private LocalDate date;
    private Integer busId;
    private FlatBusEntity flatBusEntity;
    private Set<RouteRideEntityFast> shiftRides;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "date")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @ManyToOne
    @JoinColumn(name = "bus_id", referencedColumnName = "bus_id", insertable = false, updatable = false)
    public FlatBusEntity getFlatBusEntity() {
        return flatBusEntity;
    }

    public void setFlatBusEntity(FlatBusEntity flatBusEntity) {
        this.flatBusEntity = flatBusEntity;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "operation_ride", joinColumns = { @JoinColumn(name = "operation_id") }, inverseJoinColumns = {
            @JoinColumn(name = "route_ride_id") })
    //@OneToMany(targetEntity = RouteRideEntityFast.class, fetch = FetchType.EAGER)
    //@JoinColumn(name = "operation_id", referencedColumnName = "operation_id")
    public Set<RouteRideEntityFast> getShiftRides() {
        return shiftRides;
    }

    public void setShiftRides(Set<RouteRideEntityFast> shiftRides) {
        this.shiftRides = shiftRides;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlatOperationEntity)) return false;
        FlatOperationEntity that = (FlatOperationEntity) o;
        return Objects.equals(operationId, that.operationId) &&
                Objects.equals(date, that.date) &&
                Objects.equals(busId, that.busId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operationId, date, busId, flatBusEntity, shiftRides);
    }

    @Override
    public String toString() {
        return "FlatOperationEntity{" +
                "operationId=" + operationId +
                ", date=" + date +
                ", busId=" + busId +
                ", flatBusEntity=" + flatBusEntity +
                ", shiftRides=" + shiftRides +
                '}';
    }
}
