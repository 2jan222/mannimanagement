package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util;

import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

/**
 * Arg Parser.
 *
 * @author Janik Mayr
 */
public class ArgsParser {
    private static HashSet<String> parsedArgs = new HashSet<>();

    /**
     * Executes code when arg is known to parser.
     *
     * @param argKeyword keyword to search in args for.
     * @param consumer for keyword.
     */
    public static void checkForKeyword(String argKeyword, Consumer<String> consumer) {
        if (parsedArgs.contains(argKeyword)) {
            consumer.accept(argKeyword);
        }
    }

    /**
     * Pareses all args and stores them.
     *
     * @param args to parse.
     */
    public static void parseArgs(List<String> args) {
        parsedArgs.addAll(args);
    }

}

