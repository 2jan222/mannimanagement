package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import org.jetbrains.annotations.Contract;

import java.util.Objects;

@SuppressWarnings("WeakerAccess")
public class StationIDPair {
    private int x;
    private int y;

    @Contract(pure = true)
    public StationIDPair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Contract(value = "null -> false", pure = true)
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StationIDPair)) {
            return false;
        }
        StationIDPair stationIDPair = (StationIDPair) o;
        return x == stationIDPair.x && y == stationIDPair.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
