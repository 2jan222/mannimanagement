package at.fhv.ftb.inf.vz.semester4.groupc.layers.service.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Route;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.RouteEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationEntityMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.PathCreator;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade.PersistenceFacade;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.StationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.RouteDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.StationDTOMapper;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * PathService.
 */
@SuppressWarnings("unchecked")
public class PathService {

    private Loggerable logger = Loggerable.getInstance();

    static {
        PathCreator.initStationPlanFromDB();
    }

    public LinkedList<StationDTO> fetchStations() {
        List list = PersistenceFacade.getInstance().getAll(StationEntity.class);
        return new LinkedList<StationDTO>(StationDTOMapper.INSTANCE.toDTOs(StationEntityMapper.INSTANCE.toMojos(list)));
    }

    @SuppressWarnings("unchecked")
    public LinkedList<RouteDTO> getRoutes() {
        return new LinkedList<RouteDTO>(RouteDTOMapper.INSTANCE.toDTOs(RouteEntityMapper.INSTANCE.toMojos(
                PersistenceFacade.getInstance().getAll(RouteEntity.class))));
        //DUMMY DATA
        //return new LinkedList<>(Arrays.asList(new RouteDTO(18, Date.valueOf(LocalDate.now()),
        // Date.valueOf(LocalDate.now().plusDays(90)), "V1"), new RouteDTO(9, "V99")));
    }

    public PathCreator getDummyPathCreator() {
        Station s0 = new Station(0, "start dummy name", "sdn");
        Station s1 = new Station(1, "end dummy name", "edn");
        PathCreator pathCreator = new PathCreator(s0, s1);
        pathCreator.addStation(null, new Station(3, "inter0", "i0"));
        return pathCreator;
    }

    public PathCreator getTOPathCreator(@NotNull RouteDTO currentRoute) {
        logger.debug("Route Ride DTO: " + currentRoute.toString(), LoggerableColor.ANSI_BLUE);
        Route route = RouteDTOMapper.INSTANCE.toMojo(currentRoute);
        LinkedList<Path> paths = new LinkedList<>(route.getPaths());
        return PathCreator.of(paths.getFirst());
    }

    public PathCreator getBackPathCreator(@NotNull RouteDTO currentRoute) {
        logger.debug("Route Ride DTO: " + currentRoute.toString(), LoggerableColor.ANSI_BLUE);
        Route route = RouteDTOMapper.INSTANCE.toMojo(currentRoute);
        LinkedList<Path> paths = new LinkedList<>(route.getPaths());
        Path path;
        try {
            path = paths.get(1);
        } catch (IndexOutOfBoundsException e) {
            path = new Path();
        }
        return (path != null) ? PathCreator.of(path) : null; //TODO EMPTY PATH
    }
}
