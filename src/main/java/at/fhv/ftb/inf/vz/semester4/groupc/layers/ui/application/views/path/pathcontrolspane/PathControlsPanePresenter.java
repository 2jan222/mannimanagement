package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.Pair;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.path.PathService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.PathCreationPresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.PathCreationInfo;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.PathsOfRoutePanePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.PathsOfRoutePaneView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.route.RouteCreatePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.route.RouteCreateView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.CommonResourceGetter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.PopupWindowCreator;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import javax.inject.Inject;

/**
 * PathControlsPanePresenter.
 * Views the selection of the route as well as the selection of the valid date
 */
@LoggerableClassDisplayName("[PathControlsPanePresenter]")
public class PathControlsPanePresenter implements Initializable {

    @Inject
    Loggerable logger;
    @Inject
    PathService pathService;
    @FXML
    private AnchorPane pathsOfRideAnchorPane;
    @FXML
    private ChoiceBox<String> choiceBox;
    @FXML
    private DatePicker assignmentFromDateDatePicker;
    @FXML
    private DatePicker assignmentToDateDatePicker;

    private PathCreationPresenter wrapperPresenter;
    private PathsOfRoutePanePresenter childrenPresenter;

    private HashMap<RouteDTO, Pair<PathsOfRoutePanePresenter, Node>> loadedFXMLParts = new HashMap<>();
    private HashMap<String, RouteDTO> lookup = new HashMap<>();
    private RouteDTO currentRoute;

    private PathCreationInfo toPath;
    private PathCreationInfo backPath;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        choiceBox.setOnAction(e -> changeSelectedRoute());
    }

    public void setStage(Stage stage) {
        if (stage != null) {
            logger.debug("Stage set");
            stage.setOnCloseRequest(event -> tabExitCall());
        } else {
            logger.warn("Stage should have been set");
        }
    }

    private void createAlert(boolean isToPath, Runnable toRunOnOk) {
        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeNo = new ButtonType("No");
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Save Journey ?");
        alert.setHeaderText("Save Journey " + ((isToPath) ? "to" : "back") + " ?");
        alert.setContentText("Unsaved changes will be lost!");
        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
        DialogPane dialogPane = alert.getDialogPane();
        Button yesButton = (Button) dialogPane.lookupButton(buttonTypeYes);
        yesButton.setId("closeYesButton");
        Button noButton = (Button) dialogPane.lookupButton(buttonTypeNo);
        noButton.setId("closeNoButton");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == buttonTypeYes) {
            toRunOnOk.run();
        }
    }

    public void refreshUp() {
        logger.debug("Refresh chain up");
        wrapperPresenter.refreshFromBelow();
    }

    public void refresh() {
        logger.debug("Refresh");
        updateRoutePickerItems();
        updateDatePicker(currentRoute);
        refreshDown();
    }

    private void refreshDown() {
        logger.debug("Refresh Children");
        if (childrenPresenter != null) {
            childrenPresenter.refresh();
        } else {
            logger.debug("Has no children to refresh");
        }
        logger.debug("Refreshed Children");
    }

    public void refreshFromBelow() {
        refreshUp();
    }

    /**
     * Updates Items of route picker.
     */
    private void updateRoutePickerItems() {
        logger.debug("Fetch and Set Route Pickers Items");
        LinkedList<RouteDTO> routes = pathService.getRoutes();
        routes.forEach(e -> {
            final String displayName = "Route " + e.getRouteNumber();
            if (!lookup.containsKey(displayName)) {
                choiceBox.getItems().add(displayName);
                lookup.put(displayName, e);
            }
        });
        logger.debug("Fetched and Set Route Pickers Items[" + routes.size() +"]");
    }

    private void changeSelectedRoute() {
        String newVal = choiceBox.getValue();
        RouteDTO routeDTO = lookup.get(newVal);
        currentRoute = routeDTO;
        exchangePathsOfRouteView(routeDTO);
        toPath = new PathCreationInfo(pathService.getTOPathCreator(routeDTO));
        backPath = new PathCreationInfo(pathService.getBackPathCreator(routeDTO));
        refresh();
    }

    private void exchangePathsOfRouteView(RouteDTO routeDTO) {
        currentRoute = routeDTO;
        if (!loadedFXMLParts.containsKey(routeDTO)) {
            PathsOfRoutePaneView pathsOfRoutePaneView = new PathsOfRoutePaneView();
            childrenPresenter = (PathsOfRoutePanePresenter) pathsOfRoutePaneView.getPresenter();
            childrenPresenter.setWrapperPresenter(this);
            loadedFXMLParts.put(routeDTO, new Pair<>(childrenPresenter, pathsOfRoutePaneView.getView()));
        } else {
            childrenPresenter = loadedFXMLParts.get(routeDTO).getKey();
        }
        pathsOfRideAnchorPane.getChildren().clear();
        pathsOfRideAnchorPane.getChildren().add(loadedFXMLParts.get(routeDTO).getValue());
    }

    private void updateDatePicker(RouteDTO routeDTO) {
        logger.debug("Refresh DatePickers");
        if (routeDTO != null) {
            LocalDate validFrom = routeDTO.getValidFrom();
            LocalDate validTo = routeDTO.getValidTo();
            if (validFrom != null) {
                assignmentFromDateDatePicker.setValue(validFrom);
            } else {
                assignmentFromDateDatePicker.setValue(null);
            }
            if (validTo != null) {
                assignmentToDateDatePicker.setValue(validTo);
            } else {
                assignmentToDateDatePicker.setValue(null);
            }
        }
        logger.debug("Refreshed DatePickers");
    }

    public void setWrapperPresenter(PathCreationPresenter presenter) {
        this.wrapperPresenter = presenter;
    }

    public void createRoute(ActionEvent actionEvent) {
        RouteCreateView view = new RouteCreateView();
        view.getPresenter(o -> ((RouteCreatePresenter) o).setRunnableOnSuccessfulFinish(this::refreshUp));
        Stage newRoute = PopupWindowCreator.createWindow(
                view,
                "New Route",
                CommonResourceGetter.getBusIcon(),
                CommonResourceGetter.getWindowFromActionEvent(actionEvent),
                Modality.WINDOW_MODAL,
                (Consumer<Stage>) stage -> stage.setResizable(true)
        );
        newRoute.show();
    }

    public PathCreationInfo getPathToInfo() {
        return toPath;
    }

    public PathCreationInfo getPathBackInfo() {
        return backPath;
    }

    public RouteDTO getCurrentRoute() {
        return currentRoute;
    }

    public void tabExitCall() {
        if (toPath != null && !toPath.getIsSaved()) {
            createAlert(true, () -> toPath.save(currentRoute, true));
        }
        if (backPath != null && !backPath.getIsSaved()) {
            createAlert(false, () -> backPath.save(currentRoute, true));
        }
    }
}
