package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.stationpath;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.stationpath.shapes.CirclePair;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationConnectorDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Transform;

import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javax.inject.Inject;

/**
 * StationPathPresenter.
 */
@LoggerableClassDisplayName("[StationPathPresenter]")
public class StationPathPresenter implements MenuViewItem, Initializable {
    @Inject
    private static Loggerable logger;
    private static final int ARR_SIZE = 8;
    private Canvas pathCanvas;
    @FXML
    private AnchorPane canvasRoot;
    @FXML
    private StackPane stackPane;
    private Affine urTransform;

    LinkedList<StationConnectorDTO> connections = new LinkedList<>();
    HashMap<Integer, CirclePair> circles = new HashMap<>();

    @Override
    public String getMenuDisplayName() {
        return "Paths";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pathCanvas = new Canvas();
        GraphicsContext gc = pathCanvas.getGraphicsContext2D();
        urTransform = gc.getTransform();

        pathCanvas.widthProperty().bind(canvasRoot.widthProperty());
        pathCanvas.widthProperty().addListener(observable -> redraw(gc));
        pathCanvas.heightProperty().bind(canvasRoot.heightProperty());
        pathCanvas.heightProperty().addListener(observable -> redraw(gc));



        /* DUMMY DATA */
        StationDTO dto = new StationDTO();
        for (int i = 1; i < 50; i += (7 + (2.3 % i))) {
            createConnection(dto, dto, i, i * 15, i + ((12 * i) % i) * 2 + 30,
                    (int) (i * 2.6) + i * 10, (int) ((int) (i * 9.3) + (i * 2.1) % (i / 2.3)) + 250);
        }
        for (int i = 1; i < 50; i += (7 + (2.3 % i))) {
            createConnection(dto, dto, 300 + i, i + ((12 * i) % i) * 2 + 30, i * 15,
                    (int) ((int) (i * 9.3) + (i * 2.1) % (i / 2.3)) + 250, (i * 7) + i * 10);
        }

    }

    public void createConnection(StationDTO start, StationDTO end, int id, int xStart, int yStart, int xEnd, int yEnd) {
        StationConnectorDTO stationConnectorDTO = new StationConnectorDTO(id, start, end);
        connections.add(stationConnectorDTO);
        circles.put(stationConnectorDTO.getConnectionId(),
                new CirclePair(
                        drawStationNode(stationConnectorDTO.getStart(), xStart, yStart),
                        drawStationNode(stationConnectorDTO.getEnd(), xEnd, yEnd)
                ));
    }

    public void redraw(GraphicsContext gc) {
        gc.setTransform(urTransform);
        gc.translate(0, 0);
        //pathCanvas.getGraphicsContext2D().clearRect(0, 0, pathCanvas.getWidth(), pathCanvas.getHeight());
        canvasRoot.getChildren().clear();
        //canvasRoot.getChildren().addAll(pathCanvas);
        //drawHelperLines(gc, 50);
        circles.values().forEach(e -> e.redrawInto(canvasRoot));

    }

    Circle drawStationNode(StationDTO stationDTO, int x, int y) {
        int radius = 10;
        Circle circle = new Circle(x, y, radius, Color.BLUE);
        circle.setCursor(Cursor.MOVE);
        circle.setOnMousePressed(circleOnMousePressedEventHandler);
        circle.setOnMouseDragged(circleOnMouseDraggedEventHandler);
        circle.setOnMouseReleased(e -> {
            Circle source = (Circle) e.getSource();
            source.setCenterX(source.getCenterX() + circle.getTranslateX());
            source.setCenterY(source.getCenterY() + circle.getTranslateY());
            source.setTranslateX(0);
            source.setTranslateY(0);
            logger.debug(source.getCenterX() + " : " + source.getCenterY());
        });
        final ChangeListener<Number> changeListener =
                (observable, oldValue, newValue) -> redraw(pathCanvas.getGraphicsContext2D());
        circle.centerXProperty().addListener(changeListener);
        circle.centerYProperty().addListener(changeListener);
        circle.radiusProperty().addListener(changeListener);
        return circle;
    }

    private void drawHelperLines(GraphicsContext gc, @SuppressWarnings("SameParameterValue") int distance) {
        for (int i = 0; i < pathCanvas.getWidth(); i += distance) {
            gc.strokeLine(i, 0, i, pathCanvas.getHeight());
        }
        for (int i = 0; i < pathCanvas.getHeight(); i += distance) {
            gc.strokeLine(0, i, pathCanvas.getWidth(), i);
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    //      EVENT HANDLER FOR CIRCLE MOVEMENT
    //-----------------------------------------------------------------------------------------------------------------

    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    EventHandler<MouseEvent> circleOnMousePressedEventHandler =
            new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent t) {
                    orgSceneX = t.getSceneX();
                    orgSceneY = t.getSceneY();
                    Circle source = (Circle) (t.getSource());
                    orgTranslateX = source.getTranslateX();
                    orgTranslateY = source.getTranslateY();
                }
            };

    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler =
            new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent t) {
                    double offsetX = t.getSceneX() - orgSceneX;
                    double offsetY = t.getSceneY() - orgSceneY;
                    double newTranslateX = orgTranslateX + offsetX;
                    double newTranslateY = orgTranslateY + offsetY;
                    ((Circle) (t.getSource())).setTranslateX(newTranslateX);
                    ((Circle) (t.getSource())).setTranslateY(newTranslateY);
                }
            };

    @Deprecated
    void drawCirclePair(GraphicsContext gc, CirclePair pair) {
        pair.redrawInto(canvasRoot);
        /*
        Circle start = pair.getStart();
        Circle end = pair.getEnd();
                Vector2d[] vector2ds =
                circleLineIntersection(
                        new Vector2d(start.getCenterX(), start.getCenterY()),
                        new Vector2d(end.getCenterX(), end.getCenterY()),
                        new Vector2d(end.getCenterX(), end.getCenterY()),
                        end.getRadius());
        Vector2d distVec1 = (Vector2d) vector2ds[0].clone();
        Point2d startAsPoint2D = new Point2d(start.getCenterX(), start.getCenterY());
        distVec1.sub(startAsPoint2D);
        Vector2d distVec2 = (Vector2d) vector2ds[1].clone();
        distVec2.sub(startAsPoint2D);
        Vector2d result = (distVec1.length() <= distVec2.length()) ? vector2ds[0] : vector2ds[1];
        drawArrow(gc, (int) start.getCenterX(), (int) start.getCenterY(), (int) result.x, (int) result.y);

         */
    }

    @Deprecated
    void drawArrow(GraphicsContext gc, int x1, int y1, int x2, int y2) {
        gc.setFill(Color.RED);
        double dx = x2 - x1, dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int len = (int) (Math.sqrt(dx * dx + dy * dy));
        Transform transform = Transform.translate(x1, y1);
        transform = transform.createConcatenation(Transform.rotate(Math.toDegrees(angle), 0, 0));
        gc.setTransform(new Affine(transform));
        gc.setStroke(Color.RED);
        gc.strokeLine(0, 0, len, 0);
        gc.fillPolygon(new double[]{len, len - ARR_SIZE, len - ARR_SIZE, len}, new double[]{0, -ARR_SIZE, ARR_SIZE, 0},
                4);
        gc.setStroke(Color.BLACK);
    }
}
