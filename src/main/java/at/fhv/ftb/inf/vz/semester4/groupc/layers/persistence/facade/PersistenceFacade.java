package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class PersistenceFacade {
    private static PersistenceFacade ourInstance = new PersistenceFacade();
    private HashMap<Class, DBMapper> mappers = new HashMap<>();
    //private HashMap<Class, DBMapper<? extends DatabaseEntityMarker>> mappers = new HashMap<>();
    //
    // TODO: automatic checked casting

    @Contract(pure = true)
    public static PersistenceFacade getInstance() {
        return ourInstance;
    }

    private PersistenceFacade() {

        mappers.putIfAbsent(BusEntity.class, new BusPersistenceEntityMapper<BusEntity>());
        mappers.putIfAbsent(DriverEntity.class, new DriverPersistenceEntityMapper<DriverEntity>());
        mappers.putIfAbsent(DrivingLicensesEntity.class, new DrivingLicensesPersistenceEntityMapper<DrivingLicensesEntity>());
        mappers.putIfAbsent(OperationEntity.class, new OperationPersistenceEntityMapper<OperationEntity>());
        mappers.putIfAbsent(PathEntity.class, new PathPersistenceEntityMapper<PathEntity>());
        mappers.putIfAbsent(PathStationEntity.class, new PathStationPersistenceEntityMapper<PathStationEntity>());
        mappers.putIfAbsent(RouteEntity.class, new RoutePersistenceEntityMapper<RouteEntity>());
        mappers.putIfAbsent(RouteRideEntity.class, new RouteRidePersistenceEntityMapper<RouteRideEntity>());
        mappers.putIfAbsent(ShiftEntity.class, new ShiftPersistenceEntityMapper<ShiftEntity>());
        mappers.putIfAbsent(StartTimeEntity.class, new StartTimePersistenceEntityMapper<StartTimeEntity>());
        mappers.putIfAbsent(StationEntity.class, new StationPersistenceEntityMapper<StationEntity>());
        mappers.putIfAbsent(SuspendedEntity.class, new SuspendedPersistenceEntityMapper<SuspendedEntity>());
        mappers.putIfAbsent(StationConnectorEntity.class, new StationConnectorPersistenceEntityMapper<StationConnectorEntity>());
        mappers.putIfAbsent(FlatBusEntity.class, new FlatBusPersistenceEntityMapper<FlatBusEntity>());
        mappers.putIfAbsent(FlatStationEntity.class, new FlatStationPersistenceEntityMapper<FlatStationEntity>());
        mappers.putIfAbsent(FlatStationConnectorEntity.class, new FlatStationConnectorPersistenceEntityMapper<FlatStationConnectorEntity>());
        mappers.putIfAbsent(OperationShiftEntity.class, new OperationShiftPersistenceEntityMapper<OperationShiftEntity>());
        mappers.putIfAbsent(AbsenceEntity.class, new AbsencePersistenceEntityMapper<AbsenceEntity>());
        mappers.putIfAbsent(FlatOperationEntity.class, new FlatOperationPersistenceEntityMapper<FlatOperationEntity>());
        mappers.putIfAbsent(FlatRouteEntity.class, new FlatRoutePersistenceEntityMapper<FlatRouteEntity>());
        mappers.putIfAbsent(OperationRouteRideEntityFast.class, new OperationRouteRideEntityFastPersistenceEntityMapper<OperationRouteRideEntityFast>());
    }

    public List getAll(Class classOfEntity) {
        DBMapper mapper = mappers.get(classOfEntity);
        return mapper.getAll();

    }

    public List getAllWhere(Class classOfEntity, String where) {
        DBMapper mapper = mappers.get(classOfEntity);
        return mapper.getAllWhere(where);

    }

    public void create(@NotNull Object object) {
        DBMapper mapper = mappers.get(object.getClass());
        mapper.create(object);
    }

    public Object read(Integer id, Class classOfEntity) {
        DBMapper mapper = mappers.get(classOfEntity);
        return mapper.read(id);
    }

    public void update(@NotNull Object object) {
        DBMapper mapper = mappers.get(object.getClass());
        mapper.update(object);
    }

    public void delete(@NotNull Object object) {
        DBMapper mapper = mappers.get(object.getClass());
        mapper.delete(object);
    }

    public void updateWithFragments(Class classOfEntity, String setQueryFragment, String whereQueryFragment) {
        DBMapper mapper = mappers.get(classOfEntity);
        mapper.update(setQueryFragment, whereQueryFragment);
    }

    public Object executeTransaction(@NotNull Function<Session, ?> function) {
        Session session = DatabaseConnector.getSession();
        Transaction transaction = session.beginTransaction();
        Object apply = function.apply(session);
        transaction.commit();
        session.close();
        return apply;
    }

}
