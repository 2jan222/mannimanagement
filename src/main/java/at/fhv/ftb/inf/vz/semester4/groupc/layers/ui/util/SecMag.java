package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util;

public class SecMag extends SecurityManager {

    public void inspect() {
        int i = 0;
        int maxDepth = 60;
        for (; i <= maxDepth && i < this.getClassContext().length; ++i) {
            Class aClass = this.getClassContext()[i];
            System.out.println(aClass);
        }

    }
}
