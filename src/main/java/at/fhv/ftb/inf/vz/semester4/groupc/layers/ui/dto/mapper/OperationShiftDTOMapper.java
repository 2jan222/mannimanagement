package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationShift;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.OperationShiftDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
@Deprecated
public interface OperationShiftDTOMapper {

    OperationShiftDTOMapper INSTANCE = Mappers.getMapper(OperationShiftDTOMapper.class);

    OperationShiftDTO toDTO(OperationShift operationShift);

    @Mapping(target = "shiftRides", ignore = true)
    @Mapping(target = "driver", ignore = true)
    OperationShift toMojo(OperationShiftDTO operationShiftDTO);

    void updateDTO(OperationShift operationShift, @MappingTarget OperationShiftDTO operationShiftDTO);

    @Mapping(target = "shiftRides", ignore = true)
    @Mapping(target = "driver", ignore = true)
    void updateMojo(OperationShiftDTO operationShiftDTO, @MappingTarget OperationShift operationShift);

    List<OperationShiftDTO> toDTOs(List<OperationShift> operationShifts);

    List<OperationShift> toMojos(List<OperationShiftDTOMapper> operationShiftDTOs);
}
