package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {RouteRideEntityMapper.class, PathEntityMapper.class})
public interface RouteEntityMapper {
    RouteEntityMapper INSTANCE = Mappers.getMapper(RouteEntityMapper.class);

    @Mapping(target = "paths", source = "pathsByRouteId")
    @Mapping(target = "routeRides", source = "routeRidesByRouteId")
    Route toMojo(RouteEntity routeEntity);

    @Mapping(target = "pathsByRouteId", source = "paths")
    @Mapping(target = "routeRidesByRouteId", source = "routeRides")
    RouteEntity toPojo(Route route);

    @Mapping(target = "paths", source = "pathsByRouteId")
    @Mapping(target = "routeRides", source = "routeRidesByRouteId")
    void updateMojo(RouteEntity routeEntity, @MappingTarget Route route);

    @Mapping(target = "pathsByRouteId", source = "paths")
    @Mapping(target = "routeRidesByRouteId", source = "routeRides")
    void updatePojo(Route route, @MappingTarget RouteEntity routeEntity);

    List<Route> toMojos(List<RouteEntity> routeEntities);

    List<RouteEntity> toPojos(List<Route> routes);

}
