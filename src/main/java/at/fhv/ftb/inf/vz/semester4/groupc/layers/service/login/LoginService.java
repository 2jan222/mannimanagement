package at.fhv.ftb.inf.vz.semester4.groupc.layers.service.login;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.assignment.BusAssignmentView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.driver_assigment.DriverAssignmentView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.tour.TourView;
import com.airhacks.afterburner.views.FXMLView;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;

import java.util.HashMap;
import java.util.LinkedList;
import javax.inject.Singleton;

/**
 * LoginService.
 * Deals with Login
 */
@Singleton
@LoggerableClassDisplayName("[LoginService]")
public class LoginService {
    //TODO Ask Database.
    private HashMap<String, String> map = new HashMap<>();

    {
        map.put("admin", "admin");
        map.put("username", "password");
        map.put("", "");
    }

    public boolean validate(String username, String password) {
        return map.containsKey(username) && password.equals(map.get(username));
    }

    public LinkedList<FXMLView> getViewsForUser() {
        //TODO Ask Database/Session User which views should be displayed.
        LinkedList<FXMLView> views = new LinkedList<>();
        //views.add(new ErrorView());
        //views.add(new BlueBGView());
        //views.add(new RedBGView());
        //views.add(new StationView());
        //views.add(new PathCreationView());
        views.add(new DriverAssignmentView());
        views.add(new BusAssignmentView());
        //views.add(new AssignDriverView());
        //views.add(new StationPathView());
        views.add(new TourView());
        return views;
    }
}
