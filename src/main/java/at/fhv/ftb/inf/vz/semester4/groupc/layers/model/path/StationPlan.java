package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationConnector;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.TestOnly;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.stream.Collectors;
import javax.inject.Singleton;

/**
 * Graph of Stations.
 */
@Singleton
public class StationPlan {
    private static final StationPlan INSTANCE = new StationPlan();
    private static HashMap<Integer, Station> lookupId2Station = new HashMap<>();
    private static HashMap<Station, Integer> lookupStation2Id = new HashMap<>();
    private static HashMap<Integer, LinkedList<Pair<Integer, StationConnector>>> graph = new HashMap<>();

    public static boolean addStation(Station station) {
        if (lookupStation2Id.containsKey(station)) {
            return false;
        } else {
            int lookupId = addStationToLookup(station);
            graph.put(lookupId, new LinkedList<>());
            return true;
        }
    }

    public static boolean addStationPath(@NotNull StationConnector connection, boolean addMissing) {
        Station start = connection.getStart();
        Station end = connection.getEnd();

        boolean success = true;
        if (!lookupStation2Id.containsKey(start)) {
            if (addMissing) {
                success = addStation(start);
            } else {
                return false;
            }
        }
        if (success && !lookupStation2Id.containsKey(end)) {
            if (addMissing) {
                success = addStation(end);
            } else {
                removeStationFromLookUp(start);
                return false;
            }
        }
        int lookupEnd = lookup(end);
        if (success && lookupEnd >= 0) {
            Pair<Integer, StationConnector> pair = new Pair<>(lookupEnd, connection);
            int lookupStart = lookup(start);
            if (lookupStart >= 0 && graph.containsKey(lookupStart)) {
                LinkedList<Pair<Integer, StationConnector>> pairs = graph.get(lookupStart);
                if (pairs == null) {
                    pairs = new LinkedList<>();
                }
                pairs.addLast(pair);
            }
            return true;
        } else {
            return false;
        }

    }

    @SuppressWarnings("WeakerAccess")
    @NotNull
    public static LinkedList<Pair<Station, StationConnector>> getNeighbourStationWithConnections(Station station) {
        int lookup = lookup(station);
        if (lookup < 0) {
            return new LinkedList<>();
        } else {
            return graph.get(lookup)
                    .stream()
                    .map(e -> new Pair<>(lookup(e.getKey()), e.getValue()))
                    .collect(Collectors.toCollection(LinkedList::new));
        }
    }

    @SuppressWarnings("WeakerAccess")
    @NotNull
    public static LinkedList<Station> getNeighbourStations(Station station) {
        int lookup = lookup(station);
        if (lookup < 0) {
            return new LinkedList<>();
        } else {
            return (LinkedList<Station>) graph.get(lookup)
                    .stream()
                    .map(e -> lookup(e.getKey()))
                    .collect(Collectors.toList());
        }
    }

    @SuppressWarnings("WeakerAccess")
    @NotNull
    public static LinkedList<Station> getAllStations() {
        return new LinkedList<>(lookupId2Station.values());
    }

    @SuppressWarnings("WeakerAccess")
    public static LinkedList<StationConnector> getAllConnections() {
        LinkedList<StationConnector> all = new LinkedList<>();
        graph.values().forEach(list -> list.forEach(e -> {
            if (!all.contains(e.getValue())) {
                all.add(e.getValue());
            }
        }));
        return all;
    }

    private static int lookup(@NotNull Station station) {
        Integer integer = lookupStation2Id.get(station);
        return (integer != null) ? integer : -1;
    }

    @Nullable
    private static Station lookup(int id) {
        return lookupId2Station.get(id);
    }

    private static int addStationToLookup(Station station) {
        int id = lookupStation2Id.size() + 1;
        if (!lookupStation2Id.containsKey(station) && !lookupId2Station.containsValue(station)) {
            //SECOND BOOLEAN EXPRESSION CAN BE OMITTED. KEEP FOR FAIL SAVE.
            lookupId2Station.put(id, station);
            lookupStation2Id.put(station, id);
        }
        return id;
    }

    private static void removeStationFromLookUp(Station station) {
        lookupId2Station.remove(lookupStation2Id.remove(station));
    }


    @SuppressWarnings("WeakerAccess")
    public static LinkedList<StationConnector> getConnectionsBetween(Station start, Station endStation) {
        LinkedList<Pair<Station, StationConnector>> withConnections = getNeighbourStationWithConnections(start);
        LinkedList<StationConnector> stationConnectors = new LinkedList<>();
        withConnections.forEach(e -> {
            if (e.getKey() == endStation) {
                stationConnectors.add(e.getValue());
            }
        });
        return stationConnectors;
    }

    @Contract(pure = true)
    public static StationPlan getInstance() {
        return INSTANCE;
    }

    @TestOnly
    StationPlan() {
    }
}
