package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Mapper(uses = StationEntityMapper.class)
public interface ShiftRideEntityMapper {

    ShiftRideEntityMapper INSTANCE = Mappers.getMapper(ShiftRideEntityMapper.class);

    @Mapping(target = "date", ignore = true)
    @Mapping(target = "operationShiftId", ignore = true)
    @Mapping(target = "operationId", ignore = true)
    //@Mapping(target = "date", source = "routeRideDate")
    @Mapping(target = "startTime", source = "startTimeByStartTimeId.startTime")
    @Mapping(target = "start", source = "routeRideEntity", qualifiedByName = "getStartStation")
    @Mapping(target = "shiftRideId", source = "routeRideId")
    @Mapping(target = "end", source = "routeRideEntity", qualifiedByName = "getEndStation")
    @Mapping(target = "endTime", source = "routeRideEntity", qualifiedByName = "endTimeMapping")
    ShiftRide toMojo(RouteRideEntityFast routeRideEntity);

    @Mapping(target = "shifts", ignore = true)
    @Mapping(target = "operations", ignore = true)
    @Mapping(target = "routeEntity", ignore = true)
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "startTimeByStartTimeId", ignore = true)
    //@Mapping(target = "routeRideDate", source = "date")
    @Mapping(target = "routeRideId", source = "shiftRideId")
    @Mapping(target = "routeId", ignore = true)
    //@Mapping(target = "routeByRouteId", ignore = true)
    //@Mapping(target = "operationByOperationId", ignore = true)
    //@Mapping(target = "busId", ignore = true)
    //@Mapping(target = "busByBusId", ignore = true)
    RouteRideEntityFast toPojo(ShiftRide shiftRide);

    @Mapping(target = "date", ignore = true)
    @Mapping(target = "operationShiftId", ignore = true)
    @Mapping(target = "operationId", ignore = true)
    //@Mapping(target = "date", source = "routeRideDate")
    @Mapping(target = "startTime", source = "startTimeByStartTimeId.startTime")
    @Mapping(target = "start", source = "routeRideEntity", qualifiedByName = "getStartStation")
    @Mapping(target = "shiftRideId", source = "routeRideId")
    @Mapping(target = "end", source = "routeRideEntity", qualifiedByName = "getEndStation")
    @Mapping(target = "endTime", source = "routeRideEntity", qualifiedByName = "endTimeMapping")
    void updateMojo(RouteRideEntityFast routeRideEntity, @MappingTarget ShiftRide shiftRide);

    @Mapping(target = "shifts", ignore = true)
    @Mapping(target = "operations", ignore = true)
    @Mapping(target = "routeEntity", ignore = true)
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "startTimeByStartTimeId", ignore = true)
    //@Mapping(target = "routeRideDate", source = "date")
    @Mapping(target = "routeRideId", source = "shiftRideId")
    @Mapping(target = "routeId", ignore = true)
    //@Mapping(target = "routeByRouteId", ignore = true)
    //@Mapping(target = "operationByOperationId", ignore = true)
    //@Mapping(target = "busId", ignore = true)
   // @Mapping(target = "busByBusId", ignore = true)
    void updatePojo(ShiftRide shiftRide, @MappingTarget RouteRideEntityFast routeRideEntity);

    List<ShiftRide> toMojos(List<RouteRideEntityFast> routeRideEntities);

    List<RouteRideEntityFast> toPojos(List<ShiftRide> shiftRides);

    @Named("endTimeRoute")
    default LocalTime endTimeMapping(RouteRideEntityFast mojo) {
        Set<PathStationEntity> pathStations = mojo.getStartTimeByStartTimeId().getPathByPathId().getPathStationsByPathId();
        long l = 0;
        for (PathStationEntity station : pathStations) {
            l = l + station.getStationConnector().getDuration();
        }
        return mojo.getStartTimeByStartTimeId().getStartTime().plus(l, ChronoUnit.SECONDS);

    }

    @Named("getStartStation")
    default Station getStartStation(RouteRideEntityFast pojo) {
       return getStationList(pojo).getFirst().getConnection().getStart();
    }
    @Named("getEndStation")
    default Station getEndStation(RouteRideEntityFast pojo) {
        return getStationList(pojo).getLast().getConnection().getEnd();
    }

    default LinkedList<PathStation> getStationList(RouteRideEntityFast pojo) {
        Set<PathStationEntity> entities = pojo.getStartTimeByStartTimeId().getPathByPathId().getPathStationsByPathId();
        LinkedList<PathStationEntity> entList = new LinkedList<>(entities);
        ArrayList<PathStation> list = (ArrayList<PathStation>) PathStationEntityMapper.INSTANCE.toMojos(entList);
        LinkedList<PathStation> linkList = new LinkedList<>(list);
        linkList.sort(PathStation::compareTo);
        return linkList;

    }
}
