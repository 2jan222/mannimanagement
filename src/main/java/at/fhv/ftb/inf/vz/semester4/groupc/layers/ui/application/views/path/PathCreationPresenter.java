package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.service.path.PathService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.PathControlsPanePresenter;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.PathControlsPaneView;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util.DragAndDropManager;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * PathCreationPresenter.
 * Views the stations on the right side to add to paths of routes
 */
@LoggerableClassDisplayName("[PathCreationPresenter]")
public class PathCreationPresenter implements MenuViewItem, Initializable {

    @Inject
    PathService pathService;

    @Inject
    Loggerable logger;

    @FXML
    private TableView<StationDTO> stationTableView;

    @FXML
    private TableColumn<StationDTO, String> stationNameCol;

    @FXML
    private TableColumn<StationDTO, Long> stationNumberCol;

    @FXML
    private TableColumn<StationDTO, String> stationAbbreviationCol;

    @FXML
    private AnchorPane routeAnchorPane;

    @FXML
    private SplitPane splitPlane;

    private PathControlsPanePresenter childrenPresenter;


    @Override
    public String getMenuDisplayName() {
        return "Route & Path";
    }

    @Override
    public void onMenuSwitch() {
        refresh();
    }

    @Override
    public void onTabExit() {
        if (childrenPresenter != null) {
            childrenPresenter.tabExitCall();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logger.debug("Init Path");
        stationNameCol.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getStationName()));
        stationAbbreviationCol.setCellValueFactory(cellData ->
                new SimpleStringProperty(cellData.getValue().getShortName()));
        stationNumberCol.setCellValueFactory(cellData ->
                new SimpleLongProperty(cellData.getValue().getStationId()).asObject());
        stationTableView.setOnDragDetected(e -> {
            Dragboard dragBoard = stationTableView.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            StationDTO src = stationTableView.getSelectionModel().getSelectedItem();
            DragAndDropManager.add("" + src.getStationId(), src);
            content.putString("" + src.getStationId());
            dragBoard.setContent(content);
            e.consume();
        });

        PathControlsPaneView pathPaneView = new PathControlsPaneView();
        childrenPresenter = ((PathControlsPanePresenter) pathPaneView.getPresenter());
        childrenPresenter.setWrapperPresenter(this);
        routeAnchorPane.getChildren().add(pathPaneView.getView());

        setStationTableViewsStations();
        childrenPresenter.refresh();
        logger.debug("Done Init Path");
    }

    public void setStage(Stage stage) {
        if (stage != null) {
            logger.debug("Stage set");
            stage.widthProperty().addListener((obs, oldVal, newVal) -> recalculateDivider());
            stage.heightProperty().addListener((obs, oldVal, newVal) -> recalculateDivider());
            stage.iconifiedProperty().addListener((ov, t, t1) -> recalculateDivider());
            stage.maximizedProperty().addListener((ov, t, t1) -> recalculateDivider());
            childrenPresenter.setStage(stage);
        } else {
            logger.warn("Stage should not be null");
        }
    }

    public void refreshFromBelow() {
        //ADD Logic to stop a refresh request
        refresh();
    }

    private void refresh() {
        logger.debug("Refresh");
        setStationTableViewsStations();
        recalculateDivider();
        childrenPresenter.refresh();
    }

    private void recalculateDivider() {
        logger.debug("Recalculate Divider");
        double w = 0;
        for (TableColumn<StationDTO, ?> e : stationTableView.getColumns()) {
            w += e.getWidth();
        }
        w = 1 - ((w + 20) / splitPlane.getWidth());
        splitPlane.setDividerPositions(w);
    }

    private void setStationTableViewsStations() {
        long startMilli = System.currentTimeMillis();
        logger.debug("Fetch and Set Stations");
        ObservableList<StationDTO> stations = FXCollections.observableArrayList(pathService.fetchStations());
        stationTableView.setItems(stations);
        logger.debug("Fetched and Set Stations[" + stations.size() + "](" + (System.currentTimeMillis() - startMilli) + " ms)");
    }

}
