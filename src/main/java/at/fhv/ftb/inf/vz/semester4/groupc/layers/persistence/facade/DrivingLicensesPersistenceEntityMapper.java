package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class DrivingLicensesPersistenceEntityMapper<DrivingLicensesEntity> extends PersistenceEntityMapper<DrivingLicensesEntity> {
    @Override
    public DrivingLicensesEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (DrivingLicensesEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.DrivingLicensesEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("DrivingLicensesEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<DrivingLicensesEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from DrivingLicensesEntity ");
            return (List<DrivingLicensesEntity>) query.getResultList();
        }
    }

    @Override
    public List<DrivingLicensesEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from DrivingLicensesEntity " + whereQueryFragment);
            return (List<DrivingLicensesEntity>) query.getResultList();
        }
    }

}
