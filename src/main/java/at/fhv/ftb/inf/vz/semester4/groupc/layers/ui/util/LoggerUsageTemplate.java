package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.util;

import com.github.jan222ik.Loggerable;
import com.github.jan222ik.LoggerableColor;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import com.github.jan222ik.loggers.MultiLogger;
import com.github.jan222ik.loggers.SystemStreamLogger;

@LoggerableClassDisplayName(value = "[Name]", color = LoggerableColor.ANSI_CYAN)
public class LoggerUsageTemplate {
    private Loggerable logger = Loggerable.getInstance();

    private void logTemplate() {
        logger.setChainPath(true);
        logger.setMaxDepth(60);
        logger.info("Info Message");
        logger.debug("Debug Message");
        logger.warn("Warn Message");
        logger.error("Error Message");
        logger.fatal("Fatal Message");
        new Subclass().log();
    }

    @LoggerableClassDisplayName(value = "[Subclass]", color = LoggerableColor.ANSI_YELLOW)
    class Subclass {

        private Loggerable logger = Loggerable.getInstance();
        void log() {
            logger.info("msg");
        }


    }

    public static void main(String[] args) {
        Loggerable.setLogger(new MultiLogger(new SystemStreamLogger()));
        new LoggerUsageTemplate().logTemplate();
    }
}
