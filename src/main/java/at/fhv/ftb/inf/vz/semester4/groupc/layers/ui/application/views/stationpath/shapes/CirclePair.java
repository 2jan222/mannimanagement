package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.stationpath.shapes;

import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

import java.util.Observable;
import java.util.Observer;

/**
 * Wrapper for two Circles and their connecting arrow.
 */
public class CirclePair extends Observable implements Observer {
    private Circle start;
    private Circle end;

    public CirclePair(Circle start, Circle end) {
        this.start = start;
        this.end = end;
    }

    public Circle getStart() {
        return start;
    }

    public void setStart(Circle start) {
        this.start = start;
    }

    public Circle getEnd() {
        return end;
    }

    public void setEnd(Circle end) {
        this.end = end;
    }


    @Override
    public void update(Observable o, Object arg) {
        //TO DO Write update function
    }

    public void redrawInto(AnchorPane container) {
        Arrow arrow = new Arrow(start, end);
        container.getChildren().addAll(start, end, arrow);
        arrow.redraw();
    }
}
