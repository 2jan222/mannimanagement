package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos;

/**
 * PathStationDTO.
 */
public class PathStationDTO {
    private Integer pathId;
    private Integer positionOnPath;
    private StationConnectorDTO connection;
    private Integer connectionId;
    private Integer pathStationId;

    public Integer getPathId() {
        return pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public Integer getPositionOnPath() {
        return positionOnPath;
    }

    public void setPositionOnPath(Integer positionOnPath) {
        this.positionOnPath = positionOnPath;
    }

    public StationConnectorDTO getConnection() {
        return connection;
    }

    public void setConnection(StationConnectorDTO connection) {
        this.connection = connection;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public Integer getPathStationId() {
        return pathStationId;
    }

    public void setPathStationId(Integer pathStationId) {
        this.pathStationId = pathStationId;
    }

    @Override
    public String toString() {
        return "PathStationDTO{" +
                "pathId=" + pathId +
                ", positionOnPath=" + positionOnPath +
                ", connection=" + connection +
                ", connectionId=" + connectionId +
                ", pathStationId=" + pathStationId +
                '}';
    }
}
