package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class RoutePersistenceEntityMapper<RouteEntity> extends PersistenceEntityMapper<RouteEntity> {
    @Override
    public RouteEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (RouteEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("RouteEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<RouteEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from RouteEntity ");
            return (List<RouteEntity>) query.getResultList();
        }
    }

    @Override
    public List<RouteEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from RouteEntity " + whereQueryFragment);
            return (List<RouteEntity>) query.getResultList();
        }
    }
}
