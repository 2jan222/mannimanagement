package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.tour;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@LoggerableClassDisplayName("[TourPresenter]")
public class TourPresenter implements MenuViewItem, Initializable {
    public AnchorPane root;

    @Override
    public String getMenuDisplayName() {
        return "Create Tour";
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Loggerable.getInstance().debug("Init integrated UI");

    }

    @Override
    public void setStage(Stage stage) {
        stage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case F5:
                        try {
                            Parent page = FXMLLoader.<Parent>load(TourPresenter.class.getResource("../../../../../../groupd/ui/scenes/TourScene.fxml"));
                            root.getChildren().clear();
                            root.getChildren().add(page);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
    }
}
