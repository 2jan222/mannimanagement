package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.demo.redbg;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.menu.MenuViewItem;

/**
 * Demo Presenter.
 */

public class RedBGPresenter implements MenuViewItem {
    @Override
    public String getMenuDisplayName() {
        return "RedBG";
    }
}
