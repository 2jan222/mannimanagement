package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import java.time.LocalDate;

public class VacationEntry {
    private Integer driverId;
    private LocalDate dateFrom;
    private LocalDate dateTo;

    public boolean isDateInVacation(LocalDate date) {
        return (date.isAfter(dateFrom) && date.isBefore(dateTo));
    }


    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return "VacationEntry{" +
                "driverId=" + driverId +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                '}';
    }
}
