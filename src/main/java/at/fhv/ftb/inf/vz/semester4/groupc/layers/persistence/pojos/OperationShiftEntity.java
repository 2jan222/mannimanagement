package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos;

import org.jetbrains.annotations.TestOnly;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@Entity
@Table(name = "operation_shift", schema = "public", catalog = "kmobil")
public class OperationShiftEntity {
    private Integer operationShiftId;
    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer operationId;
    private Integer driverId;
    private Integer busId;
    private Set<RouteRideEntityFast> routeRidesByOperationShiftId;
    private DriverEntity driverByDriverId;

    public OperationShiftEntity() {
    }

    @TestOnly
    public OperationShiftEntity(Integer operationShiftId, LocalDate date, LocalTime startTime, LocalTime endTime, Integer operationId, Integer driverId, Integer busId) {
        this.operationShiftId = operationShiftId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.operationId = operationId;
        this.driverId = driverId;
        this.busId = busId;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "operation_shift_id")
    public Integer getOperationShiftId() {
        return operationShiftId;
    }

    public void setOperationShiftId(Integer operationShiftId) {
        this.operationShiftId = operationShiftId;
    }

    @Basic
    @Column(name = "date")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Basic
    @Column(name = "start_time")
    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time")
    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "operation_id")
    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "driver_id")
    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    @Basic
    @Column(name = "bus_id")
    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "operation_shift_ride", joinColumns = { @JoinColumn(name = "operation_shift_id") }, inverseJoinColumns = {
            @JoinColumn(name = "operation_ride_id") })
    public Set<RouteRideEntityFast> getRouteRidesByOperationShiftId() {
        return routeRidesByOperationShiftId;
    }

    public void setRouteRidesByOperationShiftId(Set<RouteRideEntityFast> routeRidesByOperationShiftId) {
        this.routeRidesByOperationShiftId = routeRidesByOperationShiftId;
    }

    @ManyToOne
    @JoinColumn(name = "driver_id", referencedColumnName = "driver_id", nullable = false, insertable = false, updatable = false)
    public DriverEntity getDriverByDriverId() {
        return driverByDriverId;
    }

    public void setDriverByDriverId(DriverEntity driverByDriverId) {
        this.driverByDriverId = driverByDriverId;
    }

    @Override
    public String toString() {
        return "OperationShiftEntity{" +
                "operationShiftId=" + operationShiftId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", operationId=" + operationId +
                ", driverId=" + driverId +
                ", busId=" + busId +
                //", routeRidesByOperationShiftId=" + routeRidesByOperationShiftId.size() +
                ", driverByDriverId=" + driverByDriverId +
                '}';
    }
}
