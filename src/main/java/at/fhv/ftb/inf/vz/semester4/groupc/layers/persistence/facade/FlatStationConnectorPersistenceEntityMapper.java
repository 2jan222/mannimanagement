package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.facade;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class FlatStationConnectorPersistenceEntityMapper<FlatStationConnectorEntity> extends PersistenceEntityMapper<FlatStationConnectorEntity> {
    @Override
    public FlatStationConnectorEntity read(Integer id) {
        try (Session sess = DatabaseConnector.getSession()) {
            return (FlatStationConnectorEntity) sess.get(at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.FlatStationConnectorEntity.class, id);
        }
    }

    @Override
    public void update(String setQueryFragment, String whereQueryFragment) {
        PersistenceEntityMapper.updateHelper("FlatStationConnectorEntity", setQueryFragment, whereQueryFragment);
    }

    @Override
    public List<FlatStationConnectorEntity> getAll() {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatStationConnectorEntity ");
            return (List<FlatStationConnectorEntity>) query.getResultList();
        }
    }

    @Override
    public List<FlatStationConnectorEntity> getAllWhere(String whereQueryFragment) {
        try (Session session = DatabaseConnector.getSession()) {
            final Query query = session.createQuery("from FlatStationConnectorEntity " + whereQueryFragment);
            return (List<FlatStationConnectorEntity>) query.getResultList();
        }
    }
}
