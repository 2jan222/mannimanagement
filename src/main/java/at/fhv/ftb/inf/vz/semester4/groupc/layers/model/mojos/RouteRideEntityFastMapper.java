package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.PathStationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import org.jetbrains.annotations.NotNull;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

@Mapper(uses = {PathStationEntityMapper.class, BusEntityMapper.class})
public interface RouteRideEntityFastMapper {
    RouteRideEntityFastMapper INSTANCE = Mappers.getMapper(RouteRideEntityFastMapper.class);


    @Mapping(target = "variation", ignore = true)
    @Mapping(target = "startTime", source = "startTimeByStartTimeId.startTime")
    @Mapping(target = "routeNumber", source = "routeEntity.routeNumber")
    @Mapping(target = "requiredCapacity", source = "startTimeByStartTimeId.requiredCapacity")
    @Mapping(target = "pathStations", source = "startTimeByStartTimeId.pathByPathId.pathStationsByPathId")
    @Mapping(target = "pathId", source = "startTimeByStartTimeId.pathId")
    @Mapping(target = "operationShiftId", ignore = true)
    @Mapping(target = "operationId", ignore = true)
    @Mapping(target = "endTime", source = "routeRideEntity",  qualifiedByName = "endTimeMapping")
    @Mapping(target = "date", ignore = true)
    @Mapping(target = "busId", ignore = true)
    @Mapping(target = "bus", ignore = true)
    RouteRide toMojo(RouteRideEntityFast routeRideEntity);


    @Mapping(target = "operations", ignore = true)
    @Mapping(target = "routeEntity", ignore = true)
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "startTimeByStartTimeId", ignore = true)
    RouteRideEntityFast toPojo(RouteRide routeRide);

    @Mapping(target = "variation", ignore = true)
    @Mapping(target = "startTime", source = "startTimeByStartTimeId.startTime")
    @Mapping(target = "routeNumber", source = "routeEntity.routeNumber")
    @Mapping(target = "requiredCapacity", source = "startTimeByStartTimeId.requiredCapacity")
    @Mapping(target = "pathStations", source = "startTimeByStartTimeId.pathByPathId.pathStationsByPathId")
    @Mapping(target = "pathId", source = "startTimeByStartTimeId.pathId")
    @Mapping(target = "operationShiftId", ignore = true)
    @Mapping(target = "operationId", ignore = true)
    @Mapping(target = "endTime", source = "routeRideEntity",  qualifiedByName = "endTimeMapping")
    @Mapping(target = "date", ignore = true)
    @Mapping(target = "busId", ignore = true)
    @Mapping(target = "bus", ignore = true)
    void updateMojo(RouteRideEntityFast routeRideEntity, @MappingTarget RouteRide routeRide);

    @Mapping(target = "operations", ignore = true)
    @Mapping(target = "routeEntity", ignore = true)
    @Mapping(target = "startTimeId", ignore = true)
    @Mapping(target = "startTimeByStartTimeId", ignore = true)
    void updatePojo(RouteRide routeRide, @MappingTarget RouteRideEntityFast routeRideEntity);

    List<RouteRide> toMojos(List<RouteRideEntityFast> routeRideEntities);

    List<RouteRideEntityFast> toPojos(List<RouteRide> routeRides);

    @SuppressWarnings("UnmappedTargetProperties")
    @Named("endTimeRoute")
    default LocalTime endTimeMapping(@NotNull RouteRideEntityFast mojo) {
        Set<PathStationEntity> pathStations = mojo.getStartTimeByStartTimeId().getPathByPathId().getPathStationsByPathId();
        long l = 0;
        for (PathStationEntity station : pathStations) {
            l = l + station.getStationConnector().getDuration();
        }
        return mojo.getStartTimeByStartTimeId().getStartTime().plus(l, ChronoUnit.SECONDS);

    }
}
