package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Path;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Route;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.PathCreator;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.QueryService;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.RouteDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.StationDTOMapper;
import com.github.jan222ik.Loggerable;
import com.github.jan222ik.annotations.LoggerableClassDisplayName;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import org.jetbrains.annotations.NotNull;

@SuppressWarnings("WeakerAccess")
@LoggerableClassDisplayName("[PathCreationInfo]")
public class PathCreationInfo {
    private static Loggerable logger = Loggerable.getInstance();
    private SimpleBooleanProperty isValid;
    private SimpleBooleanProperty hasChanged = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty isSaved = new SimpleBooleanProperty(true);
    private PathCreator creator;

    public PathCreationInfo(@NotNull PathCreator creator) {
        this.creator = creator;
        this.isValid = new SimpleBooleanProperty(creator.isPathComplete());
    }

    public PathCreator getCreator() {
        return creator;
    }

    public void setCreator(PathCreator creator) {
        hasChanged.set(true);
        this.creator = creator;
    }

    public boolean isIsValid() {
        return isValid.get();
    }


    public boolean isHasChanged() {
        return hasChanged.get();
    }

    public void registerHasChangedChangeListener(ChangeListener<Boolean> changeListener) {
        hasChanged.addListener(changeListener);
    }

    public void registerIsValidChangeListener(ChangeListener<Boolean> changeListener) {
        hasChanged.addListener(changeListener);
    }

    public void registerIsSavedChangeListener(ChangeListener<Boolean> changeListener) {
        isSaved.addListener(changeListener);
    }

    public void setHasChanged(boolean hasChanged) {
        this.hasChanged.set(hasChanged);
    }

    public boolean getIsSaved() {
        return isSaved.get();
    }

    public SimpleBooleanProperty isSavedProperty() {
        return isSaved;
    }

    public boolean validate() {
        if (creator != null) {
            isValid.set(creator.isPathComplete());
            hasChanged.set(false);
            return true;
        } else {
            return false;
        }
    }

    public boolean addStation(StationDTO before, StationDTO stationDTO) {
        Station beforeM = StationDTOMapper.INSTANCE.toMojo(before);
        Station station = StationDTOMapper.INSTANCE.toMojo(stationDTO);
        hasChanged.set(creator.addStation(beforeM, station));
        if (hasChanged.get()) {
            isSaved.set(false);
        }
        return hasChanged.get();
    }

    public void save(RouteDTO routeDTO, boolean isToPath) {
        if (creator != null) {
            Route route = RouteDTOMapper.INSTANCE.toMojo(routeDTO);
            Path path = creator.toPath(route, isToPath);
            try {
                QueryService.persistPathForRoute(path);
            } catch (NullPointerException npe) {
                logger.error("Path could not be persisted: Possible Reason a Path Connector is Null");
            }
            isSaved.set(true);
        } else {
            logger.debug("Delete Path");
            isSaved.set(true);
        }
    }
}
