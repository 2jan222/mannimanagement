package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.BusDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.DriverDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.OperationDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.RouteRideDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftEntryDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftRideDTO;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;

public interface AssignDriverFacade {

    LinkedList<ShiftEntryDTO> getShiftsByOperationId(Integer operationId, LocalDate date) throws AssignDriverContextCreationException;

    LinkedList<Integer> getOperationsByIDOfDay(LocalDate date);

    LinkedList<DriverDTO> getFreeDriverFormShift(ShiftEntryDTO shiftEntryDTO) throws AssignDriverContextCreationException;

    Driver getDriverById(Integer driverId, LocalDate date) throws AssignDriverContextCreationException;

    boolean assignDriverToShift(ShiftEntryDTO shiftDTO, DriverDTO driverDTO) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException;

    boolean removeDriverFromShift(ShiftEntryDTO shiftDTO) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException;

    boolean save(LocalDate date);

    OperationDTO getOperationFromId(Integer operationId, LocalDate date);

    ShiftEntryDTO setShift(Integer operationId, LinkedList<ShiftRide> rides, LocalDate date) throws AssignDriverContextCreationException;
    ShiftEntry setShiftForTimeFrame(Integer operationId, LocalTime start, LocalTime end, LocalDate date) throws AssignDriverContextCreationException, AssignDriverDataTransferUpdateException;

    boolean addRideToShift(ShiftEntryDTO shiftDTO, RouteRideDTO toAdd);

    boolean removeRideFromShift(ShiftEntryDTO shiftDTO, RouteRideDTO toRemove);

    boolean removeAllRidesAndDriverFromShift(ShiftEntryDTO shiftDTO);

    boolean removeShift(ShiftEntryDTO shiftDTO);

    boolean isSaved(LocalDate date);

    void clearContextForAllDates();

    BusDTO getBusByOperationId(Integer operationId, LocalDate date) throws AssignDriverContextCreationException;

    LinkedList<LocalDate> getDatesWithOperationsWhichHaveBus();

    void init(AssignDriverDataTransfer dataTransfer);

    LinkedList<ShiftRide> getRidesByOperationId(Integer id, LocalDate date) throws AssignDriverContextCreationException;
}
