package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.path.pathcontrolspane.pathsofroute.old.stationpane.connectionpane;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * ConnectionPanePresenter.
 * Views connections between two stations if there is more than one.
 */
@Deprecated
public class ConnectionPanePresenter implements Initializable {
    public Button connectionButton;

    public String getConnectionButton() {
        return connectionButton.getText();
    }

    public void setConnectionButton(String connectionButton) {
        this.connectionButton.setText(connectionButton);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void useConnection(ActionEvent actionEvent) {
    }
}
