package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StartTime;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.StartTimeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * StartTimeDTO mapper.
 */
@Mapper(uses = {PathDTOMapper.class, RouteRideDTOMapper.class})
public interface StartTimeDTOMapper {
    StartTimeDTOMapper INSTANCE = Mappers.getMapper(StartTimeDTOMapper.class);

    StartTimeDTO toDTO(StartTime startTime);

    StartTime toMojo(StartTimeDTO startTimeDTO);

    void updateDTO(StartTime startTime, @MappingTarget StartTimeDTO startTimeDTO);

    void updateMojo(StartTimeDTO startTimeDTO, @MappingTarget StartTime startTime);

    List<StartTimeDTO> toDTOs(List<StartTime> startTimes);

    List<StartTime> toMojos(List<StartTimeDTO> startTimeDTOS);

}

