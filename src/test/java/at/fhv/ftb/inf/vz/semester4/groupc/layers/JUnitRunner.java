package at.fhv.ftb.inf.vz.semester4.groupc.layers;


import com.github.jan222ik.Loggerable;
import com.github.jan222ik.loggers.MultiLogger;
import com.github.jan222ik.loggers.SystemStreamLogger;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

@SuppressWarnings("JUnit5Platform")
@RunWith(JUnitPlatform.class)
public abstract class JUnitRunner {
    {
        Loggerable.setLogger(new MultiLogger(new SystemStreamLogger()));
    }
}
