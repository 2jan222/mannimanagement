package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.grouping;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Tests for class GroupActions")
class GroupActionsTest extends JUnitRunner {

    @Nested
    @DisplayName("Overlapping Tests[G=Group, C=Candidate, S=StartTime, E=EndTime]")
    public class Overlapping {

        private Supplier<Path> oneHourPath = () -> {
            Station station = new Station();
            Path path = new Path();
            PathStation pathS0 = new PathStation();
            pathS0.setPathId(path.getPathId());
            pathS0.setPositionOnPath(1);
            pathS0.setConnection(new StationConnector(station, station, 500, 3600));
            //pathS0.setStation(station);
            path.getPathStations().add(pathS0);
            return path;
        };
        private Supplier<Path> threeHourPath = () -> {
            Station station = new Station();
            Path path = new Path();
            PathStation pathS0 = new PathStation();
            pathS0.setPathId(path.getPathId());
            pathS0.setPositionOnPath(1);
            pathS0.setConnection(new StationConnector(station, station, 500, 10800));
           // pathS0.setStation(station);
            path.getPathStations().add(pathS0);
            return path;
        };


        @Test
        @DisplayName("Case 1: EC before SG -> no overlapping")
        void isTimeOverlappingCaseOne() {
            LocalTime operationStartTime = LocalTime.of(6, 0, 0);
            LocalTime candidateStartTime = LocalTime.of(3, 0, 0);
            Supplier<Path> operationPath = oneHourPath;
            Supplier<Path> candidatePath = oneHourPath;

            boolean timeOverlapping = overlappingHelper(operationStartTime, candidateStartTime, operationPath, candidatePath);
            assertTrue(timeOverlapping);
        }

        public boolean overlappingHelper(LocalTime groupStartTime, LocalTime candidateStartTime, Supplier<Path> groupRidePath, Supplier<Path> candidateRidePath) {
            Operation operation = new Operation();
            RouteRide groupRide1 = new RouteRide();
            StartTime opStartTime = new StartTime();
            groupRide1.setStartTime(groupStartTime);
            opStartTime.setStartTimeType(1);
            groupRide1.setPathStations(new LinkedList<>(groupRidePath.get().getPathStations()));
            operation.setRouteRides(new LinkedList<>(Collections.singletonList(groupRide1)));

            RouteRide can = new RouteRide();
            can.setPathStations(new LinkedList<>(candidateRidePath.get().getPathStations()));
            can.setStartTime(candidateStartTime);

            return new GroupActions().isTimeOverlapping(operation, can);
        }

        @Test
        @DisplayName("Case 2: EC after SG and SC before EG -> overlapping")
        void isTimeOverlappingCaseTwo() {
            LocalTime operationStartTime = LocalTime.of(6, 0, 0);
            LocalTime candidateStartTime = LocalTime.of(6, 30, 0);
            Supplier<Path> operationPath = oneHourPath;
            Supplier<Path> candidatePath = oneHourPath;

            boolean timeOverlapping = overlappingHelper(operationStartTime, candidateStartTime, operationPath, candidatePath);
            assertTrue(timeOverlapping);
        }

        @Test
        @DisplayName("Case 3: EC after SG and SC before EG and SC after SG -> overlapping")
        void isTimeOverlappingCaseTree() {
            LocalTime operationStartTime = LocalTime.of(6, 0, 0);
            LocalTime candidateStartTime = LocalTime.of(7, 0, 0);
            Supplier<Path> operationPath = threeHourPath;
            Supplier<Path> candidatePath = oneHourPath;

            boolean timeOverlapping = overlappingHelper(operationStartTime, candidateStartTime, operationPath, candidatePath);
            assertTrue(timeOverlapping);
        }


        @Test
        @DisplayName(" Case 4: EC after SG and EC after EG but SC before EG -> overlapping")
        void isTimeOverlappingCaseFour() {
            LocalTime operationStartTime = LocalTime.of(6, 0, 0);
            LocalTime candidateStartTime = LocalTime.of(6, 30, 0);
            Supplier<Path> operationPath = oneHourPath;
            Supplier<Path> candidatePath = oneHourPath;

            boolean timeOverlapping = overlappingHelper(operationStartTime, candidateStartTime, operationPath, candidatePath);
            assertTrue(timeOverlapping);
        }

        @Test
        @DisplayName("Case 5: EC after SG and SC after EG -> not overlapping")
        void isTimeOverlappingCaseFive() {
            LocalTime operationStartTime = LocalTime.of(6, 0, 0);
            LocalTime candidateStartTime = LocalTime.of(7, 0, 0);
            Supplier<Path> operationPath = oneHourPath;
            Supplier<Path> candidatePath = oneHourPath;

            boolean timeOverlapping = overlappingHelper(operationStartTime, candidateStartTime, operationPath, candidatePath);
            assertTrue(timeOverlapping);
        }
    }




    /*

    @Test
    @DisplayName("AssigneBusToGroup")
    void assignBusToGroup() {
        GroupActions groupActions = new GroupActions();
        //Operation result = GroupActions.assignBusToGroup(group, bus);

        LinkedList<RouteRide> newrre = new LinkedList<>(result.getRouteRides());
        assertEquals(21, (int) newrre.getFirst().getBusId());
    }

    @Test
    void removeBusFromGroup() {
        //Operation operation = GroupActions.assignBusToGroup(group,bus);

        LinkedList<RouteRide> newrre = new LinkedList<>(operation.getRouteRides());
        assertEquals(21, (int) newrre.getFirst().getBusId());

        Operation result = GroupActions.removeBusFromGroup(group);

        LinkedList<RouteRide> resultList = new LinkedList<>(operation.getRouteRides());
        assertNull(resultList.getFirst().getBusId());

    }

    @Test
    void createGrouping() throws GrouperException {
        Operation result = GroupActions.createGrouping(LocalDate.now(), rre, rre2);

        //Check if Grouping is created


    }

    @Test
    void addRideToGroup() throws GrouperException {
        Operation grouping = GroupActions.createGrouping(LocalDate.now(), rre, rre2);
        GroupActions.addRideToGroup(grouping, candidate);

        //Check if candidate is in grouping

    }

    @Test
    void removeRideFromGroup() throws GrouperException {


    }

    @Test
    void isTimeOverlapping() throws GrouperException {
        Operation grouping =GroupActions.createGrouping(LocalDate.now(),rre,rre2);
        boolean result = GroupActions.isTimeOverlapping(grouping,candidate);

        assertFalse(result);
    }
   
     */
}