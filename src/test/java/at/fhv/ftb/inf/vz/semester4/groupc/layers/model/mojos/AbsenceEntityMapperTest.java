package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.AbsenceEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class AbsenceEntityMapperTest {

    @Test
    public void absenceEntityToAbsenceAndReverse() {
        AbsenceEntity ae = new AbsenceEntity(1, 1, LocalDate.MIN, LocalDate.MAX);
        Absence a = AbsenceEntityMapper.INSTANCE.toMojo(ae);
        assertEquals(Integer.valueOf(ae.getAbsenceId()), a.getAbsenceId());
        assertEquals(Integer.valueOf(ae.getDriverId()), a.getDriverId());
        assertEquals(ae.getDateFrom(), a.getDateFrom());
        assertEquals(ae.getDateTo(), a.getDateTo());
        ae = AbsenceEntityMapper.INSTANCE.toPojo(a);
        assertEquals(a.getAbsenceId(), Integer.valueOf(ae.getAbsenceId()));
        assertEquals(a.getDriverId(), Integer.valueOf(ae.getDriverId()));
        assertEquals(a.getDateFrom(), ae.getDateFrom());
        assertEquals(a.getDateTo(), ae.getDateTo());
    }

}