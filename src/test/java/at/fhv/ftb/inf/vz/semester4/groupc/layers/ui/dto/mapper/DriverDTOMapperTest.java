package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.DriverDTO;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class DriverDTOMapperTest {

    @Test
    public void driverToDriverDTOAndReverse() {
        Driver driver = new Driver(12, "Lauterach, Hölzfällergasse 33a",
                LocalDate.of(1999, 12, 24), "karl.müller@gmail.at",
                1, "Karl", "Müller", "External Bus Driver",
                "05574 987654321");
        DriverDTO driverDTO = DriverDTOMapper.INSTANCE.toDTO(driver);
        assertEquals(driver.getDriverId(), driverDTO.getDriverId());
        assertEquals(driver.getAddress(), driverDTO.getAddress());
        assertEquals(driver.getBirthday(), driverDTO.getBirthday());
        assertEquals(driver.getEmail(), driverDTO.getEmail());
        assertEquals(driver.getEmploymentType(), driverDTO.getEmploymentType());
        assertEquals(driver.getFirstname(), driverDTO.getFirstname());
        assertEquals(driver.getLastname(), driverDTO.getLastname());
        assertEquals(driver.getJobDescription(), driverDTO.getJobDescription());
        assertEquals(driver.getTelephonenumber(), driverDTO.getTelephonenumber());
        driver = DriverDTOMapper.INSTANCE.toMojo(driverDTO);
        assertEquals(driverDTO.getDriverId(), driver.getDriverId());
        assertEquals(driverDTO.getAddress(), driver.getAddress());
        assertEquals(driverDTO.getBirthday(), driver.getBirthday());
        assertEquals(driverDTO.getEmail(), driver.getEmail());
        assertEquals(driverDTO.getEmploymentType(), driver.getEmploymentType());
        assertEquals(driverDTO.getFirstname(), driver.getFirstname());
        assertEquals(driverDTO.getLastname(), driver.getLastname());
        assertEquals(driverDTO.getJobDescription(), driver.getJobDescription());
        assertEquals(driverDTO.getTelephonenumber(), driver.getTelephonenumber());
    }

}