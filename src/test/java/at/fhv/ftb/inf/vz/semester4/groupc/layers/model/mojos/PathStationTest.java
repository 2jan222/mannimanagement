package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class PathStationTest {

    @Test
    public void testSortByPosition() {
        StationConnector sc = new StationConnector();
        LinkedList<PathStation> list = new LinkedList<>();
        PathStation pathStation1 = new PathStation(1, 1, sc);
        PathStation pathStation2 = new PathStation(2, 2, sc);
        PathStation pathStation3 = new PathStation(3, 3, sc);
        PathStation pathStation4 = new PathStation(4, 35, sc);
        PathStation pathStation5 = new PathStation(5, 13, sc);
        PathStation pathStation6 = new PathStation(6, 12, sc);
        PathStation pathStation7 = new PathStation(7, 4, sc);
        PathStation pathStation8 = new PathStation(8, 90, sc);
        PathStation pathStation9 = new PathStation(9, 6, sc);
        PathStation pathStation10 = new PathStation(10, 10, sc);
        list.add(pathStation3);
        list.add(pathStation5);
        list.add(pathStation9);
        list.add(pathStation10);
        list.add(pathStation6);
        list.add(pathStation2);
        list.add(pathStation1);
        list.add(pathStation8);
        list.add(pathStation7);
        list.add(pathStation4);
        list.sort(PathStation::compareTo);
        assertEquals(Integer.valueOf(1), list.getFirst().getPathId());
        assertEquals(Integer.valueOf(8), list.getLast().getPathId());
        assertEquals(Integer.valueOf(1), list.remove().getPathId());
        assertEquals(Integer.valueOf(2), list.remove().getPathId());
        assertEquals(Integer.valueOf(3), list.remove().getPathId());
        assertEquals(Integer.valueOf(7), list.remove().getPathId());
        assertEquals(Integer.valueOf(9), list.remove().getPathId());
        assertEquals(Integer.valueOf(10), list.remove().getPathId());
        assertEquals(Integer.valueOf(6), list.remove().getPathId());
        assertEquals(Integer.valueOf(5), list.remove().getPathId());
        assertEquals(Integer.valueOf(4), list.remove().getPathId());
        assertEquals(Integer.valueOf(8), list.remove().getPathId());

    }

}