package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftRideDTO;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class ShiftRideDTOMapperTest {

    @Test
    public void shiftRideToDTOAndReverse() {

        Station station1 = new Station(1, "Station1", "s1");
        Station station2 = new Station(2, "Station2", "s2");
        ShiftRide sr = new ShiftRide(1, station1, station2, LocalTime.of(19, 0, 0),
                LocalTime.of(19, 30, 0), 1, 14, LocalDate.now());
        ShiftRideDTO srd = ShiftRideDTOMapper.INSTANCE.toDTO(sr);
        assertEquals(Integer.valueOf(sr.getShiftRideId()), srd.getShiftRideId());
        assertEquals(sr.getStart().getShortName(), srd.getStartStationName());
        assertEquals(sr.getEnd().getShortName(), srd.getEndStationName());
        assertEquals(sr.getStartTime(), srd.getStartTime());
        assertEquals(sr.getEndTime(), srd.getEndTime());
        assertEquals(sr.getDate(), srd.getDate());
        assertEquals(sr.getOperationId(), srd.getOperationId());
        assertEquals(sr.getOperationShiftId(), srd.getOperationShiftId());
        sr = ShiftRideDTOMapper.INSTANCE.toMojo(srd);
        assertEquals(srd.getShiftRideId(), Integer.valueOf(sr.getShiftRideId()));
        assertEquals(srd.getStartTime(), sr.getStartTime());
        assertEquals(srd.getEndTime(), sr.getEndTime());
        assertEquals(srd.getDate(), sr.getDate());
        assertEquals(srd.getOperationId(), sr.getOperationId());
        assertEquals(srd.getOperationShiftId(), sr.getOperationShiftId());
        //note: stations are not mapped back, so Mojo should be updated, not created from DTO!
    }

    @Test
    public void updateDTOFromMojoAndReverse() {
        Station station1 = new Station(1, "Station1", "s1");
        Station station2 = new Station(2, "Station2", "s2");
        ShiftRide sr = new ShiftRide(1, station1, station2, LocalTime.of(19, 0, 0),
                LocalTime.of(19, 30, 0), 1, 14, LocalDate.now());
        ShiftRideDTO srd = ShiftRideDTOMapper.INSTANCE.toDTO(sr);
        srd.setOperationId(3);
        srd.setEndStationName("new End");
        srd.setStartStationName("new Start");
        srd.setDate(LocalDate.MAX);
        srd.setEndTime(LocalTime.of(21, 0, 0));
        srd.setStartTime(LocalTime.of(20, 0, 0));
        srd.setOperationShiftId(33);
        ShiftRideDTOMapper.INSTANCE.updateMojo(srd, sr);
        assertEquals(Integer.valueOf(3), sr.getOperationId());
        assertEquals(LocalDate.MAX, sr.getDate());
        assertEquals(LocalTime.of(21, 0, 0), sr.getEndTime());
        assertEquals(LocalTime.of(20, 0, 0), sr.getStartTime());
        assertEquals(Integer.valueOf(33), sr.getOperationShiftId());
        //check whether renaming has caused change in stations -> shouldn't have!
        assertEquals(station1, sr.getStart());
        assertEquals(station2, sr.getEnd());
        assertEquals(station1.getShortName(), sr.getStart().getShortName());
        assertEquals(station2.getShortName(), sr.getEnd().getShortName());

        //reverse
        ShiftRide newSr = new ShiftRide(2, station2, station1, LocalTime.of(10, 30, 0),
                LocalTime.of(11, 10, 0), 11, 8, LocalDate.MIN);
        ShiftRideDTOMapper.INSTANCE.updateDTO(newSr, srd);
        assertEquals(Integer.valueOf(newSr.getShiftRideId()), srd.getShiftRideId());
        assertEquals(newSr.getStart().getShortName(), srd.getStartStationName());
        assertEquals(newSr.getEnd().getShortName(), srd.getEndStationName());
        assertEquals(newSr.getStartTime(), srd.getStartTime());
        assertEquals(newSr.getEndTime(), srd.getEndTime());
        assertEquals(newSr.getDate(), srd.getDate());
        assertEquals(newSr.getOperationId(), srd.getOperationId());
        assertEquals(newSr.getOperationShiftId(), srd.getOperationShiftId());

    }


}