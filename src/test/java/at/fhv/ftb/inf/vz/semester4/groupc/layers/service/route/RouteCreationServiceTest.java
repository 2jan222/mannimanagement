package at.fhv.ftb.inf.vz.semester4.groupc.layers.service.route;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.DatabaseConnector;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class RouteCreationServiceTest extends JUnitRunner {

    @Test
    void isNumberInUse() {
        DatabaseConnector.init();
        RouteCreationService service = new RouteCreationService();
        boolean numberInUse = service.isNumberInUse(22, LocalDate.of(2019, 4, 1), LocalDate.of(2019, 4, 2));
        assertTrue(numberInUse);
        numberInUse = service.isNumberInUse(Integer.MAX_VALUE, LocalDate.MIN, LocalDate.MIN.plusDays(3));
        assertFalse(numberInUse);
    }
}