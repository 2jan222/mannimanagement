package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.StationConnector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class StationPlanTest {
    private StationPlan graph;
    private LinkedList<Station> stations;
    private Station s0;
    private Station s1;
    private Station s2;
    private Station s3;
    private StationConnector cS1S2;
    private StationConnector cS1S3;

    @BeforeEach
    void setUp() {
        graph = new StationPlan();
        stations = new LinkedList<>();
        s0 = new Station();
        s0.setStationId(0);
        s1 = new Station();
        s1.setStationId(1);
        s2 = new Station();
        s2.setStationId(2);
        s3 = new Station();
        s3.setStationId(3);
        cS1S2 = new StationConnector(s1,s2, 500, 500);
        cS1S3 = new StationConnector(s1,s3, 500, 500);
    }

    @Test
    void addStation() {
        Station add = new Station();
        boolean b = graph.addStation(add);
        assertTrue(b);
        assertTrue(graph.getAllStations().contains(add));
        b = graph.addStation(add);
        assertFalse(b);
        assertTrue(graph.getAllStations().contains(add));
    }

    @Test
    void addStationPath() {
        boolean b = graph.addStationPath(cS1S2, false);
        assertFalse(b);
        b = graph.addStationPath(cS1S2, true);
        assertTrue(b);
        graph.addStation(s3);
        b = graph.addStationPath(cS1S3, false);
        assertTrue(b);
    }

    @Test
    void getNeighbourStationWithConnections() {
        LinkedList<Pair<Station, StationConnector>> neighbourStations = graph.getNeighbourStationWithConnections(s0);
        assertEquals(0, neighbourStations.size());
    }

    @Test
    void getNeighbourStations() {
        LinkedList<Station> neighbourStations = graph.getNeighbourStations(s0);
        assertEquals(0, neighbourStations.size());
    }

    @Test
    void getAllStations() {
        LinkedList<Station> allStations = graph.getAllStations();
        allStations.forEach(e -> {
            if (!stations.contains(e)) {
                fail("Station in Graph is not in stations list");
            }
        });
        graph.addStation(s0);
        graph.addStation(s1);
        graph.addStationPath(cS1S2, true);
        stations.add(s0);
        stations.add(s1);
        stations.add(s2);
        allStations = graph.getAllStations();
        allStations.forEach(e -> {
            if (!stations.contains(e)) {
                fail("Station in Graph is not in stations list");
            }
        });
    }


    @Test
    void getAllConnections() {
        LinkedList<StationConnector> allConnections = graph.getAllConnections();
        assertEquals(0, allConnections.size());
        graph.addStationPath(cS1S2, true);
        graph.addStationPath(cS1S3, true);
        allConnections = graph.getAllConnections();
        assertEquals(2, allConnections.size());
        for (StationConnector c: allConnections) {
            if (!(cS1S2.equals(c) || cS1S3.equals(c))) {
                fail("A connection which was not passed exists");
            }
        }
    }

}