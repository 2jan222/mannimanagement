package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.AbsenceEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.DriverEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DriverEntityMapperTest {

    @Test
    public void driverEntityToDriverAndReverse() {
        DriverEntity entity = new DriverEntity(1, "Teststraße 12", LocalDate.now(),
                "max.muster@a1.net", 0, "Max",
                "Mustermann", "Bus Driver", "0664 123456789");
        AbsenceEntity ae = new AbsenceEntity(1, 1, LocalDate.MIN, LocalDate.MAX);
        AbsenceEntity ae2 = new AbsenceEntity(2, 1, LocalDate.now(), LocalDate.MAX);
        AbsenceEntity ae3 = new AbsenceEntity(3, 1, LocalDate.MIN, LocalDate.now());
        Set<AbsenceEntity> absenceEntitySet = new HashSet<>();
        absenceEntitySet.add(ae);
        absenceEntitySet.add(ae2);
        absenceEntitySet.add(ae3);
        entity.setAbsenceEntities(absenceEntitySet);
        Driver driver = DriverEntityMapper.INSTANCE.toMojo(entity);
        assertEquals(entity.getDriverId(), driver.getDriverId());
        assertEquals(entity.getAddress(), driver.getAddress());
        assertEquals(entity.getBirthday(), driver.getBirthday());
        assertEquals(entity.getEmail(), driver.getEmail());
        assertEquals(entity.getEmploymentType(), driver.getEmploymentType());
        assertEquals(entity.getFirstname(), driver.getFirstname());
        assertEquals(entity.getLastname(), driver.getLastname());
        assertEquals(entity.getJobDescription(), driver.getJobDescription());
        assertEquals(entity.getTelephonenumber(), driver.getTelephonenumber());
        assertEquals(entity.getAbsenceEntities().size(), driver.getAbsences().size());
        entity = DriverEntityMapper.INSTANCE.toPojo(driver);
        assertEquals(driver.getDriverId(), entity.getDriverId());
        assertEquals(driver.getAddress(), entity.getAddress());
        assertEquals(driver.getBirthday(), entity.getBirthday());
        assertEquals(driver.getEmail(), entity.getEmail());
        assertEquals(driver.getEmploymentType(), entity.getEmploymentType());
        assertEquals(driver.getFirstname(), entity.getFirstname());
        assertEquals(driver.getLastname(), entity.getLastname());
        assertEquals(driver.getJobDescription(), entity.getJobDescription());
        assertEquals(driver.getTelephonenumber(), entity.getTelephonenumber());
        assertEquals(driver.getAbsences().size(), entity.getAbsenceEntities().size());
    }

}