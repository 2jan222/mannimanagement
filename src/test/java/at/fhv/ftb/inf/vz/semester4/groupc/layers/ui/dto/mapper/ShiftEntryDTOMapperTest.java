package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftEntryDTO;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class ShiftEntryDTOMapperTest {

    @Test
    public void shiftEntryToDTOAndReverse() {
        ShiftEntry os = new ShiftEntry(1, LocalDate.now(),
                LocalTime.of(19, 30, 0),
                LocalTime.of(20, 30, 0), 3, 10, 4);
        ShiftEntryDTO osDTO = ShiftEntryDTOMapper.INSTANCE.toDTO(os);
        assertEquals(os.getShiftEntryId(), osDTO.getShiftEntryId());
        assertEquals(os.getDate(), osDTO.getDate());
        assertEquals(os.getStartTime(), osDTO.getStartTime());
        assertEquals(os.getEndTime(), osDTO.getEndTime());
        assertEquals(os.getOperationId(), osDTO.getOperationId());
        assertEquals(os.getDriverId(), osDTO.getDriverId());
        assertEquals(os.getBusID(), osDTO.getBusId());
        os = ShiftEntryDTOMapper.INSTANCE.toMojo(osDTO);
        assertEquals(osDTO.getShiftEntryId(), os.getShiftEntryId());
        assertEquals(osDTO.getDate(), os.getDate());
        assertEquals(osDTO.getStartTime(), os.getStartTime());
        assertEquals(osDTO.getEndTime(), os.getEndTime());
        assertEquals(osDTO.getOperationId(), os.getOperationId());
        assertEquals(osDTO.getDriverId(), os.getDriverId());
        assertEquals(osDTO.getBusId(), os.getBusID());
    }

}