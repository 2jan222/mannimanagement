package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.BusEntity;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class BusEntityMapperTest {
    @Test
    public void busEntityToBusAndReverseTest() {
        BusEntity ent = new BusEntity(1,5000,"KK","make","model","note", LocalDate.now(), 20, 50, null,null, null);
        Bus bus = BusEntityMapper.INSTANCE.toMojo(ent);
        assertEquals(Integer.valueOf(ent.getBusId()),bus.getBusId());
        assertEquals(ent.getLicenceNumber(), bus.getLicenceNumber());
        assertEquals(ent.getMake(), bus.getMake());
        assertEquals(ent.getModel(), bus.getModel());
        assertEquals(ent.getNote(), bus.getNote());
        assertEquals(ent.getSeatPlaces(), bus.getSeatPlaces());
        assertEquals(ent.getStandPlaces(), bus.getStandPlaces());
        ent = BusEntityMapper.INSTANCE.toPojo(bus);
        assertEquals(bus.getBusId(),Integer.valueOf(ent.getBusId()));
        assertEquals(5000, ent.getMaintenanceKm());
        assertEquals(bus.getLicenceNumber(), ent.getLicenceNumber());
        assertEquals(bus.getMake(), ent.getMake());
        assertEquals(bus.getModel(), ent.getModel());
        assertEquals(bus.getNote(), ent.getNote());
        assertEquals(LocalDate.now(), ent.getRegistrationDate());
        assertEquals(bus.getSeatPlaces(), ent.getSeatPlaces());
        assertEquals(bus.getStandPlaces(), ent.getStandPlaces());

    }

}