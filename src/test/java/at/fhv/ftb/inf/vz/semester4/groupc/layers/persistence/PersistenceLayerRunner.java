package at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.BusEntityMapperTest;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationEntityMapperTest;
import org.junit.platform.suite.api.SelectClasses;

@SelectClasses({BusEntityMapperTest.class, OperationEntityMapperTest.class})
public class PersistenceLayerRunner extends JUnitRunner {
}
