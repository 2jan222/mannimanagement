package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.cucumber;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.ApplicationStartTest;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/at/fhv/ftb/inf/vz/semester4/groupc/ui/cucumber/features"},
        tags = {("@All")},
        plugin = {"json:target/cucumber.json"/* "io.cucumber.pro.JsonReporter12:default"*/})
public class CucumberRunner extends ApplicationStartTest {}
