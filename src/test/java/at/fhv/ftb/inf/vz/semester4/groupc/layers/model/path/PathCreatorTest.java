package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class PathCreatorTest extends JUnitRunner {
    private PathCreator instance;
    private Station start;
    private Station end;
    private Station inter2;
    private Station inter1;
    private StationPlan plan;
    private StationConnector startInter1Connection;
    private StationConnector inter1Inter2Connector;
    private StationConnector inter2EndConnector;

    @BeforeEach
    public void setUp() {

        plan = new StationPlan();
        start = new Station();
        start.setStationId(0);
        end = new Station();
        end.setStationId(3);
        plan.addStation(end);
        inter2 = new Station();
        inter2.setStationId(2);
        inter1 = new Station();
        inter1.setStationId(1);
        instance = new PathCreator(start, end);
        startInter1Connection = new StationConnector(start, inter1, 500, 200);
        inter1Inter2Connector = new StationConnector(inter1, inter2, 2000, 300);
        inter2EndConnector = new StationConnector(inter2, end, 2000, 300);
        new StationConnector(inter1, inter2, 2000, 300);
        plan.addStationPath(startInter1Connection, true);
        plan.addStationPath(inter1Inter2Connector, true);
        //plan.getConnectionsBetween(start, inter1).forEach(e -> System.out.println("Setup: " + e));
        PathCreator.setStationPlan(plan);
    }

    @Test
    public void addStation() {
        boolean b = instance.addStation(null, inter1);
        assertTrue(b);
        b = instance.addStation(inter1, start);
        assertFalse(b);
        b = instance.addStation(inter1, inter2);
        assertTrue(b);
        LinkedList<Station> intermediateStations = instance.getIntermediateStations();
        assertEquals(0, intermediateStations.indexOf(inter1));
        assertEquals(1, intermediateStations.indexOf(inter2));
        HashMap<Station, Pair<Station, LinkedList<StationConnector>>> connections = instance.getConnections();
        assertEquals(1, connections.get(start).getValue().size());
        assertEquals(1, connections.get(inter1).getValue().size());
        assertEquals(0, connections.get(inter2).getValue().size());
        boolean pathComplete = instance.isPathComplete();
        assertFalse(pathComplete);
        plan.addStationPath(inter2EndConnector, true);
        instance.updateConnectionLists();
        pathComplete = instance.isPathComplete();
        assertTrue(pathComplete);
    }

    @Test
    public void isPathComplete() {

    }

    @Test
    public void toPath() {
        LinkedList<Station> stations = new LinkedList<>(Arrays.asList(start, inter1, inter2, end));
        addStation();
        Route route = new Route();
        route.setRouteId(4711);
        Path path = instance.toPath(route, true);
        LinkedList<PathStation> pathStations = path.getPathStations();
        pathStations.forEach(e -> assertTrue(stations.contains(e.getConnection().getStart()) && stations.contains(e.getConnection().getEnd())));
    }


    @Test
    void of() {
        plan.addStationPath(inter2EndConnector, true);
        Path p = new Path();
        p.setPathId(90);
        p.setPathStations(new LinkedList<>(Arrays.asList(
                new PathStation(p.getPathId(), 1, startInter1Connection),
                new PathStation(p.getPathId(), 2, inter1Inter2Connector),
                new PathStation(p.getPathId(), 3, inter2EndConnector)
        )));
        PathCreator of = PathCreator.of(p);
        HashMap<Station, Pair<Station, LinkedList<StationConnector>>> connections = of.getConnections();
        connections.entrySet().forEach(System.out::println);
        boolean pathComplete = of.isPathComplete();
        assertTrue(pathComplete);
    }
}