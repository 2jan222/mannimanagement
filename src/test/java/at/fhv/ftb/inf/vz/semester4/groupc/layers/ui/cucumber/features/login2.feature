# Created by Janik Mayr at 05.04.2019
@All
@Login
@NeedDatabase
Feature: Login
  Usecase for Login Controller.

  Scenario: Successful Login 2 [admin]
    Given I see the Login mask
    Then I enter username "admin"
    And I enter password "admin"
    Then I see another mask

  Scenario: Successful Login 2 [user]
    Given I see the Login mask
    Then I enter username "username"
    And I enter password "password"
    Then I see another mask

  Scenario: Failing Login 2 [FAIL]
    Given I see the Login mask
    Then I enter username "not a user"
    And I enter password "not a password"
    Then I see the Login mask
