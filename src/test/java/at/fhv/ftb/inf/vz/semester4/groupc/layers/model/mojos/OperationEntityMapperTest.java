package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.OperationEntity;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.persistence.pojos.RouteRideEntityFast;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class OperationEntityMapperTest {
    @Test
    public void operationEntityToOperationAndReverseTest() {
        RouteRideEntityFast ride = new RouteRideEntityFast();
        ride.setRouteRideId(1);
        OperationEntity operationEntity = new OperationEntity(1, LocalDate.now(), new HashSet<>(Collections.singletonList(ride)), 4, null);
        Operation operation = OperationEntityMapper.INSTANCE.toMojo(operationEntity);
        assertEquals((Object) operationEntity.getOperationId(), operation.getOperationId());
        assertEquals(operationEntity.getDate(), operation.getDate());
        assertEquals(operationEntity.getBusId(), operation.getBusId());
        List<RouteRide> collect = operation.getRouteRides().stream().filter(e -> e.getRouteRideId() == ride.getRouteRideId()).collect(Collectors.toList());
        assertEquals(1, collect.size());
        OperationEntity operationEntity2 = OperationEntityMapper.INSTANCE.toPojo(operation);
        assertEquals((Object) operation.getOperationId(), operationEntity2.getOperationId());
        assertEquals(operation.getDate(), operationEntity2.getDate());
        assertEquals(operation.getBusId(), operationEntity2.getBusId());
        List<RouteRideEntityFast> collect1 = operationEntity2.getRouteRidesByOperationId().stream().filter(e -> e.getRouteRideId() == ride.getRouteRideId()).collect(Collectors.toList());
        assertEquals(1, collect1.size());
        assertEquals(operationEntity, operationEntity2);

    }

}