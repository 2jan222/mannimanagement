package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.application.views.ApplicationStart;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.jetbrains.annotations.TestOnly;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

@TestOnly
public class ApplicationStartTest extends ApplicationTest {
    protected final static ApplicationStart currentInstance = new ApplicationStart();

    static {
        try {
            FxToolkit.registerPrimaryStage();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        currentInstance.externalInit(new LinkedList<>(Arrays.asList("-test", "-debug")));
        Platform.runLater(() -> currentInstance.externalStart(new Stage()));
    }

    private static Supplier<Application> applicationStartSupplier = () -> {
        currentInstance.restart();
        return (Application) currentInstance;
    };


    public ApplicationStartTest() {

    }


    //@BeforeClass
    public static void beforeClass() {
        try {
            FxToolkit.registerPrimaryStage();
            //FxToolkit.setupApplication(ApplicationStart.class,"-test", "-debug");
            FxToolkit.setupApplication(applicationStartSupplier);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BeforeEach
    public void setUp() {
        //FxToolkit.showStage();
        try {
            FxToolkit.registerPrimaryStage();
            //FxToolkit.setupApplication(ApplicationStart.class,"-test", "-debug");
            FxToolkit.setupApplication(applicationStartSupplier);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    @AfterEach
    public void tearDown() {
        // release all keys
        release(new KeyCode[0]);
        // release all mouse buttons
        release(new MouseButton[0]);
        try {
            wait(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //FxToolkit.cleanupStages();
        //FxToolkit.cleanupApplication(new ApplicationAdapter(this));
    }
}