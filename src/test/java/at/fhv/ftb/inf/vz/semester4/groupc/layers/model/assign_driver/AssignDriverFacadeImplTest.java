package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Operation;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.RouteRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.DriverDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.ShiftEntryDTO;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.DriverDTOMapper;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper.ShiftEntryDTOMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class AssignDriverFacadeImplTest extends JUnitRunner {
    private static AssignDriverFacade facade;
    static {
        facade = new AssignDriverFacadeImpl();
        ((AssignDriverFacadeImpl) facade).init(new DataSupplier());
    }
    private final Bus busOp0 = new Bus(20);
    private final Bus busOp1 = new Bus(30);
    private final RouteRide ride0ofOp0 = new RouteRide(10, LocalTime.of(3, 0), LocalTime.of(4, 0), null);
    private final RouteRide ride0ofOp1 = new RouteRide(10, LocalTime.of(4, 15), LocalTime.of(5, 0), null);
    private final RouteRide ride0ofOp2 = new RouteRide(10, LocalTime.of(5, 30), LocalTime.of(6, 0), null);
    private final Operation op0 = new Operation(10, LocalDate.now(), busOp0.getBusId(), new LinkedList<>(Arrays.asList(ride0ofOp0, ride0ofOp1, ride0ofOp2)), busOp0);
    private final Operation op1 = new Operation(20, LocalDate.now(), busOp1.getBusId(), new LinkedList<>(Arrays.asList(ride0ofOp0, ride0ofOp1, ride0ofOp2)), busOp1);
    private final ShiftEntry shift0Op0 = new ShiftEntry(null, LocalDate.now(), ride0ofOp0.getStartTime(), ride0ofOp1.getEndTime(), op0.getOperationId(), busOp0.getBusId());
    private final ShiftEntry shift1Op0 = new ShiftEntry(null, LocalDate.now(), ride0ofOp2.getStartTime(), ride0ofOp2.getEndTime(), op0.getOperationId(), busOp0.getBusId());
    private final ShiftEntry shift0Op1 = new ShiftEntry(null, LocalDate.now(), ride0ofOp0.getStartTime(), ride0ofOp2.getEndTime(), op1.getOperationId(), busOp1.getBusId());
    private final Driver driver0 = new Driver(600, "addr0", LocalDate.MIN, "email0", 0, "fname0", "lname0", "jobDescription0", "telNumber0");
    private final Driver driver1 = new Driver(500, "addr1", LocalDate.MIN, "email1", 0, "fname1", "lname1", "jobDescription1", "telNumber1");

    @Test
    public void getOperationIds() {
        LocalDate date = LocalDate.of(2018, 4, 4);
        LinkedList<Integer> operationsByIDOfDay = facade.getOperationsByIDOfDay(date);
        assertEquals(0, operationsByIDOfDay.size());
    }

    @Test
    @Tag("NeedDB")
    @DisplayName("Assign Driver with DB-Persistence")
    public void assignDriver() {
        ShiftEntryDTO shiftEntryDTO = ShiftEntryDTOMapper.INSTANCE.toDTO(shift0Op0);
        DriverDTO driverDTO = DriverDTOMapper.INSTANCE.toDTO(driver0);
        boolean b;
        try {
            b = facade.assignDriverToShift(shiftEntryDTO, driverDTO);
            assertFalse(b);
            LocalDate date = LocalDate.of(2019, 7, 21);
            LinkedList<Integer> operationsByIDOfDay = facade.getOperationsByIDOfDay(date);
            LinkedList<ShiftEntryDTO> shiftsByOperationId = facade.getShiftsByOperationId(operationsByIDOfDay.getFirst(), date);
            b = facade.assignDriverToShift(shiftsByOperationId.getFirst(), driverDTO);
            assertFalse(b);
            shiftsByOperationId = facade.getShiftsByOperationId(operationsByIDOfDay.getFirst(), date);
            ShiftEntryDTO first = shiftsByOperationId.getFirst();
            assertEquals(new Integer(1), first.getDriverId());
            boolean b1 = facade.removeDriverFromShift(first);
            assertTrue(b1);
            LinkedList<DriverDTO> freeDriverFormShift = facade.getFreeDriverFormShift(first);
            System.out.println(freeDriverFormShift);
        } catch (AssignDriverContextCreationException | AssignDriverDataTransferUpdateException e) {
            fail(e);
            e.printStackTrace();
        }
    }


}