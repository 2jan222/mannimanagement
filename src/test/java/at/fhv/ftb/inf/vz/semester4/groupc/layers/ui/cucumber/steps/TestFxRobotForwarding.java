package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.cucumber.steps;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.ApplicationStartTest;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.geometry.*;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.MouseButton;
import javafx.stage.Window;
import org.hamcrest.Matcher;
import org.testfx.api.FxRobotInterface;
import org.testfx.robot.Motion;
import org.testfx.service.query.NodeQuery;
import org.testfx.service.query.PointQuery;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

@SuppressWarnings({"unused", "UnusedReturnValue", "WeakerAccess"})
public class TestFxRobotForwarding {

    ApplicationStartTest app;

    public TestFxRobotForwarding(){
    }


    @SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
    public NodeQuery fromAll() {
        return app.fromAll();
    }


    public NodeQuery from(Node... nodes) {
        return app.from(nodes);
    }


    public NodeQuery from(Collection<Node> collection) {
        return app.from(collection);
    }


    public NodeQuery lookup(String query) {
        return app.lookup(query);
    }


    public <T extends Node> NodeQuery lookup(Matcher<T> matcher) {
        return app.lookup(matcher);
    }


    public <T extends Node> NodeQuery lookup(Predicate<T> predicate) {
        return app.lookup(predicate);
    }


    public NodeQuery from(NodeQuery nodeQuery) {
        return app.from(nodeQuery);
    }


    public FxRobotInterface interact(Runnable runnable) {
        return app.interact(runnable);
    }


    public <T> FxRobotInterface interact(Callable<T> callable) {
        return app.interact(callable);
    }


    public FxRobotInterface interactNoWait(Runnable runnable) {
        return app.interactNoWait(runnable);
    }


    public <T> FxRobotInterface interactNoWait(Callable<T> callable) {
        return app.interactNoWait(callable);
    }


    public FxRobotInterface interrupt() {
        return app.interrupt();
    }


    public FxRobotInterface interrupt(int attemptsCount) {
        return app.interrupt(attemptsCount);
    }


    public FxRobotInterface sleep(long l) {
        return app.sleep(l);
    }


    public FxRobotInterface sleep(long l, TimeUnit timeUnit) {
        return app.sleep(l, timeUnit);
    }


    public FxRobotInterface clickOn(MouseButton... mouseButtons) {
        return app.clickOn(mouseButtons);
    }

    public FxRobotInterface clickOn(String fxId) {
        return app.clickOn(fxId);
    }


    public FxRobotInterface clickOn(PointQuery pointQuery, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(pointQuery, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(MouseButton... mouseButtons) {
        return app.doubleClickOn(mouseButtons);
    }


    public FxRobotInterface doubleClickOn(PointQuery pointQuery, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(pointQuery, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(double x, double y, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(x, y, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(Point2D point2D, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(point2D, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(Bounds bounds, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(bounds, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(Node node, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(node, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(Scene scene, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(scene, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(Window window, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(window, motion, mouseButtons);
    }


    public FxRobotInterface clickOn(String query, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(query, motion, mouseButtons);
    }


    public <T extends Node> FxRobotInterface clickOn(Matcher<T> matcher, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(matcher);
    }


    public <T extends Node> FxRobotInterface clickOn(Predicate<T> predicate, Motion motion, MouseButton... mouseButtons) {
        return app.clickOn(predicate, motion, mouseButtons);
    }


    public FxRobotInterface rightClickOn() {
        return app.rightClickOn();
    }


    public FxRobotInterface rightClickOn(PointQuery pointQuery, Motion motion) {
        return app.rightClickOn(pointQuery, motion);
    }


    public FxRobotInterface rightClickOn(double x, double y, Motion motion) {
        return app.rightClickOn(x, y, motion);
    }


    public FxRobotInterface rightClickOn(Point2D point2D, Motion motion) {
        return app.rightClickOn(point2D, motion);
    }


    public FxRobotInterface rightClickOn(Bounds bounds, Motion motion) {
        return app.rightClickOn(bounds, motion);
    }


    public FxRobotInterface rightClickOn(Node node, Motion motion) {
        return app.rightClickOn(node, motion);
    }


    public FxRobotInterface rightClickOn(Scene scene, Motion motion) {
        return app.rightClickOn(scene, motion);
    }


    public FxRobotInterface rightClickOn(Window window, Motion motion) {
        return app.rightClickOn(window, motion);
    }


    public FxRobotInterface rightClickOn(String query, Motion motion) {
        return app.rightClickOn(query, motion);
    }


    public <T extends Node> FxRobotInterface rightClickOn(Matcher<T> matcher, Motion motion) {
        return app.rightClickOn(matcher, motion);
    }


    public <T extends Node> FxRobotInterface rightClickOn(Predicate<T> predicate, Motion motion) {
        return app.rightClickOn(predicate, motion);
    }


    public FxRobotInterface doubleClickOn(double x, double y, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(x, y, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(Point2D point2D, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(point2D, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(Bounds bounds, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(bounds, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(Node node, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(node, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(Scene scene, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(scene, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(Window window, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(window, motion, mouseButtons);
    }


    public FxRobotInterface doubleClickOn(String query, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(query, motion, mouseButtons);
    }


    public <T extends Node> FxRobotInterface doubleClickOn(Matcher<T> matcher, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(matcher, motion, mouseButtons);
    }


    public <T extends Node> FxRobotInterface doubleClickOn(Predicate<T> predicate, Motion motion, MouseButton... mouseButtons) {
        return app.doubleClickOn(predicate, motion, mouseButtons);
    }


    public FxRobotInterface drag(MouseButton... mouseButtons) {
        return app.drag(mouseButtons);
    }


    public FxRobotInterface drag(PointQuery pointQuery, MouseButton... mouseButtons) {
        return app.drag(pointQuery, mouseButtons);
    }


    public FxRobotInterface drop() {
        return app.drag();
    }


    public FxRobotInterface dropTo(PointQuery pointQuery) {
        return app.dropTo(pointQuery);
    }


    public FxRobotInterface dropBy(double x, double y) {
        return app.dropTo(x, y);
    }


    public FxRobotInterface drag(double x, double y, MouseButton... mouseButtons) {
        return app.drag(x, y, mouseButtons);
    }


    public FxRobotInterface drag(Point2D point2D, MouseButton... mouseButtons) {
        return app.drag(point2D,mouseButtons);
    }


    public FxRobotInterface drag(Bounds bounds, MouseButton... mouseButtons) {
        return app.drag(bounds, mouseButtons);
    }


    public FxRobotInterface drag(Node node, MouseButton... mouseButtons) {
        return app.drag(node, mouseButtons);
    }


    public FxRobotInterface drag(Scene scene, MouseButton... mouseButtons) {
        return app.drag(scene, mouseButtons);
    }


    public FxRobotInterface drag(Window window, MouseButton... mouseButtons) {
        return app.drag(window, mouseButtons);
    }


    public FxRobotInterface drag(String query, MouseButton... mouseButtons) {
        return app.drag(query, mouseButtons);
    }


    public <T extends Node> FxRobotInterface drag(Matcher<T> matcher, MouseButton... mouseButtons) {
        return app.drag(matcher, mouseButtons);
    }


    public <T extends Node> FxRobotInterface drag(Predicate<T> predicate, MouseButton... mouseButtons) {
        return app.drag(predicate, mouseButtons);
    }


    public FxRobotInterface dropTo(double x, double y) {
        return app.dropTo(x,y);
    }


    public FxRobotInterface dropTo(Point2D point2D) {
        return app.dropTo(point2D);
    }


    public FxRobotInterface dropTo(Bounds bounds) {
        return app.dropTo(bounds);
    }


    public FxRobotInterface dropTo(Node node) {
        return app.dropTo(node);
    }


    public FxRobotInterface dropTo(Scene scene) {
        return app.dropTo(scene);
    }


    public FxRobotInterface dropTo(Window window) {
        return app.dropTo(window);
    }


    public FxRobotInterface dropTo(String query) {
        return app.dropTo(query);
    }


    public <T extends Node> FxRobotInterface dropTo(Matcher<T> matcher) {
        return app.dropTo(matcher);
    }


    public <T extends Node> FxRobotInterface dropTo(Predicate<T> predicate) {
        return app.dropTo(predicate);
    }


    public FxRobotInterface press(KeyCode... keyCodes) {
        return app.press(keyCodes);
    }


    public FxRobotInterface release(KeyCode... keyCodes) {
        return app.release(keyCodes);
    }


    public FxRobotInterface press(MouseButton... mouseButtons) {
        return app.press(mouseButtons);
    }


    public FxRobotInterface release(MouseButton... mouseButtons) {
        return app.release(mouseButtons);
    }


    public FxRobotInterface moveTo(PointQuery pointQuery, Motion motion) {
        return app.moveTo(pointQuery, motion);
    }


    public FxRobotInterface moveBy(double x, double y, Motion motion) {
        return app.moveBy(x, y, motion);
    }


    public FxRobotInterface moveTo(double x, double y, Motion motion) {
        return app.moveTo( x, y, motion);
    }


    public FxRobotInterface moveTo(Point2D point2D, Motion motion) {
        return app.moveTo(point2D, motion);
    }


    public FxRobotInterface moveTo(Bounds bounds, Motion motion) {
        return app.moveTo(bounds, motion);
    }


    public FxRobotInterface moveTo(Node node, Pos pos, Point2D point2D, Motion motion) {
        return app.moveTo(node, pos, point2D, motion);
    }


    public FxRobotInterface moveTo(Scene scene, Motion motion) {
        return app.moveTo(scene, motion);
    }


    public FxRobotInterface moveTo(Window window, Motion motion) {
        return app.moveTo(window, motion);
    }


    public FxRobotInterface moveTo(String query, Motion motion) {
        return app.moveTo(query, motion);
    }


    public <T extends Node> FxRobotInterface moveTo(Matcher<T> matcher, Motion motion) {
        return app.moveTo(matcher, motion);
    }


    public <T extends Node> FxRobotInterface moveTo(Predicate<T> predicate, Motion motion) {
        return app.moveTo(predicate, motion);
    }


    public FxRobotInterface scroll(int amount, VerticalDirection verticalDirection) {
        return app.scroll(amount, verticalDirection);
    }


    public FxRobotInterface scroll(VerticalDirection verticalDirection) {
        return app.scroll(verticalDirection);
    }


    public FxRobotInterface scroll(int amount, HorizontalDirection horizontalDirection) {
        return app.scroll(amount, horizontalDirection);
    }


    public FxRobotInterface scroll(HorizontalDirection horizontalDirection) {
        return app.scroll(horizontalDirection);
    }


    public FxRobotInterface push(KeyCode... keyCodes) {
        return app.push(keyCodes);
    }


    public FxRobotInterface push(KeyCodeCombination keyCodeCombination) {
        return app.push(keyCodeCombination);
    }


    public FxRobotInterface type(KeyCode... keyCodes) {
        return app.type(keyCodes);
    }


    public FxRobotInterface type(KeyCode keyCode, int times) {
        return app.type(keyCode, times);
    }


    public FxRobotInterface eraseText(int i) {
        return app.eraseText(i);
    }


    public FxRobotInterface write(char c) {
        return app.write(c);
    }


    public FxRobotInterface write(String s) {
        return app.write(s);
    }


    public FxRobotInterface write(String s, int i) {
        return app.write(s, i);
    }
}
