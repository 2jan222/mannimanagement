package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.cucumber.steps;

import cucumber.api.java8.En;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import org.jetbrains.annotations.NotNull;
import org.testfx.api.FxRobotException;



import static org.junit.jupiter.api.Assertions.assertFalse;


@SuppressWarnings("WeakerAccess")
public class LoginSteps extends UtilSteps implements En {

    public LoginSteps() {
        Given("^I see the Login mask$", this::loginVisible);
        Then("^I enter username \"([^\"]*)\"$", this::enterUsername);
        Then("^I enter password \"([^\"]*)\"$", this::enterPassword);
        Then("^I see another mask$", this::isLoggedIn);
    }

    private void loginVisible() {
        waitForFxEvents();
        boolean loginStatus = hasWindowTitle(window((Node) find("#usernameTextField")), "login");
        assertFalse(loginStatus);
    }

    public void enterPassword(@NotNull final String password) throws FxRobotException {
        clickOn("#passwordField");
        ((PasswordField) find("#passwordField")).clear();
        write(password);
        takeSnapshotOfNode("target/screencap(1).png", find("#loginPane"));
        press(KeyCode.ENTER);
    }

    public void enterUsername(@NotNull final String username) throws FxRobotException {
        clickOn("#usernameTextField");
        ((TextField) find("#usernameTextField")).clear();
        takeSnapshotOfNode("target/screencap.png", find("#loginPane"));
        write(username);
    }

    public void isLoggedIn() {
        waitForFxEvents();
        boolean loginStatus = hasWindowTitle(targetWindow(), "login");
        assertFalse(loginStatus);
        reset();
    }

}
