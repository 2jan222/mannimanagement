package at.fhv.ftb.inf.vz.semester4.groupc.layers.model;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver.AssignDriverActionCheckerTest;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver.RawDataValidatorTest;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.PathCreatorTest;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.path.StationPlanTest;
import org.junit.platform.suite.api.SelectClasses;


@SelectClasses({PathCreatorTest.class, StationPlanTest.class, AssignDriverActionCheckerTest.class, RawDataValidatorTest.class})
public class ModelLayerRunner extends JUnitRunner {
}
