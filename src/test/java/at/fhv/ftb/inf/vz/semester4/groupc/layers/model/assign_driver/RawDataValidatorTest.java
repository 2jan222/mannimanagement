package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationFlat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class RawDataValidatorTest {
    private static final String DISSATISFY_VALIDATOR = "Set fields should dissatisfy Validator";
    private static final String SATISFY_VALIDATOR = "Set fields should satisfy Validator";

    @Test
    void hasOperationBus() {
        OperationFlat operation = new OperationFlat(null, null, null, null, null);
        assertThrows(ValidationException.class,
                () -> RawDataValidator.hasOperationBus(operation), DISSATISFY_VALIDATOR);
        operation.setBusId(1);
        try {
            RawDataValidator.hasOperationBus(operation);
        } catch (ValidationException e) {
            fail(SATISFY_VALIDATOR);
        }
    }

    @Test
    void hasOperationId() {
        OperationFlat operation = new OperationFlat(null, null, null, null, null);
        assertThrows(ValidationException.class, () -> RawDataValidator.hasOperationId(null), DISSATISFY_VALIDATOR);
        assertThrows(ValidationException.class, () -> RawDataValidator.hasOperationId(operation), DISSATISFY_VALIDATOR);
        operation.setOperationId(1);
        try {
            RawDataValidator.hasOperationId(operation);
        } catch (ValidationException e) {
            fail(SATISFY_VALIDATOR);
        }
    }
}