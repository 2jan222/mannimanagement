package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.mapper;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationShift;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.dto.dtos.OperationShiftDTO;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Deprecated
class OperationShiftDTOMapperTest {

    @Test
    @Deprecated
    public void operationShiftToOperationShiftDTOAndReverse() {
        OperationShift os = new OperationShift(1, LocalDate.now(),
                LocalTime.of(19, 30, 0),
                LocalTime.of(20, 30, 0), 3, 10, 4);
        OperationShiftDTO osDTO = OperationShiftDTOMapper.INSTANCE.toDTO(os);
        assertEquals(os.getOperationShiftId(), osDTO.getOperationShiftId());
        assertEquals(os.getDate(), osDTO.getDate());
        assertEquals(os.getStartTime(), osDTO.getStartTime());
        assertEquals(os.getEndTime(), osDTO.getEndTime());
        assertEquals(os.getOperationId(), osDTO.getOperationId());
        assertEquals(os.getDriverId(), osDTO.getDriverId());
        assertEquals(os.getBusId(), osDTO.getBusId());
        os = OperationShiftDTOMapper.INSTANCE.toMojo(osDTO);
        assertEquals(osDTO.getOperationShiftId(), os.getOperationShiftId());
        assertEquals(osDTO.getDate(), os.getDate());
        assertEquals(osDTO.getStartTime(), os.getStartTime());
        assertEquals(osDTO.getEndTime(), os.getEndTime());
        assertEquals(osDTO.getOperationId(), os.getOperationId());
        assertEquals(osDTO.getDriverId(), os.getDriverId());
        assertEquals(osDTO.getBusId(), os.getBusId());

    }


}