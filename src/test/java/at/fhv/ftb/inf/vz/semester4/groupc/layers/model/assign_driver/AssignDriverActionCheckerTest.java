package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Tests for Assign Driver Actions Internal Checks")
public
class AssignDriverActionCheckerTest extends JUnitRunner {


    @Nested
    @DisplayName("Overlapping Tests[G=Shifts, T=Target, S=StartTime, E=EndTime]")
    public class Overlapping {

        @Test
        @DisplayName("Case 1: ET before SG - not overlapping")
        public void timeOverlappingCase1() {

            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.NOON, LocalTime.of(13, 0), 1, 99);
            ShiftEntry shifts = new ShiftEntry(null, LocalDate.now(), LocalTime.of(15, 0), LocalTime.of(17, 0), 1, 99);
            boolean overlapping = AssignDriverActionChecker.timeOverlappingTestAccess(target, new LinkedList<>(Collections.singletonList(shifts)));
            assertFalse(overlapping);
        }

        @Test
        @DisplayName("Case 2: ET after SG and ST before EG")
        public void timeOverlappingCase2() {

            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(18, 0), LocalTime.of(19, 0), 1, 99);
            ShiftEntry shifts = new ShiftEntry(null, LocalDate.now(), LocalTime.of(15, 0), LocalTime.of(20, 0), 1, 99);
            boolean overlapping = AssignDriverActionChecker.timeOverlappingTestAccess(target, new LinkedList<>(Collections.singletonList(shifts)));
            assertTrue(overlapping);

        }

        @Test
        @DisplayName("Case 3: ET after SG and ST before EG and ST after SG - overlapping")
        public void timeOverlappingCase3() {

            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(18, 0), LocalTime.of(19, 0), 1, 99);
            ShiftEntry shifts = new ShiftEntry(null, LocalDate.now(), LocalTime.of(15, 0), LocalTime.of(20, 0), 1, 99);
            boolean overlapping = AssignDriverActionChecker.timeOverlappingTestAccess(target, new LinkedList<>(Collections.singletonList(shifts)));
            assertTrue(overlapping);

            //   ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.NOON, LocalTime.NOON.plus(50, ChronoUnit.MINUTES), 10, 99);
            //   boolean overlapping = AssignDriverActionChecker.timeOverlappingTestAccess(target, new LinkedList<>(Arrays.asList(target)));
            //   assertTrue(overlapping, "Should overlap");
        }

        @Test
        @DisplayName("Case 4: ET after SG and ET after EG but ST before EG / overlapping")
        public void timeOverlappingCase4() {

            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(18, 0), LocalTime.of(21, 0), 1, 99);
            ShiftEntry shifts = new ShiftEntry(null, LocalDate.now(), LocalTime.of(15, 0), LocalTime.of(20, 0), 1, 99);
            boolean overlapping = AssignDriverActionChecker.timeOverlappingTestAccess(target, new LinkedList<>(Collections.singletonList(shifts)));
            assertTrue(overlapping);

        }

        @Test
        @DisplayName("Case 5: ET after SG and ST after EG - not overlapping")
        public void timeOverlappingCase5() {

            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(10, 0), LocalTime.of(11, 0), 1, 99);
            ShiftEntry shifts = new ShiftEntry(null, LocalDate.now(), LocalTime.of(15, 0), LocalTime.of(22, 0), 1, 99);
            boolean overlapping = AssignDriverActionChecker.timeOverlappingTestAccess(target, new LinkedList<>(Collections.singletonList(shifts)));
            assertFalse(overlapping);
        }


    }

    @Nested
    @DisplayName("Shift Adherence To Law Tests")
    public class TheLaw {
        @Test
        @DisplayName("Case 1, OK")
        void adherenceToLaw1() {
            HashMap<StationIDPair, Integer> times = new HashMap<>();
            times.put(new StationIDPair(1, 2), 60*5);
            DataContext context = new DataContext(times);
            Station station1 = new Station(1, "Name1", "s1");
            Station station2 = new Station(2, "Name2", "s2");
            Station station3 = new Station(3, "Name3", "s3");
            Station station4 = new Station(4, "Name4", "s4");
            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(19, 0, 0),
                    LocalTime.of(20, 0, 0), 1, 1);
            ShiftRide shiftRide1 = new ShiftRide(1, station1, station2,
                    LocalTime.of(19, 0, 0),
                    LocalTime.of(19, 20, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide2 = new ShiftRide(2, station2, station3,
                    LocalTime.of(19, 35, 0),
                    LocalTime.of(19, 45, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide3 = new ShiftRide(3, station3, station4,
                    LocalTime.of(19, 50, 0),
                    LocalTime.of(20, 0, 0), 1, 1, LocalDate.now());
            LinkedList<ShiftRide> rides = new LinkedList<>();
            rides.add(shiftRide1);
            rides.add(shiftRide2);
            rides.add(shiftRide3);
            target.setShiftRides(rides);
            assertTrue(AssignDriverActionChecker.checkShiftComplianceToLaw(target, 1, context),
                    "Should be compliant to law");

        }

        @Test
        @DisplayName("Case 2, no pause after 90 Minutes")
        public void adherenceToLaw2() {
            HashMap<StationIDPair, Integer> times = new HashMap<>();
            times.put(new StationIDPair(1, 2), 60*5);
            DataContext context = new DataContext(times);
            Station station1 = new Station(1, "Name1", "s1");
            Station station2 = new Station(2, "Name2", "s2");
            Station station3 = new Station(3, "Name3", "s3");
            Station station4 = new Station(4, "Name4", "s4");
            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(19, 0, 0),
                    LocalTime.of(21, 0, 0), 1, 1);
            ShiftRide shiftRide1 = new ShiftRide(1, station1, station2,
                    LocalTime.of(19, 0, 0),
                    LocalTime.of(20, 0, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide2 = new ShiftRide(2, station2, station3,
                    LocalTime.of(20, 5, 0),
                    LocalTime.of(20, 45, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide3 = new ShiftRide(3, station3, station4,
                    LocalTime.of(20, 50, 0),
                    LocalTime.of(21, 0, 0), 1, 1, LocalDate.now());
            LinkedList<ShiftRide> rides = new LinkedList<>();
            rides.add(shiftRide1);
            rides.add(shiftRide2);
            rides.add(shiftRide3);
            target.setShiftRides(rides);
            assertFalse(AssignDriverActionChecker.checkShiftComplianceToLaw(target, 1, context),
                    "Should not be compliant to law, no pause after 90 minutes");

        }

        @Test
        @DisplayName("Case 3, Ok")
        public void adherenceToLaw3() {
            HashMap<StationIDPair, Integer> times = new HashMap<>();
            times.put(new StationIDPair(1, 2), 60*5);
            DataContext context = new DataContext(times);
            Station station1 = new Station(1, "Name1", "s1");
            Station station2 = new Station(2, "Name2", "s2");
            Station station3 = new Station(3, "Name3", "s3");
            Station station4 = new Station(4, "Name4", "s4");
            Station station5 = new Station(5, "Name5", "s5");
            Station station6 = new Station(6, "Name6", "s6");
            Station station7 = new Station(7, "Name7", "s7");
            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(10, 0, 0),
                    LocalTime.of(18, 0, 0), 1, 1);
            ShiftRide shiftRide1 = new ShiftRide(1, station1, station2,
                    LocalTime.of(10, 0, 0),
                    LocalTime.of(11, 0, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide2 = new ShiftRide(2, station2, station3,
                    LocalTime.of(11, 20, 0),
                    LocalTime.of(12, 30, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide3 = new ShiftRide(3, station3, station4,
                    LocalTime.of(12, 50, 0),
                    LocalTime.of(13, 30, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide4 = new ShiftRide(4, station4, station5,
                    LocalTime.of(13, 50, 0),
                    LocalTime.of(14, 50, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide5 = new ShiftRide(5, station5, station6,
                    LocalTime.of(15, 5, 0),
                    LocalTime.of(16, 50, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide6 = new ShiftRide(6, station6, station7,
                    LocalTime.of(17, 10, 0),
                    LocalTime.of(18, 0, 0), 1, 1, LocalDate.now());
            LinkedList<ShiftRide> rides = new LinkedList<>();
            rides.add(shiftRide1);
            rides.add(shiftRide2);
            rides.add(shiftRide3);
            rides.add(shiftRide4);
            rides.add(shiftRide5);
            rides.add(shiftRide6);
            target.setShiftRides(rides);
            assertTrue(AssignDriverActionChecker.checkShiftComplianceToLaw(target, 0, context),
                    "should be compliant to law");
        }

        @Test
        @DisplayName("Case 4, More than eight hours")
            public void adherenceToLaw4() {
            HashMap<StationIDPair, Integer> times = new HashMap<>();
            times.put(new StationIDPair(1, 2), 60*5);
            DataContext context = new DataContext(times);
            Station station1 = new Station(1, "Name1", "s1");
            Station station2 = new Station(2, "Name2", "s2");
            Station station3 = new Station(3, "Name3", "s3");
            Station station4 = new Station(4, "Name4", "s4");
            Station station5 = new Station(5, "Name5", "s5");
            Station station6 = new Station(6, "Name6", "s6");
            Station station7 = new Station(7, "Name7", "s7");
            Station station8 = new Station(8, "Name8", "s8");
            Station station9 = new Station(9, "Name9", "s9");
            ShiftEntry target = new ShiftEntry(null, LocalDate.now(), LocalTime.of(10, 0, 0),
                    LocalTime.of(20, 20, 0), 1, 1);
            ShiftRide shiftRide1 = new ShiftRide(1, station1, station2,
                    LocalTime.of(10, 0, 0),
                    LocalTime.of(11, 0, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide2 = new ShiftRide(2, station2, station3,
                    LocalTime.of(11, 20, 0),
                    LocalTime.of(12, 30, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide3 = new ShiftRide(3, station3, station4,
                    LocalTime.of(12, 50, 0),
                    LocalTime.of(13, 30, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide4 = new ShiftRide(4, station4, station5,
                    LocalTime.of(13, 50, 0),
                    LocalTime.of(14, 50, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide5 = new ShiftRide(5, station5, station6,
                    LocalTime.of(15, 5, 0),
                    LocalTime.of(16, 50, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide6 = new ShiftRide(6, station6, station7,
                    LocalTime.of(17, 10, 0),
                    LocalTime.of(18, 0, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide7 = new ShiftRide(7, station7, station8,
                    LocalTime.of(18, 15, 0),
                    LocalTime.of(19, 20, 0), 1, 1, LocalDate.now());
            ShiftRide shiftRide8 = new ShiftRide(8, station8, station9,
                    LocalTime.of(19, 35, 0),
                    LocalTime.of(20, 20, 0), 1, 1, LocalDate.now());
            LinkedList<ShiftRide> rides = new LinkedList<>();
            rides.add(shiftRide1);
            rides.add(shiftRide2);
            rides.add(shiftRide3);
            rides.add(shiftRide4);
            rides.add(shiftRide5);
            rides.add(shiftRide6);
            rides.add(shiftRide7);
            rides.add(shiftRide8);
            target.setShiftRides(rides);
            assertFalse(AssignDriverActionChecker.checkShiftComplianceToLaw(target, 0, context),
                    "should not be compliant to law, more than eight hours of work in shift");
        }
    }

}