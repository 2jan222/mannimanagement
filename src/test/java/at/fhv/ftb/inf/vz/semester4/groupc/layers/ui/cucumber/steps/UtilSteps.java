package at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.cucumber.steps;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.ui.ApplicationStartTest;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.jetbrains.annotations.NotNull;
import org.testfx.util.WaitForAsyncUtils;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class UtilSteps extends ApplicationStartTest {

    /**
     * Takes a snapshot of a node.
     * @param referenceSnapshotPath path to file
     * @param nodeUnderTest node to take snapshot of. Snapshot will take position of node as origin (top-left).
     * @return {@code boolean} true, if no IOException occurred.
     */
    @SuppressFBWarnings({"NP_NULL_PARAM_DEREF", "NP_NULL_ON_SOME_PATH", "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE"})
    @SuppressWarnings({"UnusedReturnValue", "WeakerAccess", "ResultOfMethodCallIgnored"})
    public boolean takeSnapshotOfNode(@NotNull final String referenceSnapshotPath, @NotNull final Node nodeUnderTest) {
        File file = new File(referenceSnapshotPath);
        try {
            file.createNewFile();// if file already exists will do nothing
            final WritableImage img = new WritableImage((int)nodeUnderTest.getScene().getWidth(), (int)nodeUnderTest.getScene().getHeight());
            Platform.runLater(() -> nodeUnderTest.snapshot(null, img));
            WaitForAsyncUtils.waitForFxEvents();
            ImageIO.write(SwingFXUtils.fromFXImage(img, null), "png", file);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @SuppressWarnings({"SameParameterValue", "unchecked", "WeakerAccess"})
    public  <T extends Node> T find(@NotNull final String fxId) {
        return (T) lookup(fxId).queryAll().iterator().next();
    }

    boolean hasWindowTitle(@NotNull Window window, @NotNull String stageTitleRegex) {
        return ((Stage) window).getTitle() != null && ((Stage) window).getTitle().toLowerCase().matches(stageTitleRegex);
    }

    public boolean hasStageTitle(@NotNull Stage stage, @NotNull String stageTitleRegex) {
        return stage.getTitle() != null && stage.getTitle().toLowerCase().matches(stageTitleRegex);
    }

    void waitForFxEvents() {
        WaitForAsyncUtils.waitForFxEvents();
    }

    void reset() {
        currentInstance.restart();
    }


}
