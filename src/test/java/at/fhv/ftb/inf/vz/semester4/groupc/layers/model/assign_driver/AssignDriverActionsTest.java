package at.fhv.ftb.inf.vz.semester4.groupc.layers.model.assign_driver;

import at.fhv.ftb.inf.vz.semester4.groupc.layers.JUnitRunner;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Bus;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Driver;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.OperationFlat;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftEntry;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.ShiftRide;
import at.fhv.ftb.inf.vz.semester4.groupc.layers.model.mojos.Station;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class AssignDriverActionsTest extends JUnitRunner {
    private DataContext context;
    private DataContextBuilder builder;
    private final Bus busOp0 = new Bus(20);
    private final Bus busOp1 = new Bus(30);
    private final Station s0 = new Station(50, "Station am Fluss", "SAF");
    private final Station s1 = new Station(100, "Station am Schloss", "SAS");
    private final Station s2 = new Station(150, "Station am Berg", "SAB");
    private final Integer op0Id = 10;
    private final Integer op1Id = 20;
    private final ShiftRide ride0ofOp0 = new ShiftRide(10, s0, s1, LocalTime.of(3, 0), LocalTime.of(4, 0), op0Id, null, LocalDate.now());
    private final ShiftRide ride1ofOp0 = new ShiftRide(10, s1, s2, LocalTime.of(4, 15), LocalTime.of(5, 0), op0Id, null, LocalDate.now());
    private final ShiftRide ride0ofOp1 = new ShiftRide(10, s0, s2, LocalTime.of(5, 30), LocalTime.of(6, 0), op1Id, null, LocalDate.now());
    private final OperationFlat op0 = new OperationFlat(op0Id, LocalDate.now(), busOp0.getBusId(), busOp0, new LinkedList<>(Arrays.asList(ride0ofOp0, ride1ofOp0, ride0ofOp1)));
    private final OperationFlat op1 = new OperationFlat(op1Id, LocalDate.now(), busOp1.getBusId(), busOp1, new LinkedList<>(Arrays.asList(ride0ofOp0, ride1ofOp0, ride0ofOp1)));
    private final ShiftEntry shift0Op0 = new ShiftEntry(null, LocalDate.now(), ride0ofOp0.getStartTime(), ride1ofOp0.getEndTime(), op0.getOperationId(), busOp0.getBusId());
    private final ShiftEntry shift1Op0 = new ShiftEntry(null, LocalDate.now(), ride0ofOp1.getStartTime(), ride0ofOp1.getEndTime(), op0.getOperationId(), busOp0.getBusId());
    private final ShiftEntry shift0Op1 = new ShiftEntry(null, LocalDate.now(), ride0ofOp0.getStartTime(), ride0ofOp1.getEndTime(), op1.getOperationId(), busOp1.getBusId());
    private final Driver driver0 = new Driver(600, "addr0", LocalDate.MIN, "email0", 0, "fname0", "lname0", "jobDescription0", "telNumber0");
    private final Driver driver1 = new Driver(500, "addr1", LocalDate.MIN, "email1", 0, "fname1", "lname1", "jobDescription1", "telNumber1");


    @BeforeEach
    void setUp() {
        builder = DataContext.builder(LocalDate.now());
        try {
            builder.addOperation(op0).addShift(shift0Op0).addShift(shift1Op0).addShift(shift0Op1);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        context = builder.buildContext();
    }

    @Test
    @DisplayName("Creation of new Shift")
    void createNewShift() {
        builder = DataContext.builder(LocalDate.now());
        try {
            builder.addOperation(op0);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        context = builder.buildContext();
        ShiftEntry newShift = AssignDriverActions.createNewShift(op0.getOperationId(), new LinkedList<>(Collections.singletonList(ride0ofOp0)), context);
        assertNotNull(newShift);
        newShift = AssignDriverActions.createNewShift(op0.getOperationId(), new LinkedList<>(Collections.singletonList(ride1ofOp0)), context);
        assertNotNull(newShift);
        System.out.println(newShift);
        System.out.println(newShift.getShiftRides());
        LinkedList<ShiftEntry> shiftEntriesForOperation = context.getShiftEntriesForOperation(op0.getOperationId());
        System.out.println(shiftEntriesForOperation);
    }

    @Test
    @DisplayName("Assignment of Driver")
    void assignDriverToShift() {
        Driver d = new Driver(700, "addr", LocalDate.MIN, "email", 0, "fname", "lname", "jobDescription", "telNumber");
        boolean b = AssignDriverActions.assignDriverToShift(d, shift0Op0, context);
        assertTrue(b);
        b = AssignDriverActions.assignDriverToShift(d, shift0Op0, context);
        assertFalse(b);
        assertEquals(d, shift0Op0.getDriver());
    }

    @Test
    void removeDriverFromShift() {
        System.out.println("Test");
    }


    @Nested
    @DisplayName("Drivers Applicable For Shift Cases")
    public class DriverApplicableTestCases {
        DataContext dataContext;
        LinkedList<Driver> drivers;

        @Test
        @DisplayName("CASE 0: No Drivers on Duty -> No drivers available for shift")
        void case0() {
            dataContext = builder.buildContext();
            drivers = AssignDriverActions.calculateDriversApplicableForShift(shift0Op0, dataContext);
            assertEquals(0, drivers.size());
        }

        @Test
        @DisplayName("CASE 1: Two Drivers on Duty -> All drivers available for shift")
        void case1() {
            createContextWithDrivers();
            drivers = AssignDriverActions.calculateDriversApplicableForShift(shift0Op0, dataContext);
            assertEquals(2, drivers.size());
            assertTrue(drivers.contains(driver0));
            assertTrue(drivers.contains(driver1));
        }

        @Test
        @DisplayName("CASE 2: Two Drivers on Duty -> One driver available for shift, other one Assigned to same time")
        void case2() {
            createContextWithDrivers();
            boolean b = AssignDriverActions.assignDriverToShift(driver0, shift0Op0, dataContext);
            assertTrue(b);
            drivers = AssignDriverActions.calculateDriversApplicableForShift(shift0Op1, dataContext);
            assertEquals(1, drivers.size());
            assertTrue(drivers.contains(driver1));
            assertFalse(drivers.contains(driver0));
        }

        @Test
        @DisplayName("CASE 3: List for Shift with driver should be empty")
        void case3() {
            createContextWithDrivers();
            boolean b = AssignDriverActions.assignDriverToShift(driver0, shift0Op0, dataContext);
            assertTrue(b);
            drivers = AssignDriverActions.calculateDriversApplicableForShift(shift0Op0, dataContext);
            assertEquals(0, drivers.size());
        }

        @Test
        @DisplayName("CASE 4: Driver assigned in previous shift may driver in later one")
        void case4() {
            createContextWithDrivers();
            boolean b = AssignDriverActions.assignDriverToShift(driver0, shift0Op0, dataContext);
            assertTrue(b);
            drivers = AssignDriverActions.calculateDriversApplicableForShift(shift1Op0, dataContext);
            assertEquals(2, drivers.size());
            assertTrue(drivers.contains(driver0));
            assertTrue(drivers.contains(driver1));
        }


        void createContextWithDrivers() {
            try {
                builder.addDriverOnDuty(driver0).addDriverOnDuty(driver1);
                dataContext = builder.buildContext();
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        }
    }

}